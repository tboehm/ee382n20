#!/usr/bin/awk -f

BEGIN {
    FS=" "
    print "mass,x,y,z,fx,fy,fz"
}

# This is a point
/^[0-9]/ {
    stripped = gsub(/[,()=:]/, "", $0)
    mass = $3
    x = $5
    y = $6
    z = $7
    fx = $9
    fy = $10
    fz = $11
    printf("%s,%s,%s,%s,%s,%s,%s\n", mass, x, y, z, fx, fy, fz)
}
