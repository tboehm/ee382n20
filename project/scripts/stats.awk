#!/usr/bin/awk -f

BEGIN {
    FS = ":"
    # csv_line = "device,algorithm,seed,num_points,leaf_points,ncrit,t_copy,t_const,t_alg,t_ref,speedup,mape"
    # csv_line = "algorithm,num_points,t_const,t_alg"
    csv_line = "device,algorithm,seed,num_points,leaf_points,ncrit,t_copy,t_const,t_alg,t_total"
}

/(barnes-|naive|tree-construction)/ {
    print csv_line
    csv_line = $1
}

# /^(Copy data|Construct tree|Approximation algorithm|Reference algorithm|Algorithm|Total):/ {
/^(Copy data|Construct tree|Algorithm|Total):/ {
    # "1508 usec" -> "1508"
    split($2, num, " ")
    csv_line = csv_line "," num[1]
}

/^(Relative speed|MAPE)/ {
    # " 3650%" -> " 3650"
    split($2, space_num, "%")
    # " 3650" -> "3650"
    split(space_num[1], num, " ")
    csv_line = csv_line "," num[1]
}

END {
    print csv_line
}
