#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import seaborn as sns

data = pd.read_csv('stats.csv')

# for stat in ['speedup', 'mape', 't_const', 't_alg']:
for stat in ['t_const', 't_alg']:
    plt.clf()
    sns.barplot(data=data, x='num_points', y=stat, hue='algorithm')
    plt.savefig(f'{stat}.png')


# Compute Barnes-updated speedup over Barnes-Hut.
barnes_hut = data[data.algorithm == 'barnes-hut']
barnes_updated = data[data.algorithm == 'barnes-updated']

records = list()
for (_, hut), (_, updated) in zip(barnes_hut.iterrows(), barnes_updated.iterrows()):
    speedup = hut.t_alg / updated.t_alg
    records.append([hut.num_points, speedup])
speedup = pd.DataFrame.from_records(records, columns=['num_points', 'updated_speedup'])

plt.clf()
g = sns.lineplot(data=speedup, x='num_points', y='updated_speedup')
g.set_xscale('log')
ymax = np.ceil(g.get_ylim()[1])
g.set_ylim(0, ymax)
plt.savefig('updated_speedup.png')
