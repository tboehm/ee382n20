#!/usr/bin/env bash

OUT_DIR=runs/animation

mkdir -p ${OUT_DIR}

for leaf_points in $(seq 1 8); do
    echo "Leaf points = $leaf_points"
    outf=${OUT_DIR}/${leaf_points}.txt
    ./nbody -A barnes-hut -P 42 -L $leaf_points --normal -v > $outf
    ./scripts/points.awk $outf > ${OUT_DIR}/points_${leaf_points}.csv
    ./scripts/nodes.awk $outf > ${OUT_DIR}/nodes_${leaf_points}.csv
    ./scripts/leaves.awk $outf > ${OUT_DIR}/leaves_${leaf_points}.csv
done
