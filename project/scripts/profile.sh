#!/usr/bin/env bash

USE_GPU=0
# USE_GPU=1

N_POINTS=( 5000 10000 20000 50000 100000 200000 500000 )
ALGORITHMS=( barnes-updated barnes-hut naive )
SEEDS=( 42 2112 271828 314159 )

LEAF_POINTS=100
NCRIT=200

##############################################
# No need to edit anything beyond this point #
##############################################

if [[ "$USE_GPU" == 0 ]]; then
    GPU_FLAG=""
    DEVICE="cpu"
else
    GPU_FLAG=--gpu
    DEVICE="gpu"
fi

if [[ "$SLURM_JOB_ID" == "" ]]; then
    # Running on the login node.
    OUT_DIR=runs
    mkdir -p $OUT_DIR
    TS=$(date +%Y-%m-%d_%H%M%S)
    OUT_FNAME=out_${TS}.txt
    STATS_FNAME=stats_${TS}.csv
else
    # Running on the compute node.
    OUT_DIR=runs/${SLURM_JOB_ID}
    OUT_FNAME=out.txt
    STATS_FNAME=stats.csv
fi

SCRIPT=$(realpath "$0")
SCRIPT_DIR=$(dirname "$SCRIPT")
if ! [ -d "$SCRIPT_DIR" ]; then
    printf '%b' "Unable to set SCRIPT_DIR\n"
    exit 1
fi

AWK_SCRIPT="${SCRIPT_DIR}/stats.awk"
if ! [ -x "$AWK_SCRIPT" ]; then
    printf '%b' "Unable to find awk script\n"
    exit 1
fi


OUT_FILE=${OUT_DIR}/${OUT_FNAME}
STATS_FILE=${OUT_DIR}/${STATS_FNAME}

if [[ "$SLURM_JOB_ID" == "" ]]; then
    # Running on the login node.
    rm -f "${OUT_DIR}/last-out"
    rm -f "${OUT_DIR}/last-stats"

    touch "$OUT_FILE"
    touch "$STATS_FILE"

    pushd "$OUT_DIR" > /dev/null || exit 1
    ln -s "${OUT_FNAME}" last-out
    ln -s "${STATS_FNAME}" last-stats
    popd > /dev/null || exit 1
fi

# 1. Run each algorithm on the specified device.
for seed in ${SEEDS[*]}; do
    for algorithm in ${ALGORITHMS[*]}; do
        for n_points in ${N_POINTS[*]}; do
            # The '0' is because there's no data copy time for CPU.
            printf '%b' "\n$DEVICE,$algorithm,$seed,$n_points,$LEAF_POINTS,$NCRIT,0\n"
            ./nbody \
                --seed $seed \
                --points $n_points \
                --algorithm $algorithm \
                --leaf-points $LEAF_POINTS \
                --ncrit $NCRIT \
                $GPU_FLAG
        done
    done
done | tee -a "$OUT_FILE"

# 2. Effect of -P and -L on GPU tree construction times.
for leaf_points in {1,10,100,1000}; do
    for n_points in {1000,5000,10000,50000,100000,500000,1000000,1500000}; do
        printf '%b' "\n$DEVICE,barnes-hut,2112,$n_points,$leaf_points,$NCRIT\n"
        ./nbody \
            --points $n_points \
            --algorithm barnes-hut \
            --leaf-points $leaf_points \
            $GPU_FLAG
    done
done | tee -a "$OUT_FILE"

# Convert raw output into a CSV:
#   a. Output raw escape sequences
#   b. Remove color
#   c. Process stats
#   d. Redirect to stats file
cat -v "$OUT_FILE" \
    | sed 's/\^\[\[[0-9;]*m//g' \
    | $AWK_SCRIPT \
          > "$STATS_FILE"
