#!/usr/bin/env python3

from itertools import product, combinations

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas as pd
import seaborn as sns

# Plotting defaults
mpl.style.use('seaborn-muted')
mpl.rcParams['savefig.dpi'] = 500
sns.set_style('whitegrid')
sns.set_context('paper')

# Colormap for forces.
cmap = sns.color_palette('rocket', as_cmap=True).colors

# Bounds for the point space.
BOUNDS = 1

# Directory with the CSV files.
RUN_DIR = 'runs/animation'


def to_colormap(column, scale='linear'):
    lo, hi = column.min(), column.max()
    if scale not in ['linear', 'log']:
        raise NotImplementedError(f"Scale '{scale}' is not supported")
    normalized = (column - lo) / (hi - lo)
    if scale == 'log':
        normalized = np.log10(10 * normalized + 1) / 1.05
        # normalized = np.log10(100 * normalized + 1) / 2
    indices = (255 * normalized).astype(int)
    colors = indices.apply(lambda index: cmap[index])
    return colors


def to_rgb(r_col, g_col, b_col, lo=None, hi=None):
    rgb = list()
    for column in (r_col, g_col, b_col):
        if lo is None:
            lo = column.min()
        if hi is None:
            hi = column.max()
        normalized = (column - lo) / (hi - lo)
        rgb.append(normalized)
    rgb = np.array(rgb).transpose(1, 0)
    return list(rgb)


def plot_points(points, ax):
    ax.scatter(points.x, points.y, points.z, c=points.color, s=40, marker='o', alpha=0.5)


def plot_node_cms(nodes, ax, alpha=0.1):
    ax.scatter(
        nodes.cmx, nodes.cmy, nodes.cmz, c=nodes.color, s=100 * nodes.mass, marker='o', alpha=alpha
    )


def plot_bounding_boxes(nodes, ax, alpha=0.05):
    for _, row in nodes.iterrows():
        grid_dim = abs(row.xr - row.xl)
        point_ranges = [(row.xl, row.xr), (row.yl, row.yr), (row.zl, row.zr)]
        rgb = row.color

        for p1, p2 in combinations(np.array(list(product(*point_ranges))), 2):
            if np.sum(np.abs(p1 - p2)) == grid_dim:
                ax.plot3D(*zip(p1, p2), color=rgb, alpha=alpha)


def plot_forces(points, ax):
    ax.quiver(
        points.x,
        points.y,
        points.z,
        points.fx,
        points.fy,
        points.fz,
        length=0.05,
        normalize=True,
        colors=points.force_color,
    )


for leaf_points in range(1, 8 + 1):
    print(f'Creating plot for {leaf_points = }')
    points = pd.read_csv(f'{RUN_DIR}/points_{leaf_points}.csv')
    nodes = pd.read_csv(f'{RUN_DIR}/nodes_{leaf_points}.csv')
    leaves = pd.read_csv(f'{RUN_DIR}/leaves_{leaf_points}.csv')

    nodes['color'] = to_rgb(nodes.xl, nodes.yl, nodes.zl, lo=-BOUNDS, hi=BOUNDS)

    leaves['color'] = to_rgb(leaves.xl, leaves.yl, leaves.zl, lo=-BOUNDS, hi=BOUNDS)

    points['color'] = to_rgb(points.x, points.y, points.z, lo=-BOUNDS, hi=BOUNDS)

    points['force_magnitude'] = np.sqrt(points.fx ** 2 + points.fy ** 2 + points.fz ** 2)
    points['force_color'] = to_colormap(points.force_magnitude, scale='log')

    fig = plt.figure(figsize=(10, 10))
    ax = Axes3D(fig)

    plot_points(points, ax)
    # plot_node_cms(nodes, ax, alpha=0.25)
    plot_bounding_boxes(leaves, ax, alpha=0.5)
    # plot_forces(points, ax)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.set_xlim(-BOUNDS, BOUNDS)
    ax.set_ylim(-BOUNDS, BOUNDS)
    ax.set_zlim(-BOUNDS, BOUNDS)

    plt.savefig(f'{RUN_DIR}/animation_{leaf_points}.png', transparent=True)
