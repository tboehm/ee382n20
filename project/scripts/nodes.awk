#!/usr/bin/awk -f

BEGIN {
    FS=" "
    print "node_id,child_points,mass,cmx,cmy,cmz,xl,xr,yl,yr,zl,zr"
}

# This is a node
/mass, cm/ {
    # Remove characters that make the formatting nice.
    gsub(/[:,()\[\]\+|\\]/, "", $0)
    # Remove leading spaces and hyphens.
    gsub(/^ *-+/, "", $0)
    node_id = $1
    child_points = $2
    mass = $4
    cmx = $7
    cmy = $8
    cmz = $9
    xl = $11
    xr = $12
    yl = $14
    yr = $15
    zl = $17
    zr = $18
    printf("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n",
           node_id, child_points, mass, cmx, cmy, cmz, xl, xr, yl, yr, zl, zr);
}
