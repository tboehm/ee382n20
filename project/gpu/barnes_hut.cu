#include "barnes_hut.cuh"

// Local, non-CUDA
//#include <math.h>
#include "points.h"

// Local, CUDA
#include "constants.cuh"
#include "tree.cuh"
#include "force.cuh"

extern __constant__ struct cuda_constants CudaConstants;

__device__ static inline double
distance_total(GNode* node, Point* particle)
{
    double x = node->cm.x - particle->x;
    double y = node->cm.y - particle->y;
    double z = node->cm.z - particle->z;
    return sqrt(x * x + y * y + z * z);
}

__device__ double3
calc_force_points(Point* point1, Point* point2)
{
    double3 force = {0.f, 0.f, 0.f};

    if(point1 == point2)
    {
        return force;
    }

    return compute_point_forces(point1, point2);
}

__device__ double3
get_force(GNode *node, Point *particle)
{
    if((long) node == -1 || node->num_points == 0)
    {
        double3 forces = {0.f, 0.f, 0.f};
        return forces;
    }
    else if(node->diameter < CudaConstants.theta * distance_total(node, particle))
    {
        // calculate as a single point
        return calc_force_points(&node->cm, particle);
    }
    else if(node->is_internal)
    {
        // Recurse down the sub tree
        double3 forces = {0.f, 0.f, 0.f};

        for(int i = 0; i < N_CHILDREN_CUDA; i++)
        {
            double3 new_forces = get_force(node->children[i], particle);
            forces.x += new_forces.x;
            forces.y += new_forces.y;
            forces.z += new_forces.z;
        }

        return forces;
    }
    else
    {
        // leaf node
        double3 forces = {0.f, 0.f, 0.f};
        
        for(int i = 0; i < node->num_points; i++)
        {
            Point *p2 = &node->points[i];
            if(p2 == particle)
                continue;
            double3 new_forces = calc_force_points(p2, particle);
            forces.x += new_forces.x;
            forces.y += new_forces.y;
            forces.z += new_forces.z;
        }

        return forces;
    }
}




__global__ void
barnes_hut_kernel(void)
{
    // Get id
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    // check if ID is within bounds
    if(id >= CudaConstants.num_points)
        return;

    // if it is, call get_force on it
    double3 forces = get_force(&CudaConstants.node_pool[0], &CudaConstants.points[id]);
    CudaConstants.points[id].forces.x = forces.x;
    CudaConstants.points[id].forces.y = forces.y;
    CudaConstants.points[id].forces.z = forces.z;
}
