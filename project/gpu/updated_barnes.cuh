#ifndef __UPDATED_BARNES_CUH__
#define __UPDATED_BARNES_CUH__

#ifdef __CUDACC__
#define CUDA_GLOBAL __global__
#else
#define CUDA_GLOBAL
#endif

CUDA_GLOBAL void
updated_barnes_kernel();

#endif // __UPDATED_BARNES_CUH__
