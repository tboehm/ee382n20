#ifndef __TREE_CUH__
#define __TREE_CUH__

#ifdef __CUDACC__
#define CUDA_GLOBAL __global__
#else
#define CUDA_GLOBAL
#endif

#include <stdint.h>

#include "points.h"
#include "tree.h"

#include "constants.cuh"

// Size of the per-thread interaction point buffer for Barnes-updated.
#define INTER_POINT_MAX 64

typedef uint64_t code_t;

typedef struct gpu_tree_node GNode;

struct gpu_tree_node {
    Point *inter_points[INTER_POINT_MAX];
    GNode *children[N_CHILDREN_CUDA];
    GNode *parent;
    Point *points; // pointer to range of points
    Point  cm;     ///< The center of mass for this node.
    double min_x;
    double max_x;
    double min_y;
    double max_y;
    double min_z;
    double max_z;
    double diameter;
    int    id;
    int    num_points;
    int    com_num_points; // number of points contained in COM
    int    height;
    int    is_internal;       ///< Whether this node is internal or a leaf.
    int    inter_point_count; ///< Number of interaction points this node has, currently.
    bool   contained_in_cell; ///< This node has an ancestor who is a cell.
    bool   is_cell;           ///< Whether this tree constitutes a cell.
};

// Currently unused (bottom-up approach to construction).
CUDA_GLOBAL void
tree_kernel_compute_child_indices(
    const GNode *parent,
    Point       *points,
    uint8_t     *indices,
    int          array_length
);

CUDA_GLOBAL void
tree_kernel_compute_morton_codes(
    Point  *points,
    int     num_points,
    code_t *codes
);

CUDA_GLOBAL void
tree_kernel_print_codes(
    code_t *morton_codes,
    Point  *points,
    int     num_points
);

CUDA_GLOBAL void
tree_kernel_generate_hierarchy(
    GNode   *node_pool,
    GNode   *root,
    code_t  *codes,
    int     *num_nodes,
    int      node_id,
    int      depth,
    char    *leaf_mask
);

CUDA_GLOBAL void
tree_kernel_generate_hierarchy_opt(
    GNode   *node_pool,
    GNode   *root,
    code_t  *codes,
    int      num_points,
    int     *num_nodes,
    char    *leaf_mask
);

CUDA_GLOBAL void
tree_kernel_get_leaves(
    char *leaf_mask, 
    int *leaf_prefix, 
    int *leaves,
    int num_nodes
);

CUDA_GLOBAL void
tree_kernel_compute_node_mass(
    GNode *node_pool, 
    int *leaves, 
    int num_leaves
);

CUDA_GLOBAL void
tree_kernel_print_tree(
    GNode *root
);

void
tree_generate_hierarchy_cpu(
    GNode   *d_node_pool,
    GNode   *d_root,
    code_t  *d_codes,
    int     *d_num_nodes,
    int      node_id,
    int      depth,
    Point   *h_points
);

#endif // __TREE_CUH__
