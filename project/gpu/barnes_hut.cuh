#ifndef __BARNES_HUT_CUH__
#define __BARNES_HUT_CUH__

#ifdef __CUDACC__
#define CUDA_GLOBAL __global__
#else
#define CUDA_GLOBAL
#endif

CUDA_GLOBAL void
barnes_hut_kernel();

#endif // __BARNES_HUT_CUH__
