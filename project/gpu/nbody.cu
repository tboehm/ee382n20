#include "nbody.cuh"

#include <string>
#include <algorithm>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <vector>

// CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_functions.h>
// #include <cudaProfiler.h>

// Thrust
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/sort.h>
#include <thrust/scan.h>
#include <thrust/execution_policy.h>
#include <cstdlib>

// Local, non-CUDA
#include "CycleTimer.h"
#include "points.h"
#include "tree.h"

// Local, CUDA
#include "barnes_hut.cuh"
#include "constants.cuh"
#include "error_check.cuh"
#include "force.cuh"
#include "tree.cuh"
#include "updated_barnes.cuh"

// TODO: Add num_points and various pointers (e.g. the Morton codes) to constant memory.
__constant__ struct cuda_constants CudaConstants;

extern int NCrit;
extern int MaxLeafPoints;

NBodyCuda::NBodyCuda(double theta, double gravity, double bounds, Point *points, int num_points)
{
    this->theta = theta;
    this->gravity = gravity;
    this->bounds = bounds;
    this->host_points = points;
    this->num_points = num_points;

    host_child_indices = NULL;
    cuda_child_indices = NULL;
    cuda_root = NULL;

    time_copy_host_device = 0.0;
    time_construct_tree = 0.0;
    time_compute_forces = 0.0;
    time_copy_device_host = 0.0;

    printf("Allocating arrays\n");

    ERROR_CHECK(cudaMalloc(&cuda_points, sizeof(Point) * num_points));
    ERROR_CHECK(cudaMalloc(&cuda_morton_codes, sizeof(code_t) * num_points));
    ERROR_CHECK(cudaMalloc(&cuda_node_pool, sizeof(GNode) * MAX_NODES));
    ERROR_CHECK(cudaMalloc(&cuda_num_nodes, sizeof(int)));
    ERROR_CHECK(cudaMalloc(&cuda_leaf_mask, sizeof(char) * MAX_NODES));  // num_points could be too large?
    ERROR_CHECK(cudaMalloc(&cuda_leaf_prefix, sizeof(int) * MAX_NODES));  // num_points could be too large?
    ERROR_CHECK(cudaMalloc(&cuda_leaves, sizeof(int) * MAX_NODES));  // num_points could be too large?

    printf("Copying constants to GPU\n");

    struct cuda_constants constants;

    constants.points = cuda_points;
    constants.node_pool = cuda_node_pool;
    constants.gravity = gravity;
    constants.theta = theta;
    constants.root_bounds = bounds;
    constants.ncrit = NCrit;
    constants.max_leaf_points = MaxLeafPoints;
    constants.num_points = num_points;

    ERROR_CHECK(cudaMemcpyToSymbol(CudaConstants, &constants, sizeof(constants)));
}

NBodyCuda::~NBodyCuda()
{
    if (cuda_root) {
        // ERROR_CHECK(cudaFree(cuda_root));
        ERROR_CHECK(cudaFree(cuda_points));
        ERROR_CHECK(cudaFree(cuda_morton_codes));
        ERROR_CHECK(cudaFree(cuda_node_pool));
        ERROR_CHECK(cudaFree(cuda_num_nodes));
        ERROR_CHECK(cudaFree(cuda_leaf_mask));
        ERROR_CHECK(cudaFree(cuda_leaf_prefix));
        ERROR_CHECK(cudaFree(cuda_leaves));
        // ERROR_CHECK(cudaFree(cuda_child_indices));
        // free(host_morton_codes);
        // free(host_child_indices);
        host_points = NULL;
    }
}

// Figure out in which child each point will end up.
void
NBodyCuda::compute_child_points(int num_points, GNode *cuda_parent, Point *cuda_points, uint8_t *cuda_indices)
{
    const int NumThreads = 4;

    tree_kernel_compute_child_indices<<<1, NumThreads>>>(
        cuda_parent,
        cuda_points,
        cuda_indices,
        num_points
    );
}

void
NBodyCuda::copy_points_to_gpu()
{
    double time_start = CycleTimer::currentSeconds();

    printf("Copying %d points to GPU\n", num_points);

    // Copy points to device.
    ERROR_CHECK(cudaMemcpy(cuda_points, host_points, sizeof(Point) * num_points, cudaMemcpyHostToDevice));

    // Put points/num_points in constant memory.
    struct cuda_constants constants;
    ERROR_CHECK(cudaMemcpyFromSymbol(&constants, CudaConstants, sizeof(constants)));
    constants.points = cuda_points;
    constants.num_points = num_points;
    ERROR_CHECK(cudaMemcpyToSymbol(CudaConstants, &constants, sizeof(constants)));

    ERROR_CHECK(cudaDeviceSynchronize());

    double time_end = CycleTimer::currentSeconds();

    this->time_copy_host_device = time_end - time_start;
}

void
NBodyCuda::copy_points_from_gpu(void)
{
    double time_start = CycleTimer::currentSeconds();

    printf("Copying %d points from GPU\n", num_points);

    ERROR_CHECK(cudaMemcpy(host_points, cuda_points, sizeof(Point) * num_points, cudaMemcpyDeviceToHost));

    double time_end = CycleTimer::currentSeconds();

    this->time_copy_device_host = time_end - time_start;
}

GNode *
NBodyCuda::generate_hierarchy(void)
{
    // Set up the root node and copy to the device.
    GNode root;
    root.id = 0;
    root.parent = NULL;
    root.min_x = -bounds;
    root.max_x = bounds;
    root.min_y = -bounds;
    root.max_y = bounds;
    root.min_z = -bounds;
    root.max_z = bounds;
    root.diameter = 2 * bounds;
    root.points = cuda_points;
    root.num_points = num_points;
    root.com_num_points = 0;
    memset(&root.cm, 0, sizeof(Point));
    memset(root.children, -1, sizeof(GNode *) * N_CHILDREN_CUDA);
    root.height = 0;
    root.is_internal = num_points > MaxLeafPoints;
    root.contained_in_cell = false;
    root.is_cell = num_points <= NCrit;

// #define CPU_GEN_TREE
#ifdef CPU_GEN_TREE
    num_nodes = 1 + N_CHILDREN_CUDA;
    ERROR_CHECK(cudaMemcpy(cuda_num_nodes, &num_nodes, sizeof(int), cudaMemcpyHostToDevice));
    ERROR_CHECK(cudaMemcpy(cuda_node_pool, &root, sizeof(GNode), cudaMemcpyHostToDevice));

    copy_points_from_gpu();
    tree_generate_hierarchy_cpu(
        cuda_node_pool,
        cuda_node_pool,  // first node in node_pool is root
        cuda_morton_codes,
        cuda_num_nodes,
        1,  // start allocation at node 1
        0,
        host_points
    );
    ERROR_CHECK(cudaMemcpy(&num_nodes, cuda_num_nodes, sizeof(int), cudaMemcpyDeviceToHost));
    ERROR_CHECK(cudaDeviceSynchronize());
#else
    double begin, end;
    begin = CycleTimer::currentSeconds();

    /*
    tree_kernel_generate_hierarchy<<<1, N_CHILDREN_CUDA>>>(
        cuda_node_pool,
        cuda_node_pool,  // first node in node_pool is root
        cuda_morton_codes,
        cuda_num_nodes,
        1,  // start allocation at node 1
        0,
        cuda_leaf_mask
    );
    */
    num_nodes = 1;
    ERROR_CHECK(cudaMemcpy(cuda_num_nodes, &num_nodes, sizeof(int), cudaMemcpyHostToDevice));
    ERROR_CHECK(cudaMemcpy(cuda_node_pool, &root, sizeof(GNode), cudaMemcpyHostToDevice));

    tree_kernel_generate_hierarchy_opt<<<1, THREADS_PER_BLOCK>>>(
        cuda_node_pool,
        cuda_node_pool,  // first node in node_pool is root
        cuda_morton_codes,
        num_points,
        cuda_num_nodes,
        cuda_leaf_mask
    );
    ERROR_CHECK(cudaDeviceSynchronize());
    ERROR_CHECK(cudaMemcpy(&num_nodes, cuda_num_nodes, sizeof(int), cudaMemcpyDeviceToHost));

    end = CycleTimer::currentSeconds();
    printf("tree (%d nodes) generated in %.4f seconds\n", num_nodes, end-begin);
    begin = end;

    // get list of leaves
    thrust::exclusive_scan(thrust::device, cuda_leaf_mask, cuda_leaf_mask + num_nodes, cuda_leaf_prefix, 0);
    tree_kernel_get_leaves<<<GET_NUM_BLOCKS(num_nodes), THREADS_PER_BLOCK>>>(cuda_leaf_mask, cuda_leaf_prefix, cuda_leaves, num_nodes);
    ERROR_CHECK(cudaDeviceSynchronize());

    char last_node_is_leaf;
    ERROR_CHECK(cudaMemcpy(&last_node_is_leaf, &(cuda_leaf_mask[num_nodes-1]), sizeof(char), cudaMemcpyDeviceToHost));
    ERROR_CHECK(cudaMemcpy(&num_leaves, &(cuda_leaf_prefix[num_nodes-1]), sizeof(int), cudaMemcpyDeviceToHost));
    num_leaves += last_node_is_leaf;

    end = CycleTimer::currentSeconds();
    printf("leaves list (%d leaves) computed in %.4f seconds\n", num_leaves, end-begin);
    begin = end;

    printf("num_nodes %d num_leaves %d\n", num_nodes, num_leaves);
    tree_kernel_compute_node_mass<<<GET_NUM_BLOCKS(num_leaves), THREADS_PER_BLOCK>>>(cuda_node_pool, cuda_leaves, num_leaves);

    ERROR_CHECK(cudaDeviceSynchronize());
    end = CycleTimer::currentSeconds();
    printf("mass computed in %.4f seconds\n", end-begin);
    begin = end;
#endif

    // Now that the hierarchy is generated, we know how many nodes there are. Put that number in
    // constant memory.
    struct cuda_constants constants;
    ERROR_CHECK(cudaMemcpyFromSymbol(&constants, CudaConstants, sizeof(constants)));
    constants.num_nodes = num_nodes;
    ERROR_CHECK(cudaMemcpyToSymbol(CudaConstants, &constants, sizeof(constants)));

    printf("Kernel complete\n");

    return cuda_node_pool;
}

void
NBodyCuda::construct_tree(void)
{
    printf("Constructing tree: begin\n");

    double start = CycleTimer::currentSeconds(), end;

    // Compute the Morton codes.
    // host_morton_codes = (code_t *)malloc(sizeof(code_t) * num_points);
    int num_blocks = GET_NUM_BLOCKS(num_points);
    tree_kernel_compute_morton_codes<<<num_blocks, THREADS_PER_BLOCK>>>(cuda_points, num_points, cuda_morton_codes);
    ERROR_CHECK(cudaDeviceSynchronize());

    // Sort child points
    thrust::device_ptr<Point> thrust_dev_points(cuda_points);
    // thrust::device_ptr<uint8_t> thrust_dev_keys(cuda_child_indices);
    thrust::device_ptr<code_t> thrust_dev_keys(cuda_morton_codes);

    thrust::sort_by_key(thrust_dev_keys, thrust_dev_keys + num_points, thrust_dev_points);
    // thrust::sort(thrust_dev_keys, thrust_dev_keys + num_points);
    ERROR_CHECK(cudaDeviceSynchronize());

    end = CycleTimer::currentSeconds();
    printf("morton code completed in %g seconds\n", end - start);
    // print_codes<<<1, 1>>>(cuda_morton_codes, cuda_points, num_points);
    // ERROR_CHECK(cudaDeviceSynchronize());

    // Compute the indices for each child (to verify Morton code computation).
    // host_child_indices = (uint8_t *)malloc(sizeof(uint8_t) * num_points);
    // ERROR_CHECK(cudaMalloc(&cuda_child_indices, sizeof(uint8_t) * num_points));
    // compute_child_points(num_points, cuda_root, cuda_points, cuda_child_indices);
    // ERROR_CHECK(cudaDeviceSynchronize());

    // Copy things back to the host.
    // ERROR_CHECK(cudaMemcpy(host_child_indices, cuda_child_indices, sizeof(uint8_t) * num_points, cudaMemcpyDeviceToHost));
    // ERROR_CHECK(cudaMemcpy(host_morton_codes, cuda_morton_codes, sizeof(code_t) * num_points, cudaMemcpyDeviceToHost));
    // ERROR_CHECK(cudaMemcpy(points, cuda_points, sizeof(Point) * num_points, cudaMemcpyDeviceToHost));

    // for (int i = 0; i < num_points; i++) {
    //     printf("Point %d: child %d, code 0x%.8x\n", i, host_child_indices[i], host_morton_codes[i]);
    // }

    /*
    size_t limit;
    cudaDeviceGetLimit(&limit, cudaLimitDevRuntimeSyncDepth); 
    printf("depth limit %d\n", limit);

    cudaError_t code;
    size_t max_depth = 10;  // max is default 24
    do {
      code = cudaDeviceSetLimit(cudaLimitDevRuntimeSyncDepth, max_depth); // set recursion depth to max
      max_depth--;
    } while (code != cudaSuccess);

    cudaDeviceGetLimit(&limit, cudaLimitDevRuntimeSyncDepth); 
    printf("updated depth limit %d\n", limit);
    */

    cuda_root = generate_hierarchy();

    ERROR_CHECK(cudaDeviceSynchronize());

    end = CycleTimer::currentSeconds();

    printf("%d points in %g seconds (w/ %d children)\n", num_points, end - start, N_CHILDREN_CUDA);

    // FIXME: increase stack size temporarily to print out tree
    /*
    cudaDeviceSetLimit(cudaLimitStackSize, 1024*8);
    printf("tree: \n");
    tree_kernel_print_tree<<<1, 1>>>(cuda_root);
    ERROR_CHECK(cudaDeviceSynchronize());
    */

    printf("Constructing tree: end\n");

    time_construct_tree = end - start;
}

__global__ void
kernel_compute_forces_naive(void)
{
    int point_idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (point_idx >= CudaConstants.num_points) {
        return;
    }

    Point *point = &CudaConstants.points[point_idx];

    double3 forces = {0.f, 0.f, 0.f};

    for (int i = 0; i < CudaConstants.num_points; i++) {
        if (i == point_idx) {
            continue;
        }

        Point *exerting_point = &CudaConstants.points[i];

        double3 new_forces = compute_point_forces(exerting_point, point);

        forces.x += new_forces.x;
        forces.y += new_forces.y;
        forces.z += new_forces.z;
    }

    point->forces.x = forces.x;
    point->forces.y = forces.y;
    point->forces.z = forces.z;

    // printf("[%d] %d = %.2f %.2f %.2f\n", point_idx, point->id, point->x, point->y, point->z); 
    // printf("[%d] Forces: (%g, %g, %g)\n", point_idx, point->forces.x, point->forces.y, point->forces.z);
}

void
NBodyCuda::compute_forces_barnes_hut(void)
{
    double time_start = CycleTimer::currentSeconds();

    printf("Running original Barnes-Hut n-body approximation algorithm\n");

    // One thread per node. Ultimately there will be far fewer since we only need a thread per cell,
    // but we don't have those grouped separately.
    int num_blocks = GET_NUM_BLOCKS(num_points);
    barnes_hut_kernel<<<num_blocks, THREADS_PER_BLOCK>>>();

    ERROR_CHECK(cudaDeviceSynchronize());

    double time_end = CycleTimer::currentSeconds();

    this->time_compute_forces = time_end - time_start;
}

void
NBodyCuda::compute_forces_updated_barnes(void)
{
    double time_start = CycleTimer::currentSeconds();

    printf("Running Barnes's 1990 n-body approximation algorithm\n");

    // One thread per node. Ultimately there will be far fewer since we only need a thread per cell,
    // but we don't have those grouped separately.
    //
    // Using a small number of blocks per thread means that there is less serialization when we call
    // cudaDeviceSynchronize() to wait for the children to complete.
    const int ThreadsPerBlock = 8;
    int num_blocks = (num_nodes + ThreadsPerBlock - 1) / ThreadsPerBlock;

    updated_barnes_kernel<<<num_blocks, ThreadsPerBlock>>>();

    ERROR_CHECK(cudaDeviceSynchronize());

    double time_end = CycleTimer::currentSeconds();

    this->time_compute_forces = time_end - time_start;
}

void
NBodyCuda::compute_forces_naive(void)
{
    double time_start = CycleTimer::currentSeconds();

    printf("Running brute force (N^2) n-body algorithm\n");

    int num_blocks = GET_NUM_BLOCKS(num_points);

    kernel_compute_forces_naive<<<num_blocks, THREADS_PER_BLOCK>>>();

    ERROR_CHECK(cudaDeviceSynchronize());

    double time_end = CycleTimer::currentSeconds();

    this->time_compute_forces = time_end - time_start;
}

long
NBodyCuda::get_total_usecs(void)
{
    double total_seconds = time_copy_host_device
                           + time_construct_tree
                           + time_compute_forces
                           + time_copy_device_host;

    return (long)(total_seconds * 1e6);
}

void
NBodyCuda::print_times(void)
{
    printf("Copy data: %.3f sec\n", time_copy_host_device + time_copy_device_host);
    printf("Construct tree: %.3f sec\n", time_construct_tree);
    printf("Algorithm: %.3f sec\n", time_compute_forces);
    printf("Total: %.3f sec\n", (double) get_total_usecs() / 1e6);
}
