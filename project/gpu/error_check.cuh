#ifndef ERROR_CHECK_CUH
#define ERROR_CHECK_CUH

#define ERROR_CHECK(func)                                                      \
  { gpuAssert((func), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char *file, int line,
                      bool abort = true) {
  if (code != cudaSuccess) {
    fprintf(stderr, "CUDA error: %s %s %d\n", cudaGetErrorString(code), file,
            line);
    if (abort)
      exit(code);
  }
}

#define GPU_ERROR_CHECK(func)                                                  \
  { gpuAssertDev((func), __FILE__, __LINE__); }

__device__
inline void gpuAssertDev(cudaError_t code, const char *file, int line,
                      bool abort = true) {
  if (code != cudaSuccess) {
    printf("CUDA error: %s %s %d\n", cudaGetErrorString(code), file,
            line);
    if (abort)
      assert(0);
  }
}

#endif
