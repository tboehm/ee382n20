#include "force.cuh"

// Local, non-CUDA
#include "points.h"

// Local, CUDA
#include "constants.cuh"

// From nbody.cu
extern __constant__ struct cuda_constants CudaConstants;

// Compute the force that p1 exerts on p2
__device__ double3
compute_point_forces(Point *p1, Point *p2)
{
    double3 dist;

    dist.x = p1->x - p2->x;
    dist.y = p1->y - p2->y;
    dist.z = p1->z - p2->z;

    double dist_total_sq = dist.x * dist.x + dist.y * dist.y + dist.z * dist.z;
    double force_total = CudaConstants.gravity * p1->mass * p2->mass / dist_total_sq;
    double3 force;

    force.x = force_total * dist.x / sqrt(dist_total_sq);
    force.y = force_total * dist.y / sqrt(dist_total_sq);
    force.z = force_total * dist.z / sqrt(dist_total_sq);

    return force;
}
