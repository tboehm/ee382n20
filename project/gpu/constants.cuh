#ifndef __CONSTANTS_CUH__
#define __CONSTANTS_CUH__

#include "points.h" // Point
#include "tree.h"   // Node

// TODO: Add Morton codes

#define THREADS_PER_BLOCK 1024
// ceil(num_threads / THREADS_PER_BLOCK)
#define GET_NUM_BLOCKS(num_threads) ((num_threads) + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK

#define MAX_NODES (1 << 22)
#define N_PARTITION 2   // number of partition in one dimension
#define N_CHILDREN_CUDA (N_PARTITION*N_PARTITION*N_PARTITION)

typedef struct gpu_tree_node GNode; // forward declaration of GNode from tree.cuh

struct cuda_constants {
    Point *points;
    GNode *node_pool;
    double gravity;
    double theta;
    double root_bounds;
    int    ncrit;
    int    max_leaf_points;
    int    num_points;
    int    num_nodes;
};

#endif // __CONSTANTS_CUH__
