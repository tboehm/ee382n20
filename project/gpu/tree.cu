#include "tree.cuh"

#include <assert.h>
#include <inttypes.h> // PRIx64
#include <stdio.h>    // printf

// Local, non-CUDA
#include "points.h"
#include "tree.h"

// Local, CUDA
#include "constants.cuh"
#include "error_check.cuh"

// From nbody.cu
extern __constant__ struct cuda_constants CudaConstants;

// From main.cpp
extern int NCrit;
extern int MaxLeafPoints;


// Currently unused (bottom-up tree construction).
__global__ void
tree_kernel_compute_child_indices(const GNode *parent, Point *points, uint8_t *indices, int array_length)
{
    int start_idx = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x;

    double new_node_width = parent->diameter / 2.f;

    for (int i = start_idx; i < array_length; i += stride) {
        Point  *point = &points[i];
        uint8_t right = point->x > parent->min_x + new_node_width;
        uint8_t top = point->y > parent->min_y + new_node_width;
        uint8_t upper = point->z > parent->min_z + new_node_width;

        indices[i] = (right) | (top << 1) | (upper << 2);
    }
}

// Clamp x between lower and upper (inclusive).
__device__ double
fclamp(double x, double lower, double upper)
{
    return fmax(lower, fmin(upper, x));
}

// Expands a 10-bit integer into 30 bits
// by inserting 2 zeros after each bit.
__device__ uint32_t
expand_bits_10(uint32_t v)
{
    v = (v * 0x00010001u) & 0xFF0000FFu;
    v = (v * 0x00000101u) & 0x0F00F00Fu;
    v = (v * 0x00000011u) & 0xC30C30C3u;
    v = (v * 0x00000005u) & 0x49249249u;
    return v;
}

// Expand a 16-bit integer into 46 bits.
__device__ uint64_t
expand_bits_16(uint64_t v)
{
    // 16 bits instead of just 10
    v = (v ^ (v << 16)) & 0x00FF0000FF0000FFul;
    v = (v ^ (v << 8)) & 0xF00F00F00F00F00Ful;
    v = (v ^ (v << 4)) & 0x30C30C30C30C30C3ul;
    v = (v ^ (v << 2)) & 0x9249249249249249ul;
    return v;
}

// Scale coordinates to fit in the unit box.
__device__ double
scale_to_unit_coordinates(double x, double scale_factor)
{
    return scale_factor * x / CudaConstants.root_bounds + scale_factor;
}

// Generate a 30-bit morton code from a point's 3D coordinates.
__device__ uint32_t
morton_30(Point *point)
{
    double hi = 1023.0f;
    double scale_factor = hi / 2;

    double x = fclamp(scale_to_unit_coordinates(point->x, scale_factor), 0.0f, hi);
    double y = fclamp(scale_to_unit_coordinates(point->y, scale_factor), 0.0f, hi);
    double z = fclamp(scale_to_unit_coordinates(point->z, scale_factor), 0.0f, hi);

    uint32_t xx = expand_bits_10((uint32_t)x);
    uint32_t yy = expand_bits_10((uint32_t)y);
    uint32_t zz = expand_bits_10((uint32_t)z);

    // return (xx << 2) | (yy << 1) | zz;
    return (zz << 2) | (yy << 1) | xx;
}

// Generate a 48-bit morton code from a point's 3D coordinates.
__device__ uint64_t
morton_48(Point *point)
{
    double hi = 65536.0f;
    double scale_factor = hi / 2;

    double x = fclamp(scale_to_unit_coordinates(point->x, scale_factor), 0.0f, hi);
    double y = fclamp(scale_to_unit_coordinates(point->y, scale_factor), 0.0f, hi);
    double z = fclamp(scale_to_unit_coordinates(point->z, scale_factor), 0.0f, hi);

    uint64_t xx = expand_bits_16((uint64_t)x);
    uint64_t yy = expand_bits_16((uint64_t)y);
    uint64_t zz = expand_bits_16((uint64_t)z);

    // printf("%.2f %.2f %.2f -> %.2f %.2f %.2f\n", point->x, point->y, point->z, x, y, z);
    // printf("xx: 0x%" PRIx64 ", yy 0x%" PRIx64 ", zz 0x%" PRIx64 "\n", xx, yy, zz);

    // return (xx << 2) | (yy << 1) | zz;
    return (zz << 2) | (yy << 1) | xx;
}

__global__ void
tree_kernel_compute_morton_codes(Point *points, int num_points, code_t *codes)
{
  /*
    int start_idx = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x;

    for (int i = start_idx; i < num_points; i += stride) {
        Point *point = &points[i];
        // point->morton_code = morton_30(point);
        point->morton_code = morton_48(point);

        codes[i] = point->morton_code;
    }
  */
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx < num_points) {
      Point *point = &points[idx];
      codes[idx] = morton_48(point);
    }
}

__global__ void
tree_kernel_print_codes(code_t *morton_codes, Point *points, int num_points)
{
    for (int i = 0; i < num_points; i++) {
        printf("[%d] %d = 0x%" PRIx64 "\n", i, points[i].id, morton_codes[i]);
    }
}

__device__ __host__ static inline int
get_prefix(code_t code, int depth) {
#if N_PARTITION == 2
    return (code >> (15-depth)*3) & 0x07;
#elif N_PARTITION == 4
    return (code >> (7-depth)*6) & 0x3f;
#else
    static_assert(0, "Unsupported partition!");
#endif
}

__device__ __host__ static int
find_first(code_t *codes, int size, int target, int depth) {
    int low = 0, high = size - 1;
    int idx = -1;
    while (low <= high) {
        int mid = (low + high) / 2;
        int prefix = get_prefix(codes[mid], depth);
        if (target < prefix) {
            high = mid - 1;
        } else if (target > prefix) {
            low = mid + 1;
        } else {
            idx = mid;
            high = mid - 1;
        }
    }
    return idx;
}

__device__ __host__ static int
find_last(code_t *codes, int size, int target, int depth) {
    int low = 0, high = size - 1;
    int idx = -1;
    while (low <= high) {
        int mid = (low + high) / 2;
        int prefix = get_prefix(codes[mid], depth);
        if (target < prefix) {
            high = mid - 1;
        } else if (target > prefix) {
            low = mid + 1;
        } else {
            idx = mid;
            low = mid + 1;
        }
    }
    return idx;
}

__device__ __host__ static void
compute_node_mass(GNode *node)
{
    node->cm.id = -node->id;
    // memset(&node->cm, 0, sizeof(Point)); unnecessary since node_pool init to 0
    
    if (node->is_internal) {
        // Only look at the children
        for (int i = 0; i < N_CHILDREN_CUDA; i++) {
            // printf("[%d] child %p\n", node->id, node->children[i]);
            Point *p = &(node->children[i]->cm);
            double m = p->mass;
            node->cm.mass += m;
            node->cm.x += p->x * m;
            node->cm.y += p->y * m;
            node->cm.z += p->z * m;
        }
    } else {
        // Look at all of the points
        for (int i = 0; i < node->num_points; i++) {
            Point *p = &(node->points[i]);
            double m = p->mass;
            node->cm.mass += m;
            node->cm.x += p->x * m;
            node->cm.y += p->y * m;
            node->cm.z += p->z * m;
        }
    }

    if (node->cm.mass != 0.f) {
        // For both cases, we need to divide by total mass (as long as there *is* mass).
        node->cm.x /= node->cm.mass;
        node->cm.y /= node->cm.mass;
        node->cm.z /= node->cm.mass;
    } else {
        node->cm.x = 0.f;
        node->cm.y = 0.f;
        node->cm.z = 0.f;
    }
}

// share processing code between cpu and gpu version
__device__ __host__ static inline GNode *
process_node(int idx, GNode *node_pool, GNode *root, code_t *codes, int id, int depth, double x_inc, double y_inc, double z_inc, 
    int ncrit, int max_leaf_points)
{
    // each index corresponds to the start of the split
    int split_start = find_first(codes, root->num_points, idx, depth);
    int split_end = find_last(codes, root->num_points, idx, depth) + 1;

    // for (int i = 0; i < root->num_points; i++) {
    //   int prefix = (codes[i] >> (15-depth)*3) & 0x07;
    //   printf("[%d] %d = 0x%" PRIx64 " prefix=%d\n", i, root->points[i].id, codes[i], prefix);
    // }
    // printf("\n");

    GNode *node = &node_pool[id];
    root->children[idx] = node;
    node->id = id;
    node->parent = root;
    node->diameter = root->diameter / (double) N_PARTITION;
    node->height = root->height + 1;
    memset(node->children, -1, sizeof(GNode *) * N_CHILDREN_CUDA);

    // init COM
    memset(&(node->cm), 0, sizeof(Point)); 
    node->com_num_points = 0;

    // int x_idx = idx % N_PARTITION;
    // int y_idx = (idx / N_PARTITION) % N_PARTITION;
    // int z_idx = idx / (N_PARTITION * N_PARTITION);
#if N_PARTITION == 2
    int x_idx = idx & 0x01;
    int y_idx = (idx & 0x02) >> 1;
    int z_idx = (idx & 0x04) >> 2;
#elif N_PARTITION == 4
    int x_idx = ((idx & 0x08) >> 2) | (idx & 0x01);
    int y_idx = ((idx & 0x10) >> 3) | ((idx & 0x02) >> 1);
    int z_idx = ((idx & 0x20) >> 4) | ((idx & 0x04) >> 2);
#else
    static_assert(0, "Unsupported partition!");
#endif

    // X dimension (left/right)
    // Left side (nodes 0, 2, 4, 6)
    node->min_x = root->min_x + x_idx * x_inc;
    node->max_x = root->min_x + (x_idx + 1) * x_inc;

    // Y dimension (bottom/top)
    node->min_y = root->min_y + y_idx * y_inc;
    node->max_y = root->min_y + (y_idx + 1) * y_inc;

    // Z dimension (lower/upper)
    node->min_z = root->min_z + z_idx * z_inc;
    node->max_z = root->min_z + (z_idx + 1) * z_inc;

    node->num_points = split_start == -1 ? 0 : split_end - split_start;
    node->points = root->points + split_start;
    node->contained_in_cell = root->is_cell || root->contained_in_cell;
    node->is_cell = (!node->contained_in_cell)
                    && (node->num_points <= ncrit)
                    && (node->num_points > 0);
    node->is_internal = node->num_points > max_leaf_points || depth == (48 / (3 * (N_PARTITION / 2)) - 1);

    return node;
}

// Generate a hierarchy for a tree.
__global__ void
tree_kernel_generate_hierarchy(GNode *node_pool,
    GNode                     *root,
    code_t                    *codes,
    int                       *num_nodes,
    int                        node_id,
    int                        depth,
    char                      *leaf_mask)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= N_CHILDREN_CUDA)
      return;

    double x_inc = (root->max_x - root->min_x) / N_PARTITION;
    double y_inc = (root->max_y - root->min_y) / N_PARTITION;
    double z_inc = (root->max_z - root->min_z) / N_PARTITION;

    int id = node_id + idx;
    GNode *node = process_node(idx, node_pool, root, codes, id, depth, x_inc, y_inc, z_inc, 
        CudaConstants.ncrit, CudaConstants.max_leaf_points); 

    leaf_mask[node->id] = !node->is_internal && node->num_points > 0;  // must contain points to be a valid leaf

    // printf("[%d] d=%d n=%d split %d, %d\n", id, depth, node->num_points, split_start, split_end);
    // recurse
    if (node->is_internal) {
        int id = atomicAdd(num_nodes, N_CHILDREN_CUDA); // allocate space for child nodes
        assert(*num_nodes < MAX_NODES);
        tree_kernel_generate_hierarchy<<<1, N_CHILDREN_CUDA>>>(
            node_pool,
            node,
            codes + (node->points - root->points),
            num_nodes,
            id,
            depth + 1,
            leaf_mask
        );
    } 
    
    if (!node->is_internal) {
      for (int i = 0; i < node->num_points; i++) {
        if (!(node->points[i].x >= node->min_x && node->points[i].x <= node->max_x))
          printf("[%d] x bounds mismatch! %.2f in [%.2f, %.2f]\n", node->id, node->points[i].x, node->min_x, node->max_x);
        if (!(node->points[i].y >= node->min_y && node->points[i].y <= node->max_y))
          printf("[%d] y bounds mismatch! %.2f in [%.2f, %.2f]\n", node->id, node->points[i].y, node->min_y, node->max_y);
        if (!(node->points[i].z >= node->min_z && node->points[i].z <= node->max_z))
          printf("[%d] z bounds mismatch! %.2f in [%.2f, %.2f]\n", node->id, node->points[i].z, node->min_z, node->max_z);
      }
    }
}

// Generate a hierarchy for a tree.
__global__ void
tree_kernel_generate_hierarchy_opt(GNode *node_pool,
    GNode                     *root,
    code_t                    *codes,
    int                        num_points,
    int                       *num_nodes,
    char                      *leaf_mask)
{
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    GNode *orig_root = root;

    for (int idx = tid; idx < num_points ; idx += THREADS_PER_BLOCK) {
        root = orig_root;
        int depth = 0;
        code_t *node_codes = codes;
        while (root->is_internal) {
            int node_idx = get_prefix(codes[idx], depth);
            GNode *node = root->children[node_idx];

            // printf("[%d] prefix %d\n", idx, node_idx);
            // allocate node
            if ((long) node == -1) {
                int old = atomicCAS((int *)&(root->children[node_idx]), -1, idx);
                // printf("[%d] old %d\n", idx, old);
                // thread won
                if (old == -1) {
                    double x_inc = (root->max_x - root->min_x) / N_PARTITION;
                    double y_inc = (root->max_y - root->min_y) / N_PARTITION;
                    double z_inc = (root->max_z - root->min_z) / N_PARTITION;

                    int id = atomicAdd(num_nodes, 1);
                    assert(*num_nodes < MAX_NODES);
                    node = process_node(node_idx, node_pool, root, node_codes, id, depth, x_inc, y_inc, z_inc, 
                        CudaConstants.ncrit, CudaConstants.max_leaf_points); 

                    leaf_mask[id] = !node->is_internal && node->num_points > 0;  // must contain points to be a valid leaf
                    // printf("[%d] id=%d d=%d n=%d split %d, %d\n", idx, id, depth, node->num_points, split_start, split_end);
                }
            }
            __syncthreads();
            node = root->children[node_idx];

            int code_split = node->points - root->points;
            root = node;
            node_codes += code_split;
            depth++;
        }
    }
}

__global__ void
tree_kernel_get_leaves(char *leaf_mask, int *leaf_prefix, int *leaves, int num_nodes) 
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= num_nodes || !leaf_mask[idx])
        return;

    leaves[leaf_prefix[idx]] = idx; 
}

__global__ void
tree_kernel_compute_node_mass(GNode *node_pool, int *leaves, int num_leaves)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (idx >= num_leaves)
        return;

    GNode *node = node_pool + leaves[idx];

    memset(&(node->cm), 0, sizeof(Point)); 
    node->cm.id = -node->id;
    // Look at all of the points
    for (int i = 0; i < node->num_points; i++) {
        Point *p = &(node->points[i]);
        double m = p->mass;
        node->cm.mass += m;
        node->cm.x += p->x * m;
        node->cm.y += p->y * m;
        node->cm.z += p->z * m;
    }

    if (node->cm.mass != 0.f) {
        // For both cases, we need to divide by total mass (as long as there *is* mass).
        node->cm.x /= node->cm.mass;
        node->cm.y /= node->cm.mass;
        node->cm.z /= node->cm.mass;
    } else {
        node->cm.x = 0.f;
        node->cm.y = 0.f;
        node->cm.z = 0.f;
    }

    int com_num_points;
    GNode *parent = node->parent;
    while (parent) {
        Point *p = &(node->cm);
        double m = p->mass;
        atomicAdd(&parent->cm.mass, m);
        atomicAdd(&parent->cm.x, p->x * m);
        atomicAdd(&parent->cm.y, p->y * m);
        atomicAdd(&parent->cm.z, p->z * m);
        com_num_points = atomicAdd(&parent->com_num_points, node->num_points) + node->num_points;
        // if not last to contribute to parent COM, done
        if (com_num_points != parent->num_points)
            break;

        // last thread to contribute to COM
        node = parent;
        parent = node->parent;

        node->cm.id = -node->id;
        if (node->cm.mass != 0.f) {
            // For both cases, we need to divide by total mass (as long as there *is* mass).
            node->cm.x /= node->cm.mass;
            node->cm.y /= node->cm.mass;
            node->cm.z /= node->cm.mass;
        } else {
            node->cm.x = 0.f;
            node->cm.y = 0.f;
            node->cm.z = 0.f;
        }
    }
}


// Device function to print a tree in GPU memory.
__device__ __host__ void
print_tree_helper(GNode *root)
{
    if (root->num_points == 0)
        return;
    if (root->is_internal) {
        for (int i = 0; i < root->height; i++) {
            printf("  ");
        }
        printf("\\-+");
    } else {
        for (int i = 0; i < root->height; i++) {
            printf("  ");
        }
        printf("|--");
    }

    // if (root->num_points != 0) {
    //     int point_id = root->points[0].id;
    //     printf("%4d :: %d points (first point %d)\n", root->id, root->num_points, point_id);
    // } else {
    //     printf("%4d :: %d points\n", root->id, root->num_points);
    // }

    printf(
        "%4d :: %d points, %g mass, cm (%g, %g, %g): X [%g, %g], Y [%g, %g], Z [%g, %g]\n",
        root->id,
        root->num_points,
        root->cm.mass,
        root->cm.x,
        root->cm.y,
        root->cm.z,
        root->min_x,
        root->max_x,
        root->min_y,
        root->max_y,
        root->min_z,
        root->max_z
    );

    for (int i = 0; i < N_CHILDREN_CUDA; i++) {
        GNode *child = root->children[i];
        if ((long) child != -1) {
            print_tree_helper(child);
        }
    }
}

// Print a tree in GPU memory.
__global__ void
tree_kernel_print_tree(GNode *root)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx != 0) {
        return;
    }

    print_tree_helper(root);
}

void
tree_generate_hierarchy_cpu_helper(GNode *node_pool,
    GNode                                *root,
    code_t                               *codes,
    int                                  *num_nodes,
    int                                   node_id,
    int                                   depth)
{
    double x_inc = (root->max_x - root->min_x) / N_PARTITION;
    double y_inc = (root->max_y - root->min_y) / N_PARTITION;
    double z_inc = (root->max_z - root->min_z) / N_PARTITION;

    for (int idx = 0; idx < N_CHILDREN_CUDA; idx++) {

        int id = node_id + idx;
        GNode *node = process_node(idx, node_pool, root, codes, id, depth, x_inc, y_inc, z_inc,
            NCrit, MaxLeafPoints); 

        // printf("[%d] d=%d n=%d split %d, %d\n", id, depth, node->num_points, split_start, split_end);
        // recurse
        if (node->is_internal) {
            int id = *num_nodes;
            *num_nodes += N_CHILDREN_CUDA; // allocate space for child nodes
            assert(*num_nodes < MAX_NODES);
            tree_generate_hierarchy_cpu_helper(
                node_pool,
                node,
                codes + (node->points - root->points),
                num_nodes,
                id,
                depth + 1
            );
        }

        compute_node_mass(node);
    }
}

// wrapper to allocate host memory
void
tree_generate_hierarchy_cpu(GNode *d_node_pool,
    GNode                         *d_root,
    code_t                        *d_codes,
    int                           *d_num_nodes,
    int                            node_id,
    int                            depth,
    Point                         *h_points)
{
    GNode *node_pool = (GNode *) malloc(sizeof(GNode) * MAX_NODES);
    ERROR_CHECK(cudaMemcpy(node_pool, d_node_pool, sizeof(GNode), cudaMemcpyDeviceToHost));

    GNode *root = node_pool;
    Point *d_points = root->points;
    root->points = h_points;

    code_t *codes = (code_t *) malloc(sizeof(code_t) * root->num_points);
    ERROR_CHECK(cudaMemcpy(codes, d_codes, sizeof(code_t) * root->num_points, cudaMemcpyDeviceToHost));

    int num_nodes;
    ERROR_CHECK(cudaMemcpy(&num_nodes, d_num_nodes, sizeof(int), cudaMemcpyDeviceToHost));

    // generate tree on cpu
    tree_generate_hierarchy_cpu_helper(node_pool, root, codes, &num_nodes, node_id, depth);
    // print_tree_helper(root);

    // copy back to gpu
    // convert host ptr to gpu ptr
    for (int i = 0; i < num_nodes; i++) {
        GNode *node = &(node_pool[i]);
        node->parent = d_node_pool + node->id;
        node->points = d_points + (node->points - h_points);
        for (int j = 0; j < N_CHILDREN_CUDA; j++) {
          if ((long)node->children[j] != -1)
            node->children[j] = d_node_pool + node->children[j]->id;
        }
    }
    root->parent = NULL;

    ERROR_CHECK(cudaMemcpy(d_num_nodes, &num_nodes, sizeof(int), cudaMemcpyHostToDevice));
    ERROR_CHECK(cudaMemcpy(d_node_pool, node_pool, sizeof(GNode) * num_nodes, cudaMemcpyHostToDevice));
    ERROR_CHECK(cudaMemcpy(d_codes, d_codes, sizeof(code_t) * root->num_points, cudaMemcpyHostToDevice));
    free(node_pool);
    free(codes);
}
