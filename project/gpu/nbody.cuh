#ifndef __NBODY_CUH__
#define __NBODY_CUH__

#include <stdint.h>

// Local, not CUDA
#include "points.h"
#include "tree.h"

// Local, CUDA
#include "tree.cuh"

class NBodyCuda {
private:
    /// Simulation constants
    double theta;
    double gravity;
    double bounds;

    /// Global point array
    Point *host_points;
    Point *cuda_points;
    int num_points;

    /// Indices for sorting points into child nodes (only need values 0 through 7).
    uint8_t *host_child_indices;
    uint8_t *cuda_child_indices;

    /// Tree root
    GNode *cuda_root;

    // Node pool
    int num_nodes;
    GNode *cuda_node_pool;
    int *cuda_num_nodes;
    char *cuda_leaf_mask;   // mask for whether a node is a valid leaf
    int *cuda_leaf_prefix;  // holds index of node in cuda_leaves
    int *cuda_leaves;        // list of leaf nodes
    int num_leaves; // number of leaf nodes

    /// Morton codes
    code_t *host_morton_codes;
    code_t *cuda_morton_codes;

    /// Times for various phases of the algorithm
    double time_copy_host_device;
    double time_construct_tree;
    double time_compute_forces;
    double time_copy_device_host;

    void compute_child_points(
        int      num_points,  ///< Number of points the parent owns
        GNode   *parent,      ///< Parent node to compute children for
        Point   *cuda_points, ///< Points array
        uint8_t *cuda_indices ///< Child index array
    );

    GNode *generate_hierarchy(void);

public:
    NBodyCuda(
        double theta,     ///< Threshold for subdividing nodes
        double gravity,   ///< Force constant
        double bounds,    ///< Width of the point space
        Point *points,    ///< Points in the simulation
        int    num_points ///< Number of points in the simulation
    );

    ~NBodyCuda();

    void copy_points_to_gpu();

    void copy_points_from_gpu();

    void construct_tree();

    // Use the original Barnes-Hut algorithm
    void compute_forces_barnes_hut();

    // Use Barnes's 1990 algorithm
    void compute_forces_updated_barnes();

    // N^2 implementation
    void compute_forces_naive();

    // Copy to device, tree construction, force computation, copy back to host.
    long get_total_usecs();

    // Print times for profiling
    void print_times();
};
#endif // __NBODY_CUH__
