#include "updated_barnes.cuh"

#include <stdio.h>

// Local, non-CUDA
#include "points.h"

// Local, CUDA
#include "tree.cuh"
#include "constants.cuh"
#include "force.cuh"

// From nbody.cu
extern __constant__ struct cuda_constants CudaConstants;

#define BARNES_UPDATED_DEBUG

__device__ bool
a_within_b(double a_coordinate, double b_min, double b_max)
{
    return (a_coordinate >= b_min) && (a_coordinate <= b_max);
}

// The distance from the bounding cube around `node` to the center of mass of `cell`.
__device__ double
cooked_distance(GNode *node, GNode *cell)
{
    bool x_overlaps = a_within_b(cell->cm.x, node->min_x, node->max_x);
    bool y_overlaps = a_within_b(cell->cm.y, node->min_y, node->max_y);
    bool z_overlaps = a_within_b(cell->cm.z, node->min_z, node->max_z);

    // If 3 coordinates overlap, node is inside of cell.
    if (x_overlaps && y_overlaps && z_overlaps) {
        return 0;
    }

    double nearest_x = cell->cm.x <= node->min_x ? node->min_x : node->max_x;
    double nearest_y = cell->cm.y <= node->min_y ? node->min_y : node->max_y;
    double nearest_z = cell->cm.z <= node->min_z ? node->min_z : node->max_z;

    double dist_x = fabs(cell->cm.x - nearest_x);
    double dist_y = fabs(cell->cm.y - nearest_y);
    double dist_z = fabs(cell->cm.z - nearest_z);

    // If 2 coordinates overlap, compute distance to the nearest face. Picture pulling that face of
    // the cube out into a rectangular prism until the face hits the center of mass. The distance we
    // need to pull that face is what we're computing.
    if (x_overlaps && y_overlaps) {
        return dist_z;
    } else if (x_overlaps && z_overlaps) {
        return dist_y;
    } else if (y_overlaps && z_overlaps) {
        return dist_x;
    }

    // If 1 coordinate overlaps, compute distance to the nearest edge.
    if (x_overlaps) {
        return sqrt(dist_y * dist_y + dist_z * dist_z);
    } else if (y_overlaps) {
        return sqrt(dist_x * dist_x + dist_z * dist_z);
    } else if (z_overlaps) {
        return sqrt(dist_x * dist_x + dist_y * dist_y);
    }

    // If 0 coordinates overlap, compute the distance to the nearest corner.
    return sqrt(dist_x * dist_x + dist_y * dist_y + dist_z * dist_z);
}

// Whether to subdivide this
__device__ bool
must_subdivide(GNode *tree, GNode *node)
{
    double cd = cooked_distance(node, tree);
    bool split = (tree->diameter > (CudaConstants.theta * cd));

    return split;
}

__global__ void
kernel_sum_force_leaf(GNode *node, int inter_point_count)
{
    // int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int idx = threadIdx.x;

    if (idx >= node->num_points) {
        return;
    }

    Point *p = &(node->points[idx]);

#ifdef BARNES_UPDATED_DEBUG
    if (!p) {
        printf("node %d, thread %d: invalid node\n", node->id, p->id);
        return;
    }
#endif // BARNES_UPDATED_DEBUG

#ifdef BARNES_UPDATED_DEBUG
    if (node->inter_point_count != inter_point_count) {
        printf("Node %d, node->count %d, orig count = %d\n", node->id, node->inter_point_count, inter_point_count);
    }
#endif // BARNES_UPDATED_DEBUG

    for (int i = 0; i < inter_point_count; i++) {
        // __syncwarp();
        Point *inter_point = node->inter_points[i];

#ifdef BARNES_UPDATED_DEBUG
        if (!inter_point) {
            printf("node %d, thread %d, inter point %d null\n", node->id, p->id, i);
            continue;
        }
#endif // BARNES_UPDATED_DEBUG

        if (p != inter_point) {
            // printf("Node %d: Interaction %d -> %d\n", node->id, inter_point->id, p->id);

            // No self-interaction
            double3 forces = compute_point_forces(inter_point, p);

            p->forces.x += forces.x;
            p->forces.y += forces.y;
            p->forces.z += forces.z;
        }
    }
}

__device__ void
sum_force(GNode *node, int inter_point_count)
{
    // Because points are sorted according to the Morton codes, every node knows its full list of
    // children already. No need to recurse down `node` until we handle all of the leaves.
    int num_threads = node->num_points; // Always less than or equal to INTER_POINT_MAX
    int num_blocks = 1;
    // __threadfence(); // Make sure outstanding writes are complete
    kernel_sum_force_leaf<<<num_blocks, num_threads>>>(node, inter_point_count);
    cudaDeviceSynchronize();
}

__device__ void
add_inter_point(GNode *node, Point *inter_point)
{
#ifdef BARNES_UPDATED_DEBUG
    if (node->inter_point_count >= INTER_POINT_MAX) {
        printf("node %d, count %d\n", node->id, node->inter_point_count);
    }
#endif // BARNES_UPDATED_DEBUG
    // printf("Node %d: adding inter point %d at index %d\n", node->id, inter_point->id, node->inter_point_count);
    node->inter_points[node->inter_point_count++] = inter_point;
    if (node->inter_point_count == INTER_POINT_MAX) {
        sum_force(node, INTER_POINT_MAX);
        node->inter_point_count = 0;
    }
}

__device__ void
compute_node_forces(GNode *node, GNode *tree)
{
    if (must_subdivide(tree, node)) {
        if (tree->is_internal) {
            // Consider this node's children. We might split or just use centers of mass.
            for (int i = 0; i < N_CHILDREN_CUDA; i++) {
                GNode *child = tree->children[i];
                if ((long) child != -1 && child->num_points != 0) {
                    compute_node_forces(node, child);
                }
            }
        } else {
            // Add forces for the points that constitute this leaf.
            for (int i = 0; i < tree->num_points; i++) {
                Point *inter_point = &(tree->points[i]);
                add_inter_point(node, inter_point);
            }
        }
    } else {
        // No subdividing: use the tree's center of mass.
        Point *inter_point = &tree->cm;
        add_inter_point(node, inter_point);
    }
}

// Idea: instead of traversing the tree, what if we just iterate over all of the nodes?
__device__ void
node_forces_no_tree(GNode *node)
{
    int num_nodes = CudaConstants.num_nodes;

    // Skip the root
    for (int i = 1; i < num_nodes; i++) {
        // Keep the threads in lockstep as they go through the node array.
        __syncthreads();
        GNode *n = &CudaConstants.node_pool[i];

        if (must_subdivide(n, node)) {
            if (!n->is_internal) {
                // Subdivide n, but n is a leaf node -> use each point in n.
                // n's points -> cell forces
                for (int j = 0; j < n->num_points; j++) {
                    Point *inter_point = &n->points[j];
                    // printf("Node %d: inter node %d, adding point %d\n", node->id, n->id, inter_point->id);
                    add_inter_point(node, inter_point);
                }
            } else {
                // Do nothing. We'll find the centers of mass somewhere else during our search.
                // printf("Must subdivide %d for %d, but not internal\n", n->id, node->id);
            }
        } else {
            if (must_subdivide(n->parent, node)) {
                // We needed to subdivide n's parent, but we aren't subdividing n. Use n's center of
                // mass.
                // printf("Using node %d's COM\n", n->id);
                Point *inter_point = &n->cm;
                add_inter_point(node, inter_point);
            } else if (!n->is_internal && n->num_points > 0) {
                // We didn't need to subdivide n's parent, nor do we need to subdivide n, but n is a
                // leaf with some points that will contribute to the forces in `node`.
                for (int j = 0; j < n->num_points; j++) {
                    Point *inter_point = &n->points[j];
                    // printf(" -- Node %d: inter node %d, adding point %d\n", node->id, n->id, inter_point->id);
                    add_inter_point(node, inter_point);
                }
            } else {
                // Do nothing. We have already used an ancestor of n's center of mass.
                // printf("Skipping inter node %d to node %d interaction\n", n->id, node->id);
            }
        }
    }
}

__global__ void
updated_barnes_kernel(void)
{
    int node_idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (node_idx >= CudaConstants.num_nodes) {
        return;
    }

    GNode *node = &CudaConstants.node_pool[node_idx];
    GNode *root = &CudaConstants.node_pool[0];

    if (!node->is_cell) {
        return;
    }

    node->inter_point_count = 0;
    compute_node_forces(node, root);
    // node_forces_no_tree(node);

    if (node->inter_point_count != 0) {
        // This thread still has some unhandled interaction points.
        sum_force(node, node->inter_point_count);
    }
}
