#ifndef __FORCE_CUH__
#define __FORCE_CUH__

#include "points.h"

// Compute the force that p1 exerts on p2
__device__ double3
compute_point_forces(
    Point *p1, ///< Point exerting the force
    Point *p2  ///< Point experiencing the force
);

#endif // __FORCE_CUH__
