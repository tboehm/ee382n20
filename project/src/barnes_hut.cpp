#include "barnes_hut.h"

#include <stdio.h>
#include <math.h>

#include "force.h"
#include "points.h"
#include "tree.h"

extern double Theta;
extern double Gravity;

static inline double
distance_total(Node* node, Point* particle)
{
    double x = node->cm.x - particle->x;
    double y = node->cm.y - particle->y;
    double z = node->cm.z - particle->z;
    return sqrt(x * x + y * y + z * z);
}

force3 calculate_force_points(Point* p1, Point* p2)
{
    force3 force = {0.f, 0.f, 0.f};

    if (p1->id == p2->id) {
        return force;
    }

    return force_compute(p1, p2);
}

force3 get_force(Node* node, Point* particle, int id)
{
    if(node->num_points == 0)
    {
        // no force here
        force3 forces = {0.f, 0.f, 0.f};
        return forces;
    }
    else if(node->diameter < Theta * distance_total(node, particle))
    {
        // calculate as a single point
        return calculate_force_points(&node->cm, particle);
    }
    else if(node->is_internal)
    {
        // Recurse down the sub tree
        force3 forces = {0.f, 0.f, 0.f};

        for(int i = 0; i < 8; i++)
        {
            force3 new_forces = get_force(node->children[i], particle, id);
            forces.x += new_forces.x;
            forces.y += new_forces.y;
            forces.z += new_forces.z;
       }

        return forces;
    }
    else
    {
        // Leaf node
        // Calculate forces as the sum of all the particles in the node
        force3 forces = {0.f, 0.f, 0.f};

        for(int i = 0; i < node->num_points; i++)
        {
            Point *p2 = node->points[i];
            if (p2 == particle) {
                continue;
            }
            force3 new_forces = calculate_force_points(p2, particle);
            forces.x += new_forces.x;
            forces.y += new_forces.y;
            forces.z += new_forces.z;
        }
        return forces;
    }
}

void barnes_hut(Node* root, Point* point_array, int num_points)
{
    for(int i = 0; i < num_points; i++)
    {
        force3 forces = get_force(root, &point_array[i], i);
        point_array[i].forces.x = forces.x;
        point_array[i].forces.y = forces.y;
        point_array[i].forces.z = forces.z;
    }
}
