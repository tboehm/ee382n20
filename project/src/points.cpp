#include "points.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void
points_print(Point *point_array, int num_points)
{
    for (int i = 0; i < num_points; i++) {
        Point *p = &point_array[i];
        printf(
            "%d: mass = %g, position = (%g, %g, %g), forces = (%g, %g, %g)\n",
            p->id,
            p->mass,
            p->x,
            p->y,
            p->z,
            p->forces.x,
            p->forces.y,
            p->forces.z
        );
    }
}


static inline double
clamp_double(double x, double lower, double upper)
{
    return fmax(lower, fmin(upper, x));
}

static inline double
gaussian_double(double mean, double std)
{
    double u = (double)rand() / (double)RAND_MAX;
    double z = sqrt(-2.0 * log10(u));

    return z * std + mean;
}

static inline dist3
gaussian_point(dist3 mean, dist3 std)
{
    dist3 point;

    double u1 = (double)rand() / (double)RAND_MAX;
    double u2 = (double)rand() / (double)RAND_MAX;
    double u3 = (double)rand() / (double)RAND_MAX;

    double r = sqrt(-2.0f * log10(u1));
    double alpha = 2.0f * M_PI * u2;
    double beta = 2.0f * M_PI * u3;

    point.x = r * cos(alpha) * cos(beta);
    point.y = r * cos(alpha) * sin(beta);
    point.z = r * sin(alpha);

    point.x = std.x * point.x + mean.x;
    point.y = std.y * point.y + mean.y;
    point.z = std.z * point.z + mean.z;

    return point;
}

void
points_generate(Point *point_array, int num_points, double grid_dim, double max_weight, bool normal)
{
    // NORMAL DISTRIBUTION:
    // Center points at the origin. The standard deviation is one third the grid dimension.
    dist3 mean_position = { 0.f, 0.f, 0.f };
    dist3 stddev_position = { grid_dim / 3.f, grid_dim / 3.f, grid_dim / 3.f };

    // Masses are normally distributed about (max_weight / 2). A different distribution (e.g. Gamma)
    // might be nicer.
    double mean_mass = max_weight / 2;
    double stddev_mass = mean_mass / 3.f;

    // UNIFORM DISTRIBUTION
    double grid_min = -grid_dim;
    double grid_div = (double)RAND_MAX / (grid_dim * 2.0f);
    double mass_div = (double)RAND_MAX / max_weight;

    double x, y, z, mass;

    for (int i = 0; i < num_points; i++) {
        Point *p = &point_array[i];

        if (normal) {
            // Normal distribution
            dist3 position = gaussian_point(mean_position, stddev_position);
            x = clamp_double(position.x, -grid_dim, grid_dim);
            y = clamp_double(position.y, -grid_dim, grid_dim);
            z = clamp_double(position.z, -grid_dim, grid_dim);
            mass = gaussian_double(mean_mass, stddev_mass);
            mass = clamp_double(mass, 0.f, max_weight);
        } else {
            // Uniform distribution
            x = grid_min + (rand() / grid_div);
            y = grid_min + (rand() / grid_div);
            z = grid_min + (rand() / grid_div);
            mass = rand() / mass_div;
        }

        p->id = i;
        p->x = x;
        p->y = y;
        p->z = z;
        p->mass = mass;
        p->forces.x = 0;
        p->forces.y = 0;
        p->forces.z = 0;
    }
}

double
points_force_error(Point *ref_points, Point *alg_points, int num_points)
{
    double mape = 0;

    for (int i = 0; i < num_points; i++) {
        force3 ref_force = ref_points[i].forces;
        force3 alg_force = alg_points[i].forces;

        if (ref_force.x != 0.f) {
            mape += fabs((ref_force.x - alg_force.x) / ref_force.x);
        }
        if (ref_force.y != 0.f) {
            mape += fabs((ref_force.y - alg_force.y) / ref_force.y);
        }
        if (ref_force.z != 0.f) {
            mape += fabs((ref_force.z - alg_force.z) / ref_force.z);
        }

        // NaN check
        if (mape != mape) {
            printf("\033[31;1mNaN force at index %d:", i);
            printf(" ref forces (%f, %f, %f),", ref_force.x, ref_force.y, ref_force.z);
            printf(" alg forces (%f, %f, %f)\033[0m\n", alg_force.x, alg_force.y, alg_force.z);
            // It's not gonna get any better.
            return mape;
        }
    }

    mape /= num_points;

    return mape;
}
