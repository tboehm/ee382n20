#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/time.h>

#include "barnes_hut.h"
#include "points.h"
#include "reference.h"
#include "tree.h"
#include "updated_barnes.h"

#ifdef USE_GPU
// GPU includes
#include "nbody.cuh"
#include <thrust/sort.h>
#endif

// These values are needed for Barnes (updated). Sorry for the globals.
// NOTE: These are good candidates for constant memory on GPU.
int    NCrit;         ///< Maximum number of points per cell
int    MaxLeafPoints; ///< Maximum number of points per leaf
Node  *TreeRoot;      ///< Root of the tree
double Theta;         ///< Threshold for subdividing cells
double Gravity;       ///< Gravitational (or generic force) constant

enum AlgorithmNumbers {
    AlgorithmBarnesHut,
    AlgorithmBarnesUpdated,
    AlgorithmNaive,
};

static int
n_body_algorithm_number(const char *algorithm)
{
    if (strcmp(algorithm, "barnes-hut") == 0) {
        return AlgorithmBarnesHut;
    } else if (strcmp(algorithm, "barnes-updated") == 0) {
        return AlgorithmBarnesUpdated;
    } else if (strcmp(algorithm, "naive") == 0) {
        return AlgorithmNaive;
    } else {
        return -1;
    }
}

static void
help(const char *fname)
{
    printf("Usage: %s\n"
           "    -A, --algorithm    <algorithm name>\n"
           "    -B, --bounds       <x/y/z bounds>\n"
           "    -P, --points       <number of points>\n"
           "    -W, --weight       <maximum weight>\n"
           "    -L, --leaf-points  <max points per leaf>\n"
           "    -N, --ncrit        <max points per cell>\n"
           "    -T, --theta        <threshold for subdividing>\n"
           "    -G, --gravity      <gravitational constant>\n"
           "    -S, --seed         <random seed>\n"
           "    -c, --check\n"
           "    -g, --gpu\n"
           "    -n, --normal\n"
           "    -o, --only-tree\n"
           "    -t, --tolerance    <maximum allowable MAPE>\n"
           "    -v, --verbose\n"
           "    -h, --help\n",
           fname);
}

bool id_sort(Point a, Point b) {
    return a.id < b.id;
}

int
check_forces(Point *ref_array, Point *point_array, int num_points, double tolerance, long ref_time, long alg_time)
{
    double speedup = 100 * (double)ref_time / (double)alg_time;

    printf("Approximation algorithm: %.3f sec\n", (double)alg_time / 1e6);
    printf("Reference algorithm: %.3f sec\n", (double)ref_time / 1e6);
    if (speedup < 100) {
        printf("Relative speed: \033[31m%.0f%%\033[0m (worse than reference time)\n", speedup);
    } else {
        printf("Relative speed: %.0f%%\n", speedup);
    }

    double mape = points_force_error(ref_array, point_array, num_points);

    printf("MAPE: %.2f%%\n", 100 * mape);

    if (mape > tolerance) {
        printf(
            "\033[31;1mReference check failed: MAPE %.2f%% > %.2f%%\033[0m\n",
            100 * mape,
            100 * tolerance
        );

        return 1;
    }

    printf(
        "\033[32;1mReference check passed: MAPE %.2f%% < %.2f%%\033[0m\n",
        100 * mape,
        100 * tolerance
    );

    return 0;
}

int
main(int argc, char *argv[])
{
    char *algorithm = NULL;
    int algorithm_number = -1;
    int bounds = 1;
    int num_points = -1;
    double max_weight = 1.0f;
    MaxLeafPoints = -1;
    NCrit = -1;
    Theta = 0.5;
    Gravity = 6.6726; // just going to leave off the e-11 for now
    double tolerance = 0.05;
    int seed = 2112;
    bool normal_distribution = false;
    bool check = false;
    bool use_gpu = false;
    bool only_tree = false;
    bool verbose = false;

    int rc = 0;

    static struct option perf_options[] = {
        { "algorithm",   required_argument, 0, 'A' }, // N-body algorithm
        { "bounds",      required_argument, 0, 'B' }, // Max (and -min) for all 3 dimensions
        { "points",      required_argument, 0, 'P' }, // number of points
        { "weight",      required_argument, 0, 'W' }, // maximum node weight
        { "leaf-points", required_argument, 0, 'L' }, // maximum number of points per leaf of points
        { "ncrit",       required_argument, 0, 'N' }, // maximum number of points per cell (Barnes updated)
        { "theta",       required_argument, 0, 'T' }, // threshold for subdividing
        { "gravity",     required_argument, 0, 'G' }, // gravitational constant
        { "seed",        required_argument, 0, 'S' }, // random seed
        { "check",       no_argument,       0, 'c' }, // check the algorithm against the full N^2 version
        { "gpu",         no_argument,       0, 'g' }, // use the gpu
        { "normal",      no_argument,       0, 'n' }, // normal (Gaussian) point distribution
        { "only-tree",   no_argument,       0, 'o' }, // only construct the tree (no force computation)
        { "tolerance",   required_argument, 0, 't' }, // maximum allowable MAPE
        { "help",        no_argument,       0, 'h' }, // print help and exit
        { "verbose",     no_argument,       0, 'v' }, // verbose or not
    };

    int option_index = 0;
    int c;
    while ((c = getopt_long(argc, argv, "A:B:H:P:W:L:N:T:G:S:cgnot:hv",
                            perf_options, &option_index)) != -1) {
        switch (c) {
        case 'A':
            algorithm = (char *)optarg;
            algorithm_number = n_body_algorithm_number(algorithm);
            if (algorithm_number < 0) {
                fprintf(stderr, "Unknown algorithm: '%s'\n", algorithm);
                exit(EXIT_FAILURE);
            }
            break;
        case 'B':
            bounds = atoi(optarg);
            if (bounds <= 0) {
                fprintf(stderr, "Box bounds must be greater than 0\n");
                exit(EXIT_FAILURE);
            }
            break;
        case 'P':
            num_points = atoi(optarg);
            if (num_points <= 0) {
                fprintf(stderr, "Number of points must be greater than 0\n");
                exit(EXIT_FAILURE);
            }
            break;
        case 'W':
            max_weight = atof(optarg);
            if (max_weight <= 0) {
                fprintf(stderr, "Maximum weight must be greater than 0\n");
                exit(EXIT_FAILURE);
            }
            break;
        case 'L':
            MaxLeafPoints = atoi(optarg);
            if (MaxLeafPoints <= 0) {
                fprintf(stderr, "Maximum points per leaf must be greater than 0\n");
                exit(EXIT_FAILURE);
            }
            break;
        case 'N':
            NCrit = atoi(optarg);
            if (NCrit <= 0) {
                fprintf(stderr, "NCrit must be greater than 0\n");
                exit(EXIT_FAILURE);
            }
            break;
        case 'T':
            Theta = atof(optarg);
            if (Theta <= 0) {
                fprintf(stderr, "Theta must be greater than 0\n");
                exit(EXIT_FAILURE);
            }
            break;
        case 'G':
            Gravity = atof(optarg);
            if (Gravity <= 0) {
                fprintf(stderr, "Gravity must be greater than 0\n");
                exit(EXIT_FAILURE);
            }
            break;
        case 'S':
            seed = atoi(optarg);
            srand(seed);
            break;
        case 'c':
            check = true;
            break;
        case 'g':
            use_gpu = true;
            break;
        case 'n':
            normal_distribution = true;
            break;
        case 'o':
            only_tree = true;
            break;
        case 't':
            tolerance = atof(optarg);
            break;
        case 'h':
            help(argv[0]);
            exit(EXIT_SUCCESS);
        case 'v':
            verbose = true;
            break;
        case '?':
            break;
        default:
            printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (optind < argc) {
        fprintf(stderr, "Non-option argv elements: ");
        while (optind < argc) {
            printf("%s ", argv[optind++]);
        }
        printf("\n");
        exit(EXIT_FAILURE);
    }

    // Make sure the user supplied arguments
    if (algorithm_number < 0) {
        fprintf(stderr, "Missing required argument: --algorithm\n");
        exit(EXIT_FAILURE);
    }

    if (num_points < 0) {
        fprintf(stderr, "Missing required argument: --points\n");
        exit(EXIT_FAILURE);
    }

    if (algorithm_number == AlgorithmBarnesUpdated) {
        if (NCrit < 0) {
            fprintf(stderr, "Missing required argument: --ncrit\n");
            exit(EXIT_FAILURE);
        } else if (MaxLeafPoints < 0) {
            fprintf(stderr, "Missing required argument: --leaf-points\n");
            exit(EXIT_FAILURE);
        }

        if (NCrit < MaxLeafPoints) {
            fprintf(stderr, "NCrit must be greater than or equal to max leaf points\n");
            exit(EXIT_FAILURE);
        }
    } else {
      NCrit = NCrit == -1 ? 1 : NCrit;
      MaxLeafPoints = MaxLeafPoints == -1 ? 1 : MaxLeafPoints;
    }

    if (check && (tolerance >= 1 || tolerance <= 0)) {
        fprintf(stderr, "tolerance must be in (0, 1)\n");
        exit(EXIT_FAILURE);
    }

#ifndef USE_GPU
    if (use_gpu) {
        fprintf(stderr, "You specified '--gpu' but did not compile with -DUSE_GPU\n");
        exit(EXIT_FAILURE);
    }
#endif // ndef USE_GPU

    if (verbose) {
        printf("algorithm = %s\n", algorithm);
        printf("bounds = %d\n", bounds);
        printf("num_points = %d\n", num_points);
        printf("max_weight = %g\n", max_weight);
        printf("MaxLeafPoints = %d\n", MaxLeafPoints);
        printf("NCrit = %d\n", NCrit);
        printf("Theta = %g\n", Theta);
        printf("gravity = %g\n", Gravity);
        printf("seed = %d\n", seed);
        printf("check = %d\n", check);
        printf("use_gpu = %d\n", use_gpu);
        printf("normal_distribution = %d\n", normal_distribution);
        printf("only_tree = %d\n", only_tree);
        printf("tolerance = %g\n", tolerance);
    }

    
// #define FIXED_GRID
#ifdef FIXED_GRID
    // FIXME: temp use fix grid for testing
    num_points = int(bounds)*int(bounds);
    Point *point_array = (Point *)malloc(num_points * sizeof(Point));
    Point *ref_array = NULL;

    int i = 0;
    for (int x = int(bounds)-1; x >= 0; x--) {
      for (int y = int(bounds)-1; y >= 0; y--) {
        Point *p = &point_array[i];
        p->id = i;
        p->x = x;
        p->y = y;
        p->z = 0;
        p->mass = 10;
        p->forces.x = 0;
        p->forces.y = 0;
        p->forces.z = 0;

        i++;
      }
    }
#else

    // Generate set of random points in the gridspace
    // Grid space ranges from (-bounds) to bounds in all three directions.
    Point *point_array = (Point *)malloc(num_points * sizeof(Point));
    Point *ref_array = NULL;
    points_generate(point_array, num_points, bounds, max_weight, normal_distribution);
#endif

    if (check) {
        ref_array = (Point *)malloc(num_points * sizeof(Point));
        memcpy(ref_array, point_array, num_points * sizeof(Point));
    }

#ifdef USE_GPU
    if (use_gpu) {
        NBodyCuda *nbody_cuda = new NBodyCuda(Theta, Gravity, bounds, point_array, num_points);

        nbody_cuda->copy_points_to_gpu();

        nbody_cuda->construct_tree();

        if (only_tree) {
            nbody_cuda->print_times();
            printf("Tree construction complete. Exiting\n");
            return rc;
        }

        if (algorithm_number == AlgorithmBarnesHut) {
            nbody_cuda->compute_forces_barnes_hut();
        } else if (algorithm_number == AlgorithmBarnesUpdated) {
            nbody_cuda->compute_forces_updated_barnes();
        } else {
            nbody_cuda->compute_forces_naive();
        }

        // Will copy points back into point_array.
        nbody_cuda->copy_points_from_gpu();

        nbody_cuda->print_times();

        // Memory transfers + tree construction + force computation.
        long alg_time = nbody_cuda->get_total_usecs();

        if (check) {
            // See how we did. Compare execution times and compute MAPE.
            printf("==================== CPU REF ====================\n");

            struct timeval start, end, diff;

            gettimeofday(&start, NULL);

            Node *root = tree_construct(bounds, ref_array, num_points);
            // tree_print(root);

            TreeRoot = root;
            gettimeofday(&end, NULL);
            timersub(&end, &start, &diff);
            long tree_time = diff.tv_usec + diff.tv_sec * 1e6;
            printf("Construct tree: %.3f sec\n", (double) tree_time / 1e6);

            if (algorithm_number == AlgorithmBarnesHut) {
                printf("Running Barnes-Hut on CPU\n");
                barnes_hut(root, ref_array, num_points);
            } else if (algorithm_number == AlgorithmBarnesUpdated) {
                printf("Running Barnes-updated algorithm on CPU\n");
                updated_barnes(root);
            } else {
                reference_forces(ref_array, num_points);
            }

            gettimeofday(&end, NULL);
            timersub(&end, &start, &diff);
            long ref_time = diff.tv_usec + diff.tv_sec * 1e6;

            // Make sure the comparison makes sense.
            thrust::sort(ref_array, ref_array + num_points, id_sort);
            thrust::sort(point_array, point_array + num_points, id_sort);

            rc = check_forces(ref_array, point_array, num_points, tolerance, ref_time, alg_time);
        }

        return rc;
    }
#endif

    // Create a Barnes Hut Tree representation of the grid space
    Node *root;

    struct timeval start_construction;
    gettimeofday(&start_construction, NULL);
    root = tree_construct(bounds, point_array, num_points);

    TreeRoot = root;

    if (verbose) {
        tree_print(root);
    }

    struct timeval start_algorithm, diff;
    gettimeofday(&start_algorithm, NULL);
    timersub(&start_algorithm, &start_construction, &diff);

    long construct_usec = diff.tv_sec * 1e6 + diff.tv_usec;

    if (only_tree) {
        printf("Construct tree: %.3f sec\n", (double) construct_usec / 1e6);
        printf("Algorithm: 0 sec\n");
        printf("Total: %.3f sec\n", (double) construct_usec / 1e6);
        printf("Tree construction complete. Exiting\n");
        return rc;
    }

    if(algorithm_number == AlgorithmBarnesHut){
        barnes_hut(root, point_array, num_points);
    } else {
        updated_barnes(root);
    }

    struct timeval end_time;
    gettimeofday(&end_time, NULL);

    if (verbose) {
        // tree_print(root);
        points_print(point_array, num_points);
    }

    timersub(&end_time, &start_algorithm, &diff);

    long algorithm_usec = diff.tv_sec * 1e6 + diff.tv_usec;
    long alg_time = algorithm_usec + construct_usec;

    printf("Construct tree: %.3f sec\n", (double) construct_usec / 1e6);
    printf("Algorithm: %.3f sec\n", (double) algorithm_usec / 1e6);
    printf("Total: %.3f sec\n", (double) alg_time / 1e6);

    if (check) {
        long ref_time = reference_forces(ref_array, num_points);

        rc = check_forces(ref_array, point_array, num_points, tolerance, ref_time, alg_time);
    }

    // Tree destruction also frees the point array.
    tree_destruct(root);

    return rc;
}
