#include "tree.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "points.h"

// Sorry for the externs.
extern int NCrit;
extern int MaxLeafPoints;

static int TreeId = 0;

// Compute the total mass and center of mass for a node/subtree.
static void
compute_node_mass(Node *node)
{
    node->cm.id = -node->id;
    node->cm.mass = 0.f;
    node->cm.x = 0.f;
    node->cm.y = 0.f;
    node->cm.z = 0.f;

    if (node->is_internal) {
        // Only look at the children
        for (int i = 0; i < N_CHILDREN; i++) {
            Point *p = &node->children[i]->cm;
            double m = p->mass;
            node->cm.mass += m;
            node->cm.x += p->x * m;
            node->cm.y += p->y * m;
            node->cm.z += p->z * m;
        }
    } else {
        // Look at all of the points
        for (int i = 0; i < node->num_points; i++) {
            Point *p = node->points[i];
            double m = p->mass;
            node->cm.mass += m;
            node->cm.x += p->x * m;
            node->cm.y += p->y * m;
            node->cm.z += p->z * m;
        }
    }

    if (node->cm.mass != 0.f) {
        // For both cases, we need to divide by total mass (as long as there *is* mass).
        node->cm.x /= node->cm.mass;
        node->cm.y /= node->cm.mass;
        node->cm.z /= node->cm.mass;
    } else {
        node->cm.x = 0.f;
        node->cm.y = 0.f;
        node->cm.z = 0.f;
    }
}

static void
inherit_points_from_parent(Node *parent, Node *child)
{
    // The child inherits from its parent all points that are within its bounds.
    for (int i = 0; i < parent->num_points; i++) {
        Point *p = parent->points[i];
        if ((p->x >= child->min_x && p->x < child->max_x)
            && (p->y >= child->min_y && p->y < child->max_y)
            && (p->z >= child->min_z && p->z < child->max_z)) {
            child->points[child->num_points++] = p;
        }
    }
}

Node *
tree_construct(double grid_dim, Point *point_array, int num_points)
{
    Node *root = (Node *)malloc(sizeof(Node));

    root->id = TreeId++;

    root->parent = NULL;
    memset(root->children, 0, sizeof(Node *) * N_CHILDREN);
    root->min_x = -grid_dim;
    root->max_x = grid_dim;
    root->min_y = -grid_dim;
    root->max_y = grid_dim;
    root->min_z = -grid_dim;
    root->max_z = grid_dim;
    root->diameter = 2.0f * grid_dim;
    root->points = (Point **)malloc(num_points * sizeof(Point *));
    for (int i = 0; i < num_points; i++) {
        root->points[i] = &point_array[i];
    }
    root->num_points = num_points;
    root->cm.mass = 0.f;
    root->height = 0;
    root->is_internal = false;

    // Split node
    root->is_cell = num_points <= NCrit;
    tree_split_node(root);

    // compute COM
    compute_node_mass(root);
    return root;
}

void
tree_destruct(Node *root)
{
    for (int i = 0; i < N_CHILDREN; i++) {
        Node *child = root->children[i];
        if (child) {
            tree_destruct(child);
        }
    }

    // Free if all children are null
    free(root->points);
    free(root);
}

void tree_split_node(Node *parent)
{
    if (parent->num_points <= MaxLeafPoints) {
        // Nothing to do!
        return;
    }

    parent->is_internal = true;

    // Each new node will cover one-eighth the space of the parent.
    double x_range = (parent->max_x - parent->min_x) / 2;
    double y_range = (parent->max_y - parent->min_y) / 2;
    double z_range = (parent->max_z - parent->min_z) / 2;

    for (int i = 0; i < N_CHILDREN; i++) {
        Node *node = (Node *)malloc(sizeof(Node));

        // X dimension (left/right)
        if (i % 2 == 0) {
            // Left side (nodes 0, 2, 4, 6)
            node->min_x = parent->min_x;
            node->max_x = parent->min_x + x_range;
        } else {
            // Right side (nodes 1, 3, 5, 7)
            node->min_x = parent->max_x - x_range;
            node->max_x = parent->max_x;
        }

        // Y dimension (bottom/top)
        if ((i / 2) % 2 == 0) {
            // Bottom side (nodes 0, 1, 4, 5)
            node->min_y = parent->min_y;
            node->max_y = parent->min_y + y_range;
        } else {
            // Top side (nodes 2, 3, 6, 7)
            node->min_y = parent->max_y - y_range;
            node->max_y = parent->max_y;
        }

        // Z dimension (lower/upper)
        if ((i / 4) == 0) {
            // Lower side (nodes 0, 1, 2, 3)
            node->min_z = parent->min_z;
            node->max_z = parent->min_z + z_range;
        } else {
            // Upper side (nodes 4, 5, 6, 7)
            node->min_z = parent->max_z - z_range;
            node->max_z = parent->max_z;
        }

        // Now that the bounds are set, construct the rest of the node.
        node->id = TreeId++;
        parent->children[i] = node;
        node->parent = parent;
        memset(node->children, 0, sizeof(Node *) * N_CHILDREN);
        node->diameter = parent->diameter / 2.f;
        node->height = parent->height + 1;

        // Each node has an array of all of the points it inherits from its parent. This includes
        // those that its children will later split off since in order to facilitate easier
        // interaction list computation.
        node->points = (Point **)malloc(sizeof(Point *) * parent->num_points);
        node->num_points = 0;
        inherit_points_from_parent(parent, node);

        // Decide whether this node will be a cell for computing forces.
        node->is_cell = (!parent->is_cell) && (node->num_points <= NCrit);

        // Split this node into children, if applicable. Then, compute the center of mass.
        node->is_internal = false;
        tree_split_node(node);
        compute_node_mass(node);
    }

}

void
tree_print(Node *root)
{
    if (root->num_points == 0)
        return;
    if (root->is_internal) {
        for (int i = 0; i < root->height; i++) {
            printf("  ");
        }
        printf("\\-+");
    } else {
        for (int i = 0; i < root->height; i++) {
            printf("  ");
        }
        printf("|--");
    }

    printf(
        "%4d :: %d points, %g mass, cm (%g, %g, %g): X [%g, %g], Y [%g, %g], Z [%g, %g]\n",
        root->id,
        root->num_points,
        root->cm.mass,
        root->cm.x,
        root->cm.y,
        root->cm.z,
        root->min_x,
        root->max_x,
        root->min_y,
        root->max_y,
        root->min_z,
        root->max_z
    );

    for (int i = 0; i < N_CHILDREN; i++) {
        Node *child = root->children[i];
        if (child) {
            tree_print(child);
        }
    }
}
