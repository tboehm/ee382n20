#include "reference.h"

#include <math.h>     // sqrt
#include <stdio.h>    // printf()
#include <sys/time.h> // gettimeofday(), timersub(), struct timeval

#include "points.h"

extern double Gravity;

static inline void
calculate_forces(Point *p1, Point *p2)
{
    // FIXME: Use force_compute() from force.h
    double dist_x = p1->x - p2->x;
    double dist_y = p1->y - p2->y;
    double dist_z = p1->z - p2->z;

    double dist_total_sq = dist_x * dist_x + dist_y * dist_y + dist_z * dist_z;

    double force_total = Gravity * p1->mass * p2->mass / dist_total_sq;

    double force_x = force_total * dist_x / sqrt(dist_total_sq);
    double force_y = force_total * dist_y / sqrt(dist_total_sq);
    double force_z = force_total * dist_z / sqrt(dist_total_sq);

    // Forces are computed for point 2, so we need to flip the sign for point 1.
    p1->forces.x -= force_x;
    p1->forces.y -= force_y;
    p1->forces.z -= force_z;

    p2->forces.x += force_x;
    p2->forces.y += force_y;
    p2->forces.z += force_z;
}

long
reference_forces(Point *points, int n_points)
{
    struct timeval start, end, diff;

    printf("Running reference (N^2) algorithm on CPU\n");
    gettimeofday(&start, NULL);

    for (int i = 0; i < n_points; i++) {
        Point *p1 = &points[i];
        for (int j = i + 1; j < n_points; j++) {
            Point *p2 = &points[j];
            calculate_forces(p1, p2);
        }
    }

    gettimeofday(&end, NULL);

    timersub(&end, &start, &diff);

    long reference_usec = diff.tv_sec * 1000000 + diff.tv_usec;

    return reference_usec;
}
