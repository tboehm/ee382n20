#ifndef __BARNES_HUT_H__
#define __BARNES_HUT_H__

#include "points.h"
#include "tree.h"

void
barnes_hut(
    Node  *root,
    Point *point_array,
    int    num_points
);

#endif // __BARNES_HUT_H__
