#ifndef __REFERENCE_H__
#define __REFERENCE_H__

#include "points.h"

/**
 * Compute the forces for a set of points precisely.
 *
 * @return The execution time in microseconds.
 */
long
reference_forces(
    Point *points,  ///< Points whose forces to compute
    int    n_points ///< Number of points
);

#endif // __REFERENCE_H__
