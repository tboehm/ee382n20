#ifndef __POINTS_H__
#define __POINTS_H__

#define N_CHILDREN 8

#include <stdbool.h>
#include <stdint.h>

struct force {
    double x;
    double y;
    double z;
};

typedef struct force force3;

struct dist {
    double x;
    double y;
    double z;
};

typedef struct dist dist3;

struct nbody_point {
    int id;
    // uint64_t morton_code; // Not sure whether we actually need this.
    double x;
    double y;
    double z;
    double mass;
    force3 forces;
};

typedef struct nbody_point Point;

/**
 * Print an array of points.
 */
void
points_print(
    Point *point_array, ///< Points to print
    int    num_points   ///< Length of points array
);

/**
 * Generate an array of points.
 *
 * TODO: Add an way to specify the point and mass distributions.
 */
void
points_generate(
    Point *point_array, ///< Allocated points array
    int    num_points,  ///< Length of points array
    double grid_dim,    ///< Half the width of the point cube
    double max_weight,  ///< Maximum mass of a point
    bool   normal       ///< Draw from a normal distribution
);

/**
 * Compute the mean absolute percent error (MAPE) for point forces.
 *
 * @return The MAPE for the alg_points forces compared to ref_points forces.
 */
double
points_force_error(
    Point *ref_points, ///< Points with forces from the reference algorithm
    Point *alg_points, ///< Points with forces from an approximation
    int    num_points  ///< Length of points array
);

#endif // __POINTS_H__
