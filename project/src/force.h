#ifndef __FORCE_H__
#define __FORCE_H__

#include "points.h"

/**
 * Compute forces between two points.
 *
 * @return A struct with the x, y, and z components of the force.
 */
force3
force_compute(
    Point *p1, ///< Point that forces are acting upon
    Point *p2  ///< Point exerting a force
);

#endif // __FORCE_H__
