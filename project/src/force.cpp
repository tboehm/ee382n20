#include "force.h"

#include <math.h> // sqrt

#include "points.h"

extern double Gravity;

force3
force_compute(Point *p1, Point *p2)
{
    dist3 dist;

    dist.x = p1->x - p2->x;
    dist.y = p1->y - p2->y;
    dist.z = p1->z - p2->z;

    double dist_total_sq = dist.x * dist.x + dist.y * dist.y + dist.z * dist.z;
    double force_total = Gravity * p1->mass * p2->mass / dist_total_sq;
    force3 force;

    force.x = force_total * dist.x / sqrt(dist_total_sq);
    force.y = force_total * dist.y / sqrt(dist_total_sq);
    force.z = force_total * dist.z / sqrt(dist_total_sq);

    return force;
}
