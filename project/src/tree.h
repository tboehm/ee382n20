#ifndef __TREE_H__
#define __TREE_H__

#include <stdbool.h>

#include "points.h"

typedef struct tree_node Node;

struct tree_node {
    int     id;
    double  min_x;
    double  max_x;
    double  min_y;
    double  max_y;
    double  min_z;
    double  max_z;
    double  diameter;
    Node   *parent;
    Node   *children[N_CHILDREN];
    Point **points;
    int     num_points;
    int     height;
    Point   cm;          ///< The center of mass for this node.
    bool    is_internal; ///< Whether this node is internal or a leaf.
    bool    is_cell;     ///< Whether this tree constitutes a cell.
};

/**
 * Construct a tree for the Barnes-Hut or updated Barnes algorithm.
 *
 * @return The tree's root node.
 */
Node *
tree_construct(
    double grid_dim,    ///< Width of the box
    Point *point_array, ///< Points to put into the tree
    int    num_points   ///< Total number of points
);

/**
 * Destroy a tree.
 */
void
tree_destruct(
    Node *root ///< Tree to destroy
);

/**
 * Split nodes for the Barnes (updated) algorithm.
 */
void
tree_split_node(
    Node *parent ///< Parent node to split
);

/**
 * Print a tree in a way that's easy for humans to parse.
 */
void
tree_print(
    Node *root ///< Root node of the tree to print
);

#endif // __TREE_H__
