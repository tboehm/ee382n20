#ifndef __UPDATED_BARNES_H__
#define __UPDATED_BARNES_H__

#include "tree.h"

/**
 * Run the Barnes's 1990 algorithm for the N-body problem.
 */
void
updated_barnes(
    Node *tree ///< Tree to compute the simulation on
);

#endif // __UPDATED_BARNES_H__
