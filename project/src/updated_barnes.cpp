#include "updated_barnes.h"

#include <math.h>    // fabs()
#include <stdbool.h> // true/false
#include <stdio.h>   // printf() and friends
#include <stdlib.h>  // malloc()

#include "force.h"
#include "points.h"
#include "tree.h"

// Sorry for using extern.
extern int    NCrit;
extern int    MaxLeafPoints;
extern Node  *TreeRoot;
extern double Theta;
extern double Gravity;

static inline bool
a_within_b(double a_coordinate, double b_min, double b_max)
{
    return (a_coordinate >= b_min) && (a_coordinate <= b_max);
}

// The distance from the bounding cube around `node` to the center of mass of `cell`.
static double
cooked_distance(Node *node, Node *cell)
{
    bool x_overlaps = a_within_b(cell->cm.x, node->min_x, node->max_x);
    bool y_overlaps = a_within_b(cell->cm.y, node->min_y, node->max_y);
    bool z_overlaps = a_within_b(cell->cm.z, node->min_z, node->max_z);

    // If 3 coordinates overlap, node is inside of cell.
    if (x_overlaps && y_overlaps && z_overlaps) {
        return 0;
    }

    double nearest_x = cell->cm.x <= node->min_x ? node->min_x : node->max_x;
    double nearest_y = cell->cm.y <= node->min_y ? node->min_y : node->max_y;
    double nearest_z = cell->cm.z <= node->min_z ? node->min_z : node->max_z;

    double dist_x = fabs(cell->cm.x - nearest_x);
    double dist_y = fabs(cell->cm.y - nearest_y);
    double dist_z = fabs(cell->cm.z - nearest_z);

    // If 2 coordinates overlap, compute distance to the nearest face. Picture pulling that face of
    // the cube out into a rectangular prism until the face hits the center of mass. The distance we
    // need to pull that face is what we're computing.
    if (x_overlaps && y_overlaps) {
        return dist_z;
    } else if (x_overlaps && z_overlaps) {
        return dist_y;
    } else if (y_overlaps && z_overlaps) {
        return dist_x;
    }

    // If 1 coordinate overlaps, compute distance to the nearest edge.
    if (x_overlaps) {
        return sqrt(dist_y * dist_y + dist_z * dist_z);
    } else if (y_overlaps) {
        return sqrt(dist_x * dist_x + dist_z * dist_z);
    } else if (z_overlaps) {
        return sqrt(dist_x * dist_x + dist_y * dist_y);
    }

    // If 0 coordinates overlap, compute the distance to the nearest corner.
    return sqrt(dist_x * dist_x + dist_y * dist_y + dist_z * dist_z);
}

// Whether to subdivide this
static inline bool
must_subdivide(Node *tree, Node *node)
{
    double cd = cooked_distance(node, tree);

    // This piece of Scheme from Barnes's paper is not completely clear to me:
    //
    // (and (cell? tree)
    //      (> (diameter tree)
    //         (* theta ....)))
    //
    // I think the first condition (cell? tree) really means that "tree" is a collection of points
    // rather than just a single point (which he uses the predicate "body?" to describe).
    //
    // The branch of interaction_list() in which we don't subdivide the tree should just consider
    // all points in that node. Since there's no point in subdividing leaf nodes, I'm using
    // is_internal to decide, here.

    bool split = (tree->diameter > (Theta * cd));

    return split;
}

static void
sum_force_leaf(Node *node, Point *inter_point)
{
    for (int i = 0; i < node->num_points; i++) {
        Point *p = node->points[i];
        if (p == inter_point) {
            continue;
        }

        force3 forces = force_compute(inter_point, p);

        p->forces.x += forces.x;
        p->forces.y += forces.y;
        p->forces.z += forces.z;
    }
}

static void
sum_force(Node *node, Point *inter_point)
{
    if (!node->is_internal) {
        sum_force_leaf(node, inter_point);
    } else {
        for (int c = 0; c < N_CHILDREN; c++) {
            Node *child = node->children[c];
            if (!child) {
                continue;
            }
            sum_force(child, inter_point);
        }
    }
}

static void
compute_node_forces(Node *node, Node *tree)
{
    if (tree->num_points == 0) {
        return;
    } else if (must_subdivide(tree, node)) {
        if (tree->is_internal) {
            // Consider this node's children. We might split or just use centers of mass.
            for (int i = 0; i < N_CHILDREN; i++) {
                Node *child = tree->children[i];
                compute_node_forces(node, child);
            }
        } else {
            // Add forces for the points that constitute this leaf.
            for (int i = 0; i < tree->num_points; i++) {
                Point *inter_point = tree->points[i];
                sum_force(node, inter_point);
            }
        }
    } else {
        // No subdividing: use the tree's center of mass.
        sum_force(node, &tree->cm);
    }
}

static void
sequence_force_calculation(Node *node)
{
    if (node->is_cell) {
        // This node contains fewer than NCrit points.
        // cell_force_calculation(node);
        compute_node_forces(node, TreeRoot);
    } else {
        // Internal node with more than NCrit points -> split
        for (int i = 0; i < N_CHILDREN; i++) {
            Node *child = node->children[i];
            if (!child || child->num_points == 0) {
                continue;
            }
            sequence_force_calculation(child);
        }
    }
}

void
updated_barnes(Node *tree)
{
    sequence_force_calculation(tree);
}
