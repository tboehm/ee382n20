-- Edge-centric PageRank

import "regent"
require "math"

-- Helper module to handle command line arguments
local PageRankConfig = require("pagerank_config")

local c = regentlib.c

fspace Page {
  id           : int32;
  rank         : double;
  new_rank     : double;
  outgoing     : int32;
  incoming     : int32;
  color        : int1d;
}

fspace Link(r_pages: region(Page)) {
  src         : ptr(Page, r_pages);
  dst         : ptr(Page, r_pages);
}


terra skip_header(f : &c.FILE)
  var x : uint64, y : uint64
  c.fscanf(f, "%llu\n%llu\n", &x, &y)
end


terra read_ids(f : &c.FILE, page_ids : &uint32)
  return c.fscanf(f, "%d %d\n", &page_ids[0], &page_ids[1]) == 2
end


task initialize_graph(r_pages   : region(Page),
                      r_links   : region(Link(r_pages)),
                      damp      : double,
                      num_pages : uint64,
                      filename  : int8[512])
where
  reads writes(r_pages, r_links)
do
  var ts_start = c.legion_get_current_time_in_micros()

  var id = 0
  for page in r_pages do
    page.id = id
    page.rank = 1.0 / num_pages
    page.new_rank = (1.0 - damp) / num_pages
    page.outgoing = 0
    page.incoming = 0
    id += 1
  end

  var f = c.fopen(filename, "rb")
  skip_header(f)
  var page_ids : uint32[2]
  for link in r_links do
    regentlib.assert(read_ids(f, page_ids), "Less data than there should be")
    var src_page = dynamic_cast(ptr(Page, r_pages), page_ids[0])
    var dst_page = dynamic_cast(ptr(Page, r_pages), page_ids[1])
    -- Initialize the link with 'src_page' and 'dst_page'
    r_pages[page_ids[0]].outgoing += 1
    r_pages[page_ids[1]].incoming += 1
    link.src = src_page
    link.dst = dst_page
  end
  c.fclose(f)
  var ts_stop = c.legion_get_current_time_in_micros()
  c.printf("Graph initialization took %.4f sec\n", (ts_stop - ts_start) * 1e-6)
end


task page_rank_compute_updates(
  num_pages : uint64,
  damp      : double,
  all_pages : region(Page),
  dst_pages : region(Page),
  links     : region(Link(all_pages))
)
where
  reads (all_pages.{rank,outgoing,id}, dst_pages.{rank,new_rank}, links),
  writes (dst_pages.new_rank)
do
  -- Compute new ranks based on incoming links.
  for link in links do
    dst_pages[link.dst.id].new_rank += damp * link.src.rank / link.src.outgoing
  end
end


task page_rank_update_ranks(
  new_rank : double,
  pages : region(Page)
)
where
  reads (pages.{rank, new_rank}),
  writes (pages.{rank, new_rank})
do
  -- Set the new ranks and compute the squared error.
  var err : double = 0.0

  for page in pages do
    var diff = page.rank - page.new_rank
    page.rank = page.new_rank
    page.new_rank = new_rank
    err += diff * diff
  end

  return err
end


task dump_ranks(r_pages  : region(Page),
                filename : int8[512])
where
  reads(r_pages.rank)
do
  var f = c.fopen(filename, "w")
  for page in r_pages do c.fprintf(f, "%g\n", page.rank) end
  c.fclose(f)
end


task print_info(
  color       : int,
  r_pages     : region(Page),
  r_pages_dst : region(Page),
  r_links     : region(Link(r_pages))
)
where
  reads(r_pages, r_links, r_pages_dst)
do
  for page in r_pages do
    c.printf("%d: page %d (%d)\n", color, page.id, page.color)
  end

  for page in r_pages_dst do
    c.printf("%d: dst page %d\n", color, page.id)
  end

  for link in r_links do
    c.printf("%d: link (%d -> %d)\n", color, link.src, link.dst)
  end
end


task toplevel()
  var config : PageRankConfig
  config:initialize_from_command()
  c.printf("**********************************\n")
  c.printf("* PageRank                       *\n")
  c.printf("*                                *\n")
  c.printf("* Number of Pages  : %11lu *\n",  config.num_pages)
  c.printf("* Number of Links  : %11lu *\n",  config.num_links)
  c.printf("* Damping Factor   : %11.4f *\n", config.damp)
  c.printf("* Error Bound      : %11g *\n",   config.error_bound)
  c.printf("* Max # Iterations : %11u *\n",   config.max_iterations)
  c.printf("* # Parallel Tasks : %11u *\n",   config.parallelism)
  c.printf("**********************************\n")

  -- Colorspace: one color per parallel resource.
  var cs = ispace(int1d, config.parallelism)

  -- Create a region of pages
  var is_pages = ispace(ptr, config.num_pages)
  var r_pages = region(is_pages, Page)
  -- var r_pages_copy = region(is_pages, Page)

  var is_links = ispace(ptr, config.num_links)
  var r_links = region(is_links, Link(wild)) -- Original ("wild")
  -- var r_links = region(is_links, Link(dst)) -- Not sure what I want to do here.

  -- Large graphs will spawn a task to update the ranks and compute the error in one go.
  var r_errors = region(cs, double)

  -- Initialize the page graph from a file
  initialize_graph(r_pages, r_links, config.damp, config.num_pages, config.input)

  -- Set each node's color such that we have roughly the same number of links assigned to each.
  var links_per_color = config.num_links / config.parallelism
  var next_cutoff = links_per_color
  var current_sum = 0
  var current_color : int1d = 0
  for i in r_pages.ispace do
    r_pages[i].color = current_color
    -- r_pages_copy[i].color = current_color
    current_sum += r_pages[i].incoming
    if current_sum >= next_cutoff then
      current_color += 1
      -- Recompute the cutoff rather than increment because `num_links` might not be divisible
      -- by `parallelism`.
      next_cutoff = (current_color + 1) * config.num_links / config.parallelism
    end
  end

  -- Use the number of incoming links as a weight for the partitioning. Much of this logic is based
  -- on the partitioning in legion/regent/examples/circuit/circuit_dep_par3.rg.

  -- "Destination" nodes: Each parallel task computes new_rank for each of these.
  var p_pages_dst = partition(r_pages.color, cs)

  -- Links that end at nodes in p_pages_dst.
  var p_links = preimage(r_links, p_pages_dst, r_links.dst)

  -- Links that start at nodes in p_pages_dst.
  -- var p_pages_src = image(r_pages, p_links, r_links.src)

  -- Links that start and end in p_pages_dst can be modified as we see fit.
  -- var p_pages_private = p_pages_src & p_pages_dst

  -- Links that start in another partition can't be modified until all new_rank values are ready.
  -- var p_pages_shared = p_pages_dst - p_pages_private

  -- Print the pages, dst pages, and links for each partition (debug)
  -- for color in cs do
  --   print_info(color, r_pages[color], p_pages_dst[color], p_links[color])
  -- end

  var converged = false
  var num_iterations = 0
  __fence(__execution, __block) -- This blocks to make sure we only time the pagerank computation
  var ts_start = c.legion_get_current_time_in_micros()
  var new_rank : double = (1 - config.damp) / config.num_pages
  while not converged do
    var ts_iter_start = c.legion_get_current_time_in_micros()

    num_iterations += 1

    -- Compute the new ranks.
    __demand(__index_launch)
    for i in cs do
      page_rank_compute_updates(
        config.num_pages,
        config.damp,
        r_pages,
        p_pages_dst[i],
        p_links[i]
      )
    end

    var total_error : double = 0.0
    if config.num_pages >= 131072 then
      -- For larger graphs, update the ranks in parallel.
      for i in cs do
        r_errors[i] = page_rank_update_ranks(new_rank, p_pages_dst[i])
      end

      for i in cs do
        total_error += r_errors[i]
      end
    else
      -- Not worth the overhead of parallelizing for smaller graphs. Update serially.
      total_error = page_rank_update_ranks(new_rank, r_pages)
    end
    total_error = c.sqrt(total_error)

    converged = total_error < config.error_bound

    var ts_iter_end = c.legion_get_current_time_in_micros()

    c.printf(
      "Iteration %d: error %g, completed in %.2f ms.\n",
      num_iterations,
      total_error,
      (ts_iter_end - ts_iter_start) * 1e-3
    )

    if num_iterations > config.max_iterations then
      c.printf("\27[33;1mStopping early due to iteration cap.\27[0m\n")
      break
    end
  end
  __fence(__execution, __block) -- This blocks to make sure we only time the pagerank computation
  var ts_stop = c.legion_get_current_time_in_micros()
  c.printf("PageRank converged after %d iterations in %.4f sec\n",
  num_iterations, (ts_stop - ts_start) * 1e-6)

  if config.dump_output then dump_ranks(r_pages, config.output) end
end

regentlib.start(toplevel)
