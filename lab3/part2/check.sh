#!/bin/bash

rm -rf out
mkdir out

count=0
for i in {1..6}
do
  echo "checking example $i..."
  iter=$(regent.py pagerank.rg -i examples/example$i.dat -o out/$i | grep -oP "converged after \s*\K\d+")
  diff out/$i examples/references/example$i.result.iter$iter
done

# clean up
rm -rf out

echo "done."
