==10216== NVPROF is profiling process 10216, command: ./cudaSaxpyUvm -n 500000000 -p 1 -i 15
---------------------------------------------------------
Found 4 CUDA devices
Device 0: GeForce GTX 1080 Ti
   SMs:        28
   Global mem: 11178 MB
   CUDA Cap:   6.1
Device 1: GeForce GTX 1080 Ti
   SMs:        28
   Global mem: 11178 MB
   CUDA Cap:   6.1
Device 2: GeForce GTX 1080 Ti
   SMs:        28
   Global mem: 11178 MB
   CUDA Cap:   6.1
Device 3: GeForce GTX 1080 Ti
   SMs:        28
   Global mem: 11178 MB
   CUDA Cap:   6.1
---------------------------------------------------------
Overall time :  683.767 ms [   2.322 GB/s ]
GPU Kernel   :  491.930 ms [   1.076 Ops/s]
Copy CPU->GPU:   31.385 ms [  33.730 GB/s ]
Copy CPU<-GPU:  160.437 ms [   3.299 GB/s ]
CPU time     : 1451.785 ms [   1.094 GB/s ]
check_saxpy
Test succeeded
==10216== Profiling application: ./cudaSaxpyUvm -n 500000000 -p 1 -i 15
==10216== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:  100.00%  7.37465s        15  491.64ms  242.17ms  516.65ms  saxpy_kernel(int, float, float*, float*, float*)
      API calls:   67.59%  7.37481s        15  491.65ms  242.17ms  516.66ms  cudaDeviceSynchronize
                   26.37%  2.87730s        45  63.940ms  885.24us  271.07ms  cudaMemPrefetchAsync
                    2.98%  324.80ms         3  108.27ms  101.41ms  112.31ms  cudaFree
                    2.96%  323.28ms         3  107.76ms  42.020us  323.15ms  cudaMallocManaged
                    0.04%  4.0231ms        15  268.21us  36.153us  3.4298ms  cudaLaunchKernel
                    0.02%  1.8652ms       384  4.8570us     123ns  271.68us  cuDeviceGetAttribute
                    0.02%  1.8441ms         4  461.03us  412.90us  488.04us  cudaGetDeviceProperties
                    0.02%  1.8107ms         4  452.67us  449.28us  457.63us  cuDeviceTotalMem
                    0.01%  564.05us         4  141.01us  1.3590us  557.63us  cuDeviceGetPCIBusId
                    0.00%  134.29us         4  33.573us  30.312us  42.819us  cuDeviceGetName
                    0.00%  47.941us        15  3.1960us  2.5320us  4.8010us  cudaGetDevice
                    0.00%  10.558us        15     703ns     478ns  1.1840us  cudaPeekAtLastError
                    0.00%  5.3240us         1  5.3240us  5.3240us  5.3240us  cudaGetDeviceCount
                    0.00%  1.6960us         8     212ns     117ns     753ns  cuDeviceGet
                    0.00%  1.0240us         3     341ns     133ns     703ns  cuDeviceGetCount

==10216== Unified Memory profiling result:
Device "GeForce GTX 1080 Ti (0)"
   Count  Avg Size  Min Size  Max Size  Total Size  Total Time  Name
  796666  36.264KB  4.0000KB  2.0000MB  27.55179GB   3.229934s  Host To Device
   33110  749.46KB  4.0000KB  2.0000MB  23.66502GB   1.958369s  Device To Host
   54303         -         -         -           -   7.044150s  Gpu page fault groups
Total CPU Page faults: 22896
