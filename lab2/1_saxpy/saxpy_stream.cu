#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_functions.h>

#include "CycleTimer.h"
#include "saxpy.h"
#include "error_check.cuh"

__global__ void
saxpy_kernel(int N, float alpha, float* x, float* y, float* result) {

    // compute overall index from position of thread in current block,
    // and given the block we are in
    int index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index < N) {
        result[index] = alpha * x[index] + y[index];
    }
}

static inline
int getBlocks(long working_set_size, int threadsPerBlock) {
    return ceil(working_set_size / (float) threadsPerBlock); 
}

void 
getArrays(int size, float **xarray, float **yarray, float **resultarray) {
    ERROR_CHECK(cudaMallocHost(xarray, size));
    ERROR_CHECK(cudaMallocHost(yarray, size));
    ERROR_CHECK(cudaMallocHost(resultarray, size));
}

void 
freeArrays(float *xarray, float *yarray, float *resultarray) {
    cudaFreeHost(xarray);
    cudaFreeHost(yarray);
    cudaFreeHost(resultarray);
}

void
saxpyCuda(long total_elems, float alpha, float* xarray, float* yarray, float* resultarray, int partitions) {

    const int threadsPerBlock = THREADS_PER_BLOCK; // change this if necessary

    float *device_x;
    float *device_y;
    float *device_result;

    //
    // allocate device memory buffers on the GPU using
    // cudaMalloc.  The started code issues warnings on build because
    // these buffers are used in the call to saxpy_kernel below
    // without being initialized.
    //
    int totalSize = total_elems * sizeof(float);
    ERROR_CHECK(cudaMalloc(&device_x, totalSize));
    ERROR_CHECK(cudaMalloc(&device_y, totalSize));
    ERROR_CHECK(cudaMalloc(&device_result, totalSize));
    
    // start timing after allocation of device memory.
    double startTime = CycleTimer::currentSeconds();
    int partSize = ceil(total_elems / (float) partitions);

    cudaStream_t streams[partitions];

    for (int i=0; i<partitions; i++) {
        cudaStreamCreate(&streams[i]);

        int offset = partSize * i;
        int subSize = i == partitions-1 ? total_elems - partSize * i : partSize;
  
        //
        // copy input arrays to the GPU using cudaMemcpy
        //
        double H2DCopyTimeStart = CycleTimer::currentSeconds();
        cudaMemcpyAsync(device_x+offset, xarray+offset, sizeof(float)*subSize, cudaMemcpyHostToDevice, streams[i]);
        cudaMemcpyAsync(device_y+offset, yarray+offset, sizeof(float)*subSize, cudaMemcpyHostToDevice, streams[i]);
        double H2DCopyTimeEnd = CycleTimer::currentSeconds();
        double H2DCopyTime = H2DCopyTimeEnd - H2DCopyTimeStart;
        timeCopyH2DAvg += H2DCopyTime;
         
        //
        // insert time here to begin timing only the kernel
        //
        double startGPUTime = CycleTimer::currentSeconds();
    
        // compute number of blocks and threads per block
        int numThreadBlocks = getBlocks(subSize, threadsPerBlock);

        // run saxpy_kernel on the GPU
        saxpy_kernel<<<numThreadBlocks, threadsPerBlock, 0, streams[i]>>>(subSize, alpha,
                device_x+offset, device_y+offset, device_result+offset);
    
        //
        // time only the kernel.  Since the
        // kernel will run asynchronously with the calling CPU thread, you
        // need to call cudaDeviceSynchronize() before your timer to
        // ensure the kernel running on the GPU has completed.  (Otherwise
        // you will incorrectly observe that almost no time elapses!)
        //
        // cudaStreamSynchronize(streams[i]);
        double endGPUTime = CycleTimer::currentSeconds();
        double timeKernel = endGPUTime - startGPUTime;
        timeKernelAvg += timeKernel;
    
        cudaError_t errCode = cudaPeekAtLastError();
        if (errCode != cudaSuccess) {
            fprintf(stderr, "WARNING: A CUDA error occured: code=%d, %s\n", errCode, cudaGetErrorString(errCode));
            // fprintf(stderr, "%d: nb=%d size=%d stream=%p\n", i, numThreadBlocks, subSize, streams[i]);
            // fprintf(stderr, "%d: host x=%p y=%p res=%p\n", i, xarray, yarray, resultarray);
            // fprintf(stderr, "%d: device x=%p y=%p res=%p\n", i, device_x, device_y, device_result);
        }
    
        //
        // copy result from GPU using cudaMemcpy
        //
        double D2HCopyTimeStart = CycleTimer::currentSeconds();
        cudaMemcpyAsync(resultarray+offset, device_result+offset, sizeof(float)*subSize, cudaMemcpyDeviceToHost, streams[i]);
        double D2HCopyTimeEnd = CycleTimer::currentSeconds();
        timeCopyD2HAvg += D2HCopyTimeEnd - D2HCopyTimeStart;
    }

    for (int i = 0; i < partitions; i++) {
        cudaStreamDestroy(streams[i]);
    }

    // end timing after result has been copied back into host memory.
    // The time elapsed between startTime and endTime is the total
    // time to copy data to the GPU, run the kernel, and copy the
    // result back to the CPU
    double endTime = CycleTimer::currentSeconds();
    double overallDuration = endTime - startTime;
    totalTimeAvg += overallDuration;

    //
    // free memory buffers on the GPU
    //
    cudaFree(device_x);
    cudaFree(device_y);
    cudaFree(device_result);
}

void
printCudaInfo() {

    // for fun, just print out some stats on the machine

    int deviceCount = 0;
    cudaError_t err = cudaGetDeviceCount(&deviceCount);

    printf("---------------------------------------------------------\n");
    printf("Found %d CUDA devices\n", deviceCount);

    for (int i=0; i<deviceCount; i++) {
        cudaDeviceProp deviceProps;
        cudaGetDeviceProperties(&deviceProps, i);
        printf("Device %d: %s\n", i, deviceProps.name);
        printf("   SMs:        %d\n", deviceProps.multiProcessorCount);
        printf("   Global mem: %.0f MB\n",
               static_cast<float>(deviceProps.totalGlobalMem) / (1024 * 1024));
        printf("   CUDA Cap:   %d.%d\n", deviceProps.major, deviceProps.minor);
    }
    printf("---------------------------------------------------------\n");
}
