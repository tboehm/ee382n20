#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_functions.h>

#include "CycleTimer.h"
#include "saxpy.h"

__global__ void
saxpy_kernel(int N, float alpha, float* x, float* y, float* result) {

    // compute overall index from position of thread in current block,
    // and given the block we are in
    int index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index < N)
       result[index] = alpha * x[index] + y[index];
}

static inline
int getBlocks(long working_set_size, int threadsPerBlock) {
    return ceil(working_set_size / (float) threadsPerBlock);
}

void 
getArrays(int size, float **xarray, float **yarray, float **resultarray) {
    cudaMallocManaged(xarray, size);
    cudaMallocManaged(yarray, size);
    cudaMallocManaged(resultarray, size);
}

void 
freeArrays(float *xarray, float *yarray, float *resultarray) {
    cudaFree(xarray);
    cudaFree(yarray);
    cudaFree(resultarray);
}

void
saxpyCuda(long total_elems, float alpha, float* xarray, float* yarray, float* resultarray, int partitions) {

    const int threadsPerBlock = THREADS_PER_BLOCK; // change this if necessary

    // start timing after allocation of device memory.
    double startTime = CycleTimer::currentSeconds();

    // compute number of blocks and threads per block
    int numThreadBlocks = getBlocks(total_elems, threadsPerBlock); 
    int deviceId;
    cudaGetDevice(&deviceId);

    // prefetch to device 
    double H2DCopyTimeStart = CycleTimer::currentSeconds();
    int size = total_elems * sizeof(float);
    cudaMemPrefetchAsync(xarray, size, deviceId);
    cudaMemPrefetchAsync(yarray, size, deviceId);
    double H2DCopyTimeEnd = CycleTimer::currentSeconds();
    double H2DCopyTime = H2DCopyTimeEnd - H2DCopyTimeStart;
    timeCopyH2DAvg += H2DCopyTime;

    // Start Timing Kernel
    double startGPUTime = CycleTimer::currentSeconds();

    // run saxpy_kernel on the GPU
    saxpy_kernel<<<numThreadBlocks, threadsPerBlock>>>(total_elems,
            alpha, xarray, yarray, resultarray);

    cudaDeviceSynchronize();

    // Time Kernel
    double endGPUTime = CycleTimer::currentSeconds();
    double timeKernel = endGPUTime - startGPUTime;
    timeKernelAvg += timeKernel;

    cudaError_t errCode = cudaPeekAtLastError();
    if (errCode != cudaSuccess) {
        fprintf(stderr, "WARNING: A CUDA error occured: code=%d, %s\n", errCode, cudaGetErrorString(errCode));
    }

    // prefetch from device 
    double D2HCopyTimeStart = CycleTimer::currentSeconds();
    cudaMemPrefetchAsync(resultarray, size, cudaCpuDeviceId);
    double D2HCopyTimeEnd = CycleTimer::currentSeconds();
    timeCopyD2HAvg += D2HCopyTimeEnd - D2HCopyTimeStart;
    
    double endTime = CycleTimer::currentSeconds();
    double overallDuration = endTime - startTime;
    totalTimeAvg   += overallDuration;

}

void
printCudaInfo() {

    // for fun, just print out some stats on the machine

    int deviceCount = 0;
    cudaError_t err = cudaGetDeviceCount(&deviceCount);

    printf("---------------------------------------------------------\n");
    printf("Found %d CUDA devices\n", deviceCount);

    for (int i=0; i<deviceCount; i++) {
        cudaDeviceProp deviceProps;
        cudaGetDeviceProperties(&deviceProps, i);
        printf("Device %d: %s\n", i, deviceProps.name);
        printf("   SMs:        %d\n", deviceProps.multiProcessorCount);
        printf("   Global mem: %.0f MB\n",
               static_cast<float>(deviceProps.totalGlobalMem) / (1024 * 1024));
        printf("   CUDA Cap:   %d.%d\n", deviceProps.major, deviceProps.minor);
    }
    printf("---------------------------------------------------------\n");
}
