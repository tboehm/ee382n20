#ifndef __SCAN_H__
#define __SCAN_H__

#define THREADS_PER_BLOCK 512

void exclusive_scan(int* device_start, int length, int* device_result);
double cudaScan(int* inarray, int* end, int* resultarray);
double cudaScanThrust(int* inarray, int* end, int* resultarray);
int find_repeats(int *device_input, int length, int *device_output);
double cudaFindRepeats(int *input, int length, int *output, int *output_length);
void printCudaInfo();

extern double timeCopyH2DAvg;
extern double timeCopyD2HAvg;
extern double timeKernelAvg;
extern double totalTimeAvg;

#endif // __SCAN_H__
