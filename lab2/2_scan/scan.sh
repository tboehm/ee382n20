#!/bin/bash

#SBATCH -J scan
#SBATCH -o scan.out%j  # Name of stdout output file
#SBATCH -e scan.err%j  # Name of stderr error file
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p gtx
#SBATCH -t 00:05:00
#SBATCH --mail-user=tboehm@utexas.edu
#SBATCH --mail-type=none         # Send email at begin and end of job
#SBATCH -A EE382N-20-Parallelis

mkdir -p jobs/${SLURM_JOBID}

module load gcc
module load cuda
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/apps/cuda/8.0/lib64
./checker.pl scan
./checker.pl find_repeats

mv scan.out${SLURM_JOBID} jobs/${SLURM_JOBID}/out
mv scan.err${SLURM_JOBID} jobs/${SLURM_JOBID}/err

rm -f jobs/last
ln -s $(realpath jobs/${SLURM_JOBID}) jobs/last
