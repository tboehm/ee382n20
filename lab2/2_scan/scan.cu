#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_functions.h>

#include <thrust/scan.h>
#include <thrust/device_ptr.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>

#include "CycleTimer.h"
#include "scan.h"
#include "error_check.cuh"

extern float toBW(int bytes, float sec);


/* Helper function to round up to a power of 2.
 */
static inline int nextPow2(int n)
{
    n--;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;
    return n;
}

static inline int
threads_per_block(int working_set)
{
    return working_set > THREADS_PER_BLOCK ? THREADS_PER_BLOCK : working_set;
}

__global__ void
scan_kernel_upsweep(int N, int twod, int twod1, int *output)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int i = index * twod1;

    if (i + twod1 <= N && i + twod <= N) {
        output[i + twod1 - 1] += output[i + twod - 1];
    }

    if ((twod == (N / 2)) && (i + twod1 == N)) {
        output[N - 1] = 0;
    }
    // __syncthreads();
}

__global__ void
scan_kernel_downsweep(int N, int twod, int twod1, int *output)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int i = index * twod1;

    if (i + twod1 <= N && i + twod <= N) {
        int t = output[i + twod - 1];
        output[i + twod - 1] = output[i + twod1 - 1];
        output[i + twod1 - 1] += t;
    }
    // __syncthreads();
}

static inline
int getBlocks(long working_set_size)
{
    return ceil(working_set_size / (float) THREADS_PER_BLOCK);
}

void
scanCudaKernels(long length, int *outputs)
{
    int numThreadBlocks;
    int threadsPerBlock;
    int twod;
    int twod1;
    int working_set;

    // Upsweep phase
    for (twod = 1, twod1 = 2, working_set = length / 2;
         twod < length;
         twod *= 2, twod1 *= 2, working_set /= 2) {
        numThreadBlocks = getBlocks(working_set);
        threadsPerBlock = threads_per_block(working_set);
        scan_kernel_upsweep<<<numThreadBlocks, threadsPerBlock>>>(
            length,
            twod,
            twod1,
            outputs
        );
    }

    // Make sure the first phase completes.
    ERROR_CHECK(cudaDeviceSynchronize());

    for (twod = length / 2, twod1 = length, working_set = 1;
         twod >= 1;
         twod /= 2, twod1 /= 2, working_set *= 2) {
        numThreadBlocks = getBlocks(working_set);
        threadsPerBlock = threads_per_block(working_set);
        scan_kernel_downsweep<<<numThreadBlocks, threadsPerBlock>>>(
            length,
            twod,
            twod1,
            outputs
        );
    }

    // Wait for the results.
    ERROR_CHECK(cudaDeviceSynchronize());
}

void
exclusive_scan(int *device_start, int length, int *device_result)
{
    /* Fill in this function with your exclusive scan implementation.
     * You are passed the locations of the input and output in device memory,
     * but this is host code -- you will need to declare one or more CUDA
     * kernels (with the __global__ decorator) in order to actually run code
     * in parallel on the GPU.
     * Note you are given the real length of the array, but may assume that
     * both the input and the output arrays are sized to accommodate the next
     * power of 2 larger than the input.
     */
    int pow2_len = nextPow2(length);
    scanCudaKernels(pow2_len, device_result);
}

/* This function is a wrapper around the code you will write - it copies the
 * input to the GPU and times the invocation of the exclusive_scan() function
 * above. You should not modify it.
 */
double
cudaScan(int* inarray, int* end, int* resultarray)
{
    int* device_result;
    int* device_input;
    // We round the array sizes up to a power of 2, but elements after
    // the end of the original input are left uninitialized and not checked
    // for correctness.
    // You may have an easier time in your implementation if you assume the
    // array's length is a power of 2, but this will result in extra work on
    // non-power-of-2 inputs.
    int rounded_length = nextPow2(end - inarray);
    ERROR_CHECK(cudaMalloc((void **)&device_result, sizeof(int) * rounded_length));
    ERROR_CHECK(cudaMalloc((void **)&device_input, sizeof(int) * rounded_length));
    ERROR_CHECK(cudaMemcpy(device_input, inarray, (end - inarray) * sizeof(int),
               cudaMemcpyHostToDevice));

    // For convenience, both the input and output vectors on the device are
    // initialized to the input values. This means that you are free to simply
    // implement an in-place scan on the result vector if you wish.
    // If you do this, you will need to keep that fact in mind when calling
    // exclusive_scan from find_repeats.
    ERROR_CHECK(cudaMemcpy(device_result, inarray, (end - inarray) * sizeof(int),
               cudaMemcpyHostToDevice));

    double startTime = CycleTimer::currentSeconds();

    exclusive_scan(device_input, end - inarray, device_result);

    // Wait for any work left over to be completed.
    ERROR_CHECK(cudaDeviceSynchronize());
    double endTime = CycleTimer::currentSeconds();
    double overallDuration = endTime - startTime;

    ERROR_CHECK(cudaMemcpy(resultarray, device_result, (end - inarray) * sizeof(int),
               cudaMemcpyDeviceToHost));
    return overallDuration;
}

/* Wrapper around the Thrust library's exclusive scan function
 * As above, copies the input onto the GPU and times only the execution
 * of the scan itself.
 * You are not expected to produce competitive performance to the
 * Thrust version.
 */
double cudaScanThrust(int* inarray, int* end, int* resultarray) {

    int length = end - inarray;
    thrust::device_ptr<int> d_input = thrust::device_malloc<int>(length);
    thrust::device_ptr<int> d_output = thrust::device_malloc<int>(length);

    ERROR_CHECK(cudaMemcpy(d_input.get(), inarray, length * sizeof(int),
               cudaMemcpyHostToDevice));

    double startTime = CycleTimer::currentSeconds();

    thrust::exclusive_scan(d_input, d_input + length, d_output);

    // Wait for any work left over to be completed.
    ERROR_CHECK(cudaDeviceSynchronize());
    double endTime = CycleTimer::currentSeconds();

    ERROR_CHECK(cudaMemcpy(resultarray, d_output.get(), length * sizeof(int),
               cudaMemcpyDeviceToHost));
    thrust::device_free(d_input);
    thrust::device_free(d_output);
    double overallDuration = endTime - startTime;
    return overallDuration;
}

// TODO: Split this into two kernels for less branch divergence?
__global__ void
cuda_create_masks(const int *A, int *mask, int *i_up, int total_elems)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;

    if (index < (total_elems - 1)) {
        int eq = A[index] == A[index + 1];
        mask[index] = eq;
        i_up[index] = eq;
    } else {
        mask[index] = 0;
        i_up[index] = 0;
    }
}

__global__ void
cuda_subtract_array(int *A, int N, int minuend)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    A[index] = minuend - A[index];
}

__global__ void
cuda_index_outputs(const int *mask, const int *i_up, int *outputs, int array_len)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    int output_index;

    if (mask[index]) {
        output_index = i_up[index];
        outputs[array_len - output_index] = index;
    }
}

__device__ long device_n_output_pairs;
__global__ void
cuda_count_pairs(const int *outputs, int array_len)
{
    int high = array_len;
    int low = 0;
    int position;

    while (high != low) {
        position = (high + low) / 2;
        if (outputs[position] != 0 && outputs[position + 1] == 0) {
            device_n_output_pairs = position + 1;
            return;
        } else if (outputs[position] == 0) {
            high = position;
        } else {
            low = position;
        }
    }

    // Shouldn't reach this point
    // high == low == position
    device_n_output_pairs = 0;
}

int
find_repeats(int *device_input, int length, int *device_output)
{
    /* Finds all pairs of adjacent repeated elements in the list, storing the
     * indices of the first element of each pair (in order) into device_result.
     * Returns the number of pairs found.
     * Your task is to implement this function. You will probably want to
     * make use of one or more calls to exclusive_scan(), as well as
     * additional CUDA kernel launches.
     * Note: As in the scan code, we ensure that allocated arrays are a power
     * of 2 in size, so you can use your exclusive_scan function with them if
     * it requires that. However, you must ensure that the results of
     * find_repeats are correct given the original length.
     */
    int pow2_len = nextPow2(length);
    int numThreadBlocks = getBlocks(pow2_len);

    ///////////////////////
    // Memory allocation //
    ///////////////////////

    // Allocate device memory buffers on the GPU using cudaMalloc.
    int size = pow2_len * sizeof(int);

    int *device_mask;
    int *device_i_up;

    ERROR_CHECK(cudaMalloc(&device_mask, size));
    ERROR_CHECK(cudaMalloc(&device_i_up, size));

    // Create mask and flip_mask
    cuda_create_masks<<<numThreadBlocks, THREADS_PER_BLOCK>>>(
        device_input,
        device_mask,
        device_i_up,
        length
    );
    ERROR_CHECK(cudaDeviceSynchronize());

    // Run the "split" algorithm (BLE93) to put those elements with mask 1 at
    // the start of the array.

    // (1) i_down <- prescan(not(mask))
    // We can actually skip this since we aren't doing a radix sort (which is
    // where this algorithm came from). All elements that don't have a mask
    // should just be 0 in the output rather than working their way to the top.
    ERROR_CHECK(cudaMemset(device_output, 0, length * sizeof(int)));

    // (2) i_up <- N - scan(mask)
    scanCudaKernels(pow2_len, device_i_up);
    cuda_subtract_array<<<numThreadBlocks, THREADS_PER_BLOCK>>>(device_i_up, pow2_len, pow2_len);

    // We need i_up and device_outputs set before we can proceed
    ERROR_CHECK(cudaDeviceSynchronize());

    // (3) Put the outputs in place
    cuda_index_outputs<<<numThreadBlocks, THREADS_PER_BLOCK>>>(
        device_mask,
        device_i_up,
        device_output,
        pow2_len
    );

    ERROR_CHECK(cudaDeviceSynchronize());

    // (4) Count how many pairs there are
    cuda_count_pairs<<<1, 1>>>(device_output, pow2_len);

    ERROR_CHECK(cudaDeviceSynchronize());

    // typeof(device_n_output_pairs) n_output_pairs;
    int n_output_pairs;
    ERROR_CHECK(cudaMemcpyFromSymbol(
        &n_output_pairs,
        device_n_output_pairs,
        sizeof(n_output_pairs),
        0,
        cudaMemcpyDeviceToHost
    ));

    ERROR_CHECK(cudaFree(device_mask));
    ERROR_CHECK(cudaFree(device_i_up));

    return n_output_pairs;
}

/* Timing wrapper around find_repeats. You should not modify this function.
 */
double cudaFindRepeats(int *input, int length, int *output, int *output_length) {
    int *device_input;
    int *device_output;
    int rounded_length = nextPow2(length);
    ERROR_CHECK(cudaMalloc((void **)&device_input, rounded_length * sizeof(int)));
    ERROR_CHECK(cudaMalloc((void **)&device_output, rounded_length * sizeof(int)));
    ERROR_CHECK(cudaMemcpy(device_input, input, length * sizeof(int),
               cudaMemcpyHostToDevice));

    double startTime = CycleTimer::currentSeconds();

    int result = find_repeats(device_input, length, device_output);

    // Wait for any work left over to be completed.
    ERROR_CHECK(cudaDeviceSynchronize());
    double endTime = CycleTimer::currentSeconds();

    *output_length = result;

    ERROR_CHECK(cudaMemcpy(output, device_output, length * sizeof(int),
               cudaMemcpyDeviceToHost));

    ERROR_CHECK(cudaFree(device_input));
    ERROR_CHECK(cudaFree(device_output));

    return endTime - startTime;
}


void
printCudaInfo()
{
    // Print out some stats on the machine
    int deviceCount = 0;
    ERROR_CHECK(cudaGetDeviceCount(&deviceCount));

    printf("---------------------------------------------------------\n");
    printf("Found %d CUDA devices\n", deviceCount);

    for (int i=0; i<deviceCount; i++) {
        cudaDeviceProp deviceProps;
        cudaGetDeviceProperties(&deviceProps, i);
        printf("Device %d: %s\n", i, deviceProps.name);
        printf("   SMs:        %d\n", deviceProps.multiProcessorCount);
        printf("   Global mem: %.0f MB\n",
               static_cast<float>(deviceProps.totalGlobalMem) / (1024 * 1024));
        printf("   CUDA Cap:   %d.%d\n", deviceProps.major, deviceProps.minor);
    }
    printf("---------------------------------------------------------\n");
}
