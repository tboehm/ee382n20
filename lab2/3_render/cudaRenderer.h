#ifndef __CUDA_RENDERER_H__
#define __CUDA_RENDERER_H__

#ifndef uint
#define uint unsigned int
#endif

#include <stdint.h>

#include "circleRenderer.h"

#define BOX_W 32
#define BOX_H 32
#define PIXELS_PER_THREAD 4  // Strips 1 pixel wide and PIXELS_PER_THREAD pixels tall
#define BOX_THREADS (BOX_W*BOX_H / PIXELS_PER_THREAD)

#define BOX_CIRCLES_MAX_PER_THREAD 16
#define BOX_CIRCLES_MAX 512

class CudaRenderer : public CircleRenderer {

private:

    Image* image;
    SceneName sceneName;

    int numCircles;
    float* position;
    float* velocity;
    float* color;
    float* radius;
    int nBlocksX;
    int nBlocksY;

    float* cudaDevicePosition;
    float* cudaDeviceVelocity;
    float* cudaDeviceColor;
    float* cudaDeviceRadius;
    float* cudaDeviceImageData;

public:

    CudaRenderer();
    virtual ~CudaRenderer();

    const Image* getImage();

    void setup();

    void loadScene(SceneName name);

    void allocOutputImage(int width, int height);

    void clearImage();

    void advanceAnimation();

    void render();

    void shadePixel(
        int circleIndex,
        float pixelCenterX, float pixelCenterY,
        float px, float py, float pz,
        float* pixelData);
};


#endif
