#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_functions.h>
#include <cmath>
#include "CycleTimer.h"
#include "error_check.cuh"

#include "histo.h"

void printCudaInfo() {

    // for fun, just print out some stats on the machine

    int deviceCount = 0;
    cudaError_t err = cudaGetDeviceCount(&deviceCount);

    printf("---------------------------------------------------------\n");
    printf("Found %d CUDA devices\n", deviceCount);

    for (int i=0; i<deviceCount; i++) {
        cudaDeviceProp deviceProps;
        cudaGetDeviceProperties(&deviceProps, i);
        printf("Device %d: %s\n", i, deviceProps.name);
        printf("   SMs:        %d\n", deviceProps.multiProcessorCount);
        printf("   Global mem: %.0f MB\n",
               static_cast<float>(deviceProps.totalGlobalMem) / (1024 * 1024));
        printf("   CUDA Cap:   %d.%d\n", deviceProps.major, deviceProps.minor);
    }
    printf("---------------------------------------------------------\n");
}

extern float toBW(int bytes, float sec);

// edit arguments if necessary
__global__ void
histo_kernel(unsigned char *data, 
             unsigned long len_in_byte, 
             unsigned int *histo_r, 
             unsigned int *histo_g, 
             unsigned int *histo_b, 
             unsigned int bin_width) {
    // compute global index for each thread 
    // with thread ID within a thread block and thread block ID
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (4 * tid < len_in_byte) {
      int pos = int(data[4 * tid + kR])/bin_width;
      atomicAdd(&histo_r[pos], 1);
      pos = int(data[4 * tid + kG])/bin_width;
      atomicAdd(&histo_g[pos], 1);
      pos = int(data[4 * tid + kB])/bin_width;
      atomicAdd(&histo_b[pos], 1);
    }
}

// this function computes histogram and returns pointer of histogram result
unsigned int *histogramCuda(unsigned char *data, int image_len_in_byte, unsigned int num_bins) {
   
    double startTime = CycleTimer::currentSeconds();

    // allocate array for histogram for each R,G,B
    int bin_size_in_byte = num_bins*sizeof(unsigned int);
    unsigned int *histo = (unsigned int *)malloc(3 * bin_size_in_byte);

    unsigned int *r = histo;
    unsigned int *g = histo + num_bins;
    unsigned int *b = histo + num_bins*2;

    // compute bin width 
    int bin_width = (int)ceil(256. / num_bins);

    // compute number of blocks and threads per block
    int threads_per_block = 512;
    int num_blocks = ceil(image_len_in_byte / 4 / (float) threads_per_block); 

    // TODO compute an amount of work per each thread

    // allocate device memory for image and histogram on the GPU using cudaMalloc
    unsigned char *device_data;
    ERROR_CHECK(cudaMalloc(&device_data, image_len_in_byte));

    unsigned int *device_r, *device_g, *device_b;
    ERROR_CHECK(cudaMalloc(&device_r, bin_size_in_byte));
    ERROR_CHECK(cudaMalloc(&device_g, bin_size_in_byte));
    ERROR_CHECK(cudaMalloc(&device_b, bin_size_in_byte));
    cudaMemset(device_r, 0, bin_size_in_byte);
    cudaMemset(device_g, 0, bin_size_in_byte);
    cudaMemset(device_b, 0, bin_size_in_byte);

    // copy input image array to the GPU using cudaMemcpy
    cudaMemcpy(device_data, data, image_len_in_byte, cudaMemcpyHostToDevice);

    // run kernel
    histo_kernel<<<num_blocks, threads_per_block>>>(device_data, image_len_in_byte, device_r, device_g, device_b, bin_width);

    // sync kernel
    cudaDeviceSynchronize();
    cudaError_t errCode = cudaPeekAtLastError();
    if (errCode != cudaSuccess) {
        fprintf(stderr, "WARNING: A CUDA error occured while ending: code=%d, %s\n", errCode, cudaGetErrorString(errCode)); exit(-1);
    }

    // copy histogram from GPU using cudaMemcpy
    ERROR_CHECK(cudaMemcpy(r, device_r, bin_size_in_byte, cudaMemcpyDeviceToHost));
    ERROR_CHECK(cudaMemcpy(g, device_g, bin_size_in_byte, cudaMemcpyDeviceToHost));
    ERROR_CHECK(cudaMemcpy(b, device_b, bin_size_in_byte, cudaMemcpyDeviceToHost));

    double endTime = CycleTimer::currentSeconds();
    double overallDuration = endTime - startTime;
    printf("GPU Time  : %9.3lf ms\n", 1000 * overallDuration);

    
    // free memory buffers on the GPU
    cudaFree(device_data);
    cudaFree(device_r);
    cudaFree(device_g);
    cudaFree(device_b);

    return histo;
}

