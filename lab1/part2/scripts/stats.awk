#!/usr/bin/awk -f

BEGIN {
    FS = " ?[:=] ";
    perf_events_per_run = 4;

    header = "Compiler,Algorithm,M,N,P,Base,Seed,Execution time,GFLOPS,perf1,perf1_val,perf2,perf2_val,perf3,perf3_val,perf4,perf4_val"
    perf_events = 0;
    csv_line = "";
    print(header)
}

/^Launcher:.*.\/matmul/ {
    csv_line = csv_line "GCC,";
}

/^Launcher:.*.\/icc-opt-matmul/ {
    csv_line = csv_line "ICC,";
}

/^opt_(algorithm|M|N|P|base|seed)/ {
    csv_line = csv_line $2 ",";
}

/^(seconds|GFLOPS)/ {
    csv_line = csv_line $2 ",";
}

/^opt_perf/ {
    next;
}

# Should've made the output we printed nicer
/^(ref_cycles|instructions|l1_reads|l1_read_misses|l1_writes|l1_prefetches|llc_reads|llc_read_misses|llc_writes|llc_write_misses)/ {
    csv_line = csv_line $1 "," $2;
    perf_events = perf_events + 1;
    if (perf_events != perf_events_per_run) {
        csv_line = csv_line ",";
    }
}

perf_events == perf_events_per_run {
    print (csv_line);
    csv_line = "";
    perf_events = 0;
}
