import sys

# simple script to parse output from cache oblivious sweep
if __name__ == "__main__":
    current_base = 0
    current_size = 0
    records = {}
    with open(sys.argv[1], "r") as f:
        for line in f.readlines():
            for opt in ["opt_base", "opt_M", "GFLOPS", "l1_reads", "l1_read_misses"]:
                if opt in line and "Launcher:" not in line and "perf event" not in line:
                    if "opt_base" in line:
                        current_base = int(line.strip().split('=')[1])
                        if current_base not in records:
                            records[current_base] = {}
                    elif "opt_M" in line:
                        current_size = int(line.strip().split('=')[1])
                    else:
                        category = line.strip().split('=')[0]
                        measurement = float(line.strip().split('=')[1])
                        if category not in records[current_base]:
                            records[current_base][category] = 0
                        if current_size != 32:
                            records[current_base][category] += measurement
                    # print(line)

    for base, record in records.items():
        out = []
        out.append("base=%5d\t" % (base))
        for key, item in record.items():
            out.append("%s %15.2f\t" % (key, item))
        print("".join(out))

