#!/usr/bin/env python3

from itertools import zip_longest

MAX_PERF_EVENTS = 4

TRIALS = 20

#ALGORITHMS = ["ijk_normal", "ikj_normal", "ikj_l1_tiled", "ijk_oblivious"]
ALGORITHMS = ["ijk_normal"]

PERF_EVENTS = [
    "ref_cycles",
    "instructions",
    "l1_reads",
    # "l1_writes",
    "l1_read_misses",
    # "l1_prefetches",
    # "ref_cycles",
    # "instructions",
    # "llc_reads",
    # # "llc_writes",
    # "llc_read_misses",
    # # "llc_write_misses",
]

MAT_SIZES = [32, 512, 4096]


def grouper(n, iterable, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)


for alg in ALGORITHMS:
    for mat_size in MAT_SIZES:
        for perf_events in grouper(MAX_PERF_EVENTS, PERF_EVENTS):
            command = list()
            command = [
                "./matmul",
                "--algorithm",
                alg,
                "-M",
                str(mat_size),
                "-N",
                str(mat_size),
                "-P",
                str(mat_size),
                "--verbose",
            ]
            for i, perf_event in enumerate(perf_events):
                if not perf_event:
                    continue
                command.append("--perf" + str(i + 1))
                command.append(perf_event)
            for _ in range(TRIALS):
                print(' '.join(command))

            command = list()
            command = [
                "./matmul-O3",
                "--algorithm",
                alg,
                "-M",
                str(mat_size),
                "-N",
                str(mat_size),
                "-P",
                str(mat_size),
                "--verbose",
            ]
            for i, perf_event in enumerate(perf_events):
                if not perf_event:
                    continue
                command.append("--perf" + str(i + 1))
                command.append(perf_event)
            for _ in range(TRIALS):
                print(' '.join(command))

            command = list()

            command = [
                "./icc-matmul",
                "--algorithm",
                alg,
                "-M",
                str(mat_size),
                "-N",
                str(mat_size),
                "-P",
                str(mat_size),
                "--verbose",
            ]
            for i, perf_event in enumerate(perf_events):
                if not perf_event:
                    continue
                command.append("--perf" + str(i + 1))
                command.append(perf_event)
            for _ in range(TRIALS):
                print(' '.join(command))

            command = list()
            command = [
                "./icc-opt-matmul",
                "--algorithm",
                alg,
                "-M",
                str(mat_size),
                "-N",
                str(mat_size),
                "-P",
                str(mat_size),
                "--verbose",
            ]
            for i, perf_event in enumerate(perf_events):
                if not perf_event:
                    continue
                command.append("--perf" + str(i + 1))
                command.append(perf_event)
            for _ in range(TRIALS):
                print(' '.join(command))

            command = list()
            command = [
                "./icc-opt-O3-matmul",
                "--algorithm",
                alg,
                "-M",
                str(mat_size),
                "-N",
                str(mat_size),
                "-P",
                str(mat_size),
                "--verbose",
            ]
            for i, perf_event in enumerate(perf_events):
                if not perf_event:
                    continue
                command.append("--perf" + str(i + 1))
                command.append(perf_event)
            for _ in range(TRIALS):
                print(' '.join(command))

print()
