#ifndef __MATMUL_H__
#define __MATMUL_H__

#include "matrix.h"

void matmul_ijk(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ikj(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_jik(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_jki(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_kij(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_kji(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ijk_l1_tiled(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ijk_l2_tiled(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ikj_reg_tiled(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ikj_l1_tiled(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ikj_l2_tiled(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ikj_l3_tiled(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_hierarchical(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ijk_oblivious(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_ikj_oblivious(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_transpose_ijk(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_transpose_ijk_oblivious(struct matrix *A, struct matrix *B, struct matrix *C);

void matmul_transpose_ijk_l1_tiled(struct matrix *A, struct matrix *B, struct matrix *C);

int matmul_algorithm_number(const char *name);

struct matmul_func {
    void (*func)(struct matrix *, struct matrix *, struct matrix *);
    char name[32];
};

extern int BASE_BLOCK_SIZE;

static struct matmul_func MatMulFuncs[] = {
    { matmul_ijk, "ijk_normal" },
    { matmul_ikj, "ikj_normal" },
    { matmul_jik, "jik_normal" },
    { matmul_jki, "jki_normal" },
    { matmul_kij, "kij_normal" },
    { matmul_kji, "kji_normal" },
    { matmul_ijk_l1_tiled, "ijk_l1_tiled" },
    { matmul_ijk_l2_tiled, "ijk_l2_tiled" },
    { matmul_ikj_reg_tiled, "ikj_reg_tiled" },
    { matmul_ikj_l1_tiled, "ikj_l1_tiled" },
    { matmul_ikj_l2_tiled, "ikj_l2_tiled" },
    { matmul_ikj_l3_tiled, "ikj_l3_tiled" },
    { matmul_hierarchical, "ikj_hierarchical" },
    { matmul_ijk_oblivious, "ijk_oblivious" },
    { matmul_ikj_oblivious, "ikj_oblivious" },
    { matmul_transpose_ijk, "ijk_transpose"},
    { matmul_transpose_ijk_oblivious, "ijk_transpose_oblivious"},
    { matmul_transpose_ijk_l1_tiled, "ijk_transpose_l1_tiled"}
};

#define NUM_MATMUL_FUNCS (sizeof(MatMulFuncs) / sizeof(MatMulFuncs[0]))

#endif // __MATMUL_H__
