#define _GNU_SOURCE

#include "main.h"

#include "matmul.h"
#include "perf.h"

#include <getopt.h> // getopt_long() and macros
#include <stdlib.h> // malloc(), exit codes
#include <stdio.h> // printf()
#include <string.h> // strcmp()

#define PERF_EVENT_COUNT 4

// Set this value to whatever your CPU can support.
#define YOUR_CPUS_MAX_PERF_EVENTS 4

int L1BlockSize;
int L2BlockSize;

int
main(int argc, char *argv[])
{
    char *opt_algorithm = NULL;
    int algorithm_number = -1;
    bool opt_perf_enable = true;
    int opt_perf1 = 2;
    int opt_perf2 = 2;
    int opt_perf3 = 2;
    int opt_perf4 = 2;
    int opt_M = 512;
    int opt_N = 512;
    int opt_P = 512;
    int opt_base = 16;
    int opt_seed = 2112;
    int opt_block1 = 128;
    int opt_block2 = 512;
    int opt_check = false;
    bool opt_verbose = false;

    static struct option perf_options[] = {
        { "algorithm",     required_argument, 0, 'a'}, // MMM algorithm
        { "perf1",         required_argument, 0, '1'}, // performance event 1
        { "perf2",         required_argument, 0, '2'}, // performance event 2
        { "perf3",         required_argument, 0, '3'}, // performance event 3
        { "perf4",         required_argument, 0, '4'}, // performance event 4
        { "a_rows",        required_argument, 0, 'M'}, // rows in A
        { "b_rows",        required_argument, 0, 'N'}, // rows in B
        { "a_cols",        required_argument, 0, 'P'}, // cols in A / rows in B
        { "base_size",     required_argument, 0, 'b'}, // base case size for cache oblivious
        { "seed",          required_argument, 0, 's'}, // random seed
        { "block1",        required_argument, 0, 'k'}, // block for L1 tiling
        { "block2",        required_argument, 0, 'l'}, // block for L2 tiling
        { "no-perf",       no_argument,       0, 'n'}, // disable perf_event_open
        { "check",         no_argument,       0, 'c'}, // check matmul result
        { "help",          no_argument,       0, 'h'}, // print help and exit
        { "verbose",       no_argument,       0, 'v'}, // verbose or not
    };

    int option_index = 0;
    int c;
    while ((c = getopt_long(argc, argv, "a:1:2:3:4:5:M:N:P:b:s:k:l:nchv",
                            perf_options, &option_index)) != -1) {
        switch (c) {
        case 'a':
            opt_algorithm = (char *)optarg;
            algorithm_number = matmul_algorithm_number(opt_algorithm);
            if (algorithm_number < 0) {
                fprintf(stderr, "Unknown algorithm: '%s'\n", opt_algorithm);
                exit(EXIT_FAILURE);
            }
            break;
        case '1':
            opt_perf1 = perf_event_number(optarg);
            if (opt_perf1 == -1) {
                fprintf(stderr, "Unknown performance event: %s\n", optarg);
                exit(EXIT_FAILURE);
            }
            break;
        case '2':
            opt_perf2 = perf_event_number(optarg);
            if (opt_perf2 == -1) {
                fprintf(stderr, "Unknown performance event: %s\n", optarg);
                exit(EXIT_FAILURE);
            }
            break;
        case '3':
            opt_perf3 = perf_event_number(optarg);
            if (opt_perf3 == -1) {
                fprintf(stderr, "Unknown performance event: %s\n", optarg);
                exit(EXIT_FAILURE);
            }
            break;
        case '4':
            opt_perf4 = perf_event_number(optarg);
            if (opt_perf4 == -1) {
                fprintf(stderr, "Unknown performance event: %s\n", optarg);
                exit(EXIT_FAILURE);
            }
            break;
        case 'M':
            opt_M = atoi(optarg);
            break;
        case 'N':
            opt_N = atoi(optarg);
            break;
        case 'P':
            opt_P = atoi(optarg);
            break;
        case 'b':
            opt_base = atoi(optarg);
            break;
        case 's':
            opt_seed = atoi(optarg);
            break;
        case 'k':
            opt_block1 = atoi(optarg);
            break;
        case 'l':
            opt_block2 = atoi(optarg);
            break;
        case 'n':
            opt_perf_enable = false;
            break;
        case 'c':
            opt_check = true;
            break;
        case 'h':
            help(argv);
            exit(EXIT_SUCCESS);
        case 'v':
            opt_verbose = true;
            break;
        case '?':
            break;
        default:
            printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (optind < argc) {
        fprintf(stderr, "Non-option argv elements: ");
        while (optind < argc) {
            printf("%s ", argv[optind++]);
        }
        printf("\n");
        exit(EXIT_FAILURE);
    }

    if (opt_verbose) {
        printf("opt_algorithm = %s\n", opt_algorithm);
        printf("opt_perf1 = %d\n", opt_perf1);
        printf("opt_perf2 = %d\n", opt_perf2);
        printf("opt_perf3 = %d\n", opt_perf3);
        printf("opt_perf4 = %d\n", opt_perf4);
        printf("opt_M = %d\n", opt_M);
        printf("opt_N = %d\n", opt_N);
        printf("opt_P = %d\n", opt_P);
        printf("opt_base = %d\n", opt_base);
        printf("opt_seed = %d\n", opt_seed);
        printf("opt_block1 = %d\n", opt_block1);
        printf("opt_block2 = %d\n", opt_block2);
        printf("opt_perf_enable = %d\n", opt_perf_enable);
    } else {
        printf("\033[36mUsing algorithm '%s'\n", opt_algorithm);
        printf("Matrix dimensions: A is %dx%d, B is %dx%d, C is %dx%d\033[0m\n",
               opt_M, opt_P, opt_P, opt_N, opt_M, opt_P);
    }

    // set global variable for cache oblivious base case size
    BASE_BLOCK_SIZE = opt_base;

    // Set global variables for cache-aware block sizes
    L1BlockSize = opt_block1;
    L2BlockSize = opt_block2;

    // Allocate and prepare the matrices we're going to use. A and B get some
    // random starting values.
    struct matrix *A = matrix_new(opt_M, opt_P);
    struct matrix *B = matrix_new(opt_P, opt_N);
    struct matrix *C = matrix_new(opt_M, opt_N);

    srand(opt_seed);
    matrix_randomize(A);
    matrix_randomize(B);

    // Lock to the current CPU for more accurate readings.
    lock_cpu(0);

    // Set up the performance counters.
    struct perf_event_status perf_events[PERF_EVENT_COUNT];
    int group_fd;

    if (opt_perf_enable) {
        perf_events[0].cfg = &PerfEventConfigs[opt_perf1];
        perf_events[1].cfg = &PerfEventConfigs[opt_perf2];
        perf_events[2].cfg = &PerfEventConfigs[opt_perf3];
        perf_events[3].cfg = &PerfEventConfigs[opt_perf4];

        group_fd = setup_perf_events(perf_events, YOUR_CPUS_MAX_PERF_EVENTS, opt_verbose);

        // Reset the performance counters.
        perf_reset(group_fd);
    }

    // We want to see those cold misses on the matrices. Try to flush all 3 levels of cache.
    flush_dcache((1 << 24));

    // Initialize the rusage values.
    struct rusage *initial_rusage;
    initial_rusage = rusage_start();

    // Enable performance counters.
    if (opt_perf_enable) {
        perf_enable(group_fd);
    }

    ///////////////////////////
    // Matrix multiplication //
    ///////////////////////////

    // Get rusage for *only* the multiplication
    (*MatMulFuncs[algorithm_number].func)(A, B, C);

    if (opt_perf_enable) {
        perf_disable(group_fd);
    }

    int64_t usecs = rusage_get_elapsed_usecs(initial_rusage);

    printf_gflops(usecs, algorithm_number, opt_M, opt_N, opt_P);

    if (opt_perf_enable) {
        print_perf_results(group_fd, perf_events, YOUR_CPUS_MAX_PERF_EVENTS);
    }

    if (opt_check) {
        struct matrix *ref = matrix_new(opt_M, opt_N);
        (*MatMulFuncs[1].func)(A, B, ref); // check against ikj (faster)

        for (int i = 0; i < opt_M; i++) {
            for (int j = 0; j< opt_N; j++) {
                if (ref->data[i][j] != C->data[i][j]) {
                    // printf("%d,%d mismatch: %.2f != %.2f\n", i, j, ref->data[i][j], C->data[i][j]);
                    printf("matrix mismatch!\n");
                    exit(1);
                }
            }
        }
        matrix_free(ref);
    }

    // Free the malloc'd memory for the matrices.
    matrix_free(A);
    matrix_free(B);
    matrix_free(C);

    // Free other malloc'd memory
    free(initial_rusage);

    return 0;
}

void
help(char *argv[])
{
    printf("Usage: %s [OPTIONS]\n\n"
           "  -a, --algorithm <matmul algorithm>           Select the MMM algorithm\n"
           "  -1, --perf1 <perf event> (up to 4 events)    Performance event to count\n"
           "  -M, --a_rows <rows in matrices A and C>      Matrix dimension M\n"
           "  -N, --b_cols <cols in matrices B and C>      Matrix dimension N\n"
           "  -P, --a_cols <cols in A / rows in B>         Matrix dimension P\n"
           "  -b, --base_size <base case size>             Size of base case for cache oblivious\n"
           "  -s, --seed <random seed>                     Seed for randomizing matrices\n"
           "  -k, --block1 <L1 block size>                 Size of block for L1 tiling\n"
           "  -l, --block2 <L2 block size>                 Size of block for L2 tiling\n"
           "  -n, --no-perf                                Disable perf_event_open()\n"
           "  -c, --check                                  Check result against ijk_normal\n"
           "  -h, --help                                   Print help and exit\n"
           "  -v, --verbose                                Print values of options, etc.\n",
           argv[0]);

    printf("\nAvailable matmul algorithms: ");
    for (size_t i = 0; i < NUM_MATMUL_FUNCS; i++) {
        if (i != NUM_MATMUL_FUNCS - 1) {
            printf("%s, ", MatMulFuncs[i].name);
        } else {
            printf("%s\n", MatMulFuncs[i].name);
        }
    }

    printf("\nAvailable perf events: ");
    for (size_t i = 0; i < NUM_PERF_EVENTS; i++) {
        if (i != NUM_PERF_EVENTS - 1) {
            printf("%s, ", PerfEventConfigs[i].name);
        } else {
            printf("%s\n", PerfEventConfigs[i].name);
        }
    }
}
