#ifndef __MATRIX_H__
#define __MATRIX_H__

#define RANDOM_SEED    2112
#define MAX_INIT_VALUE 100

struct matrix {
    float **data;
    int n_rows;
    int n_cols;
};

struct matrix *matrix_new(int n_rows, int n_cols);

void matrix_free(struct matrix *mat);

void matrix_randomize(struct matrix *mat);

void matrix_clear(struct matrix *mat);

struct matrix * matrix_transpose(struct matrix *mat);

#endif // __MATRIX_H__
