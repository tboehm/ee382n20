#include "matrix.h"

#include <stdio.h> // perror
#include <stdlib.h> // malloc, calloc, rand

struct matrix *
matrix_new(int n_rows, int n_cols)
{
    struct matrix *matrix = (struct matrix *)malloc(sizeof(struct matrix));
    if (!matrix) {
        perror("malloc");
        return NULL;
    }

    matrix->n_rows = n_rows;
    matrix->n_cols = n_cols;
    matrix->data = (float **)malloc(sizeof(float *) * n_rows);
    if (!matrix->data) {
        perror("malloc");
        return NULL;
    }

    for (int row = 0; row < n_rows; row++) {
        // Zero-out the contents
        matrix->data[row] = (float *)calloc(n_cols, sizeof(float));
        if (!matrix->data[row]) {
            perror("calloc");
            return NULL;
        }
    }
    return matrix;
}

void
matrix_free(struct matrix *matrix)
{
    for (int row = 0; row < matrix->n_rows; row++) {
        free(matrix->data[row]);
    }
    free(matrix->data);
    free(matrix);
}

void
matrix_randomize(struct matrix *matrix)
{
    for (int row = 0; row < matrix->n_rows; row++) {
        for (int col = 0; col < matrix->n_cols; col++) {
            matrix->data[row][col] = rand() % MAX_INIT_VALUE;
        }
    }
}

void
matrix_clear(struct matrix *matrix)
{
    for (int row = 0; row < matrix->n_rows; row++) {
        for (int col = 0; col < matrix->n_cols; col++) {
            matrix->data[row][col] = 0;
        }
    }
}

struct matrix *
matrix_transpose(struct matrix *B)
{
    // Create new matrix
    struct matrix *T = matrix_new(B->n_cols, B->n_rows);

    // Copy Transpose into new matrix
    for(int i = 0; i< B->n_rows; i++){
        for(int j = 0; j< B->n_cols; j++){
            T->data[j][i] = B->data[i][j];
        }
    }

    return T;
}
