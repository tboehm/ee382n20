#define _GNU_SOURCE

#include "perf.h"

#include <asm/unistd.h> // Syscall numbers
#include <inttypes.h> // format specifiers for priting ints
#include <linux/perf_event.h> // perf_event flags and structs
#include <sched.h> // sched_setaffinity()
#include <stdio.h> // printf()
#include <stdint.h> // int64_t
#include <stdlib.h> // malloc(), exit codes
#include <string.h> // memcpy()
#include <sys/ioctl.h> // ioctl()
#include <sys/resource.h> // rusage
#include <sys/syscall.h>
#include <sys/time.h> // rusage, timersub()

#include <unistd.h> // syscall()

int
perf_event_number(char *event)
{
    for (size_t i = 0;
         i < sizeof(PerfEventConfigs) / sizeof(PerfEventConfigs[0]);
         i++) {
        if (strcmp(event, PerfEventConfigs[i].name) == 0) {
            return i;
        }
    }

    return -1;
}

long
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags)
{
    int ret;

    ret = syscall(__NR_perf_event_open, hw_event, pid,
                  cpu, group_fd, flags);
    return ret;
}

void
perf_reset(int fd)
{
    // Reset perf event counters
    if (ioctl(fd, PERF_EVENT_IOC_RESET, PERF_IOC_FLAG_GROUP) == -1) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }
}

void
perf_enable(int fd)
{
    // Enable perf event counters
    if (ioctl(fd, PERF_EVENT_IOC_ENABLE, PERF_IOC_FLAG_GROUP) == -1) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }
}

void
perf_disable(int fd)
{
    // Disable perf event counters
    if (ioctl(fd, PERF_EVENT_IOC_DISABLE, PERF_IOC_FLAG_GROUP) == -1) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }
}

// group_fd: If -1, start a new group. Otherwise, use the value.
// cfg: Configuration for the event.
void
perf_event_new(int group_fd, struct perf_event_status *event, bool verbose)
{
    // This process; any CPU; no flags at the end of the syscall
    pid_t pid = 0;
    int cpu = -1;
    unsigned long flags = 0;

    // Zero-out the attributes to pass to the syscall and set the size
    struct perf_event_attr attrs;
    memset(&attrs, 0, sizeof(attrs));
    attrs.size = sizeof(struct perf_event_attr);

    // Likely "PERF_TYPE_HARDWARE" or "PERF_TYPE_HW_CACHE"
    attrs.type = event->cfg->type;

    // Specific OR of config values for this cache event
    attrs.config = event->cfg->config;

    // Exclude events in kernel- and hypervisor-space
    attrs.exclude_kernel = 1;
    attrs.exclude_hv = 1;

    // 64-bit unique value to work with multiple counters
    attrs.read_format = PERF_FORMAT_ID | PERF_FORMAT_GROUP;

    // Start out disabled
    attrs.disabled = 1;

    int fd = perf_event_open(&attrs, pid, cpu, group_fd, flags);
    if (fd == -1) {
        perror("perf_event_open");
        exit(EXIT_FAILURE);
    }
    event->fd = fd;

    int id;
    if (ioctl(fd, PERF_EVENT_IOC_ID, &id) == -1) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }
    event->event_id = id;

    if (verbose) {
        printf("New perf event: '%s'. id %d, fd %d, config 0x%" PRIx64 "\n",
               event->cfg->name, id, fd, event->cfg->config);
    }
}

int
setup_perf_events(struct perf_event_status *perf_events, int num_events, bool verbose)
{
    int group_fd = -1;
    for (int i = 0; i < num_events; i++) {
        perf_event_new(group_fd, &perf_events[i], verbose);
        if (i == 0) {
            group_fd = perf_events[i].fd;
        }
    }
    return group_fd;
}

void
print_perf_results(int group_fd, struct perf_event_status *perf_events, int num_events)
{
    // Read the group results
    char read_buf[4096];
    struct read_format *rf = (struct read_format *)&read_buf;
    if (read(group_fd, read_buf, sizeof(read_buf)) == -1) {
        perror("read");
        exit(EXIT_FAILURE);
    }

    // Output the results
    for (unsigned i = 0; i < rf->nr; i++) {
        uint64_t id = rf->values[i].id;
        uint64_t value = rf->values[i].value;
        for (int event_idx = 0; event_idx < num_events; event_idx++) {
            struct perf_event_status *event = &perf_events[event_idx];
            if (event->event_id == id) {
                printf("%s = %" PRIu64 "\n", event->cfg->name, value);
            }
        }
    }
}


void
flush_dcache(int size_bytes)
{
    int i;
    char x;
    char *buf = (char *)malloc(size_bytes * sizeof(char));
    for (i = 0; i < size_bytes; i++) {
        x = buf[i];
        buf[i] = x + 1;
    }
    free(buf);
}

void
lock_cpu(int cpu_idx)
{
    cpu_set_t *cpuset;
    size_t size;

    cpuset = CPU_ALLOC(1);
    if (cpuset == NULL) {
        perror("CPU_ALLOC");
        exit(EXIT_FAILURE);
    }
    size = CPU_ALLOC_SIZE(1);
    CPU_ZERO_S(size, cpuset);
    CPU_SET_S(cpu_idx, size, cpuset);

    if (sched_setaffinity(0, size, cpuset) != 0) {
        perror("sched_setaffinity");
        exit(EXIT_FAILURE);
    }

    CPU_FREE(cpuset);
}

struct rusage *
rusage_start(void)
{
    struct rusage *usage = (struct rusage *)malloc(sizeof(struct rusage));;

    if (getrusage(RUSAGE_SELF, usage) != 0) {
        perror("getrusage");
        exit(1);
    }

    return usage;
}

int64_t
rusage_get_elapsed_usecs(struct rusage *start)
{
    struct rusage current;
    struct timeval res;
    int64_t elapsed;

    if (getrusage(RUSAGE_SELF, &current) != 0) {
        perror("getrusage");
        exit(1);
    }

    timersub(&current.ru_utime, &start->ru_utime, &res);
    printf("User CPU time used = %ld.%06ld seconds\n", res.tv_sec, res.tv_usec);

    elapsed = res.tv_sec * 1e6;
    elapsed += res.tv_usec;

    timersub(&current.ru_stime, &start->ru_stime, &res);
    printf("System CPU time used = %ld.%06ld seconds\n", res.tv_sec, res.tv_usec);


    return elapsed;
}

void
printf_gflops(int64_t usecs, int algorithm_number __attribute__((unused)), int M, int N, int P)
{
    uint64_t num_ops;
    num_ops = 2 * M;
    num_ops *= N;
    num_ops *= P;

    double seconds = usecs / 1e6;
    printf("seconds = %g\n", seconds);

    // Calculate Performance
    double flops = num_ops / seconds;
    double gflops = num_ops / (usecs * 1e3);
    printf("FLOPS = %g\n", flops);
    printf("GFLOPS = %g\n", gflops);
}
