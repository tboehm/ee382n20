#ifndef __PERF_H__
#define __PERF_H__

#include <inttypes.h> // format specifiers for priting ints
#include <linux/perf_event.h> // perf_event flags and structs
#include <stdbool.h> // true/false
#include <stdint.h> // int64_t
#include <sys/time.h> // rusage
#include <sys/types.h> // pid_t

// Read format for perf events
struct read_format {
    uint64_t nr;
    struct {
        uint64_t value;
        uint64_t id;
    } values[];
};

// Perf event configuration
struct perf_event_config {
    uint64_t config;
    uint32_t type;
    char name[32];
};

// Perf event status
struct perf_event_status {
    const struct perf_event_config *cfg;
    uint64_t event_id;
    int fd;
};

static struct perf_event_config PerfEventConfigs[] = {
    // Cycles, not affected by dynamic frequency scaling
    { .name = "ref_cycles",
      .type = PERF_TYPE_HARDWARE,
      .config = PERF_COUNT_HW_REF_CPU_CYCLES },

    // Retired instructions (hardware interrupts are counted :/)
    { .name = "instructions",
      .type = PERF_TYPE_HARDWARE,
      .config = PERF_COUNT_HW_INSTRUCTIONS },

    // L1 cache events.
    { .name = "l1_reads",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_L1D)
                 | (PERF_COUNT_HW_CACHE_OP_READ << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)) },
    { .name = "l1_writes",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_L1D)
                 | (PERF_COUNT_HW_CACHE_OP_WRITE << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)) },
    { .name = "l1_prefetches",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_L1D)
                 | (PERF_COUNT_HW_CACHE_OP_PREFETCH << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)) },
    { .name = "l1_read_misses",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_L1D)
                 | (PERF_COUNT_HW_CACHE_OP_READ << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_MISS << 16)) },

    // Stampede2 SKX nodes don't have this event.
    // { .name = "l1_write_misses",
    //   .config = ((PERF_COUNT_HW_CACHE_L1D)
    //              | (PERF_COUNT_HW_CACHE_OP_WRITE << 8)
    //              | (PERF_COUNT_HW_CACHE_RESULT_MISS << 16)) },

    // Last-level cache events. We don't have any "Hardware cache events" for L2,
    // but there are few we can observe using the 'perf' command.
    { .name = "llc_reads",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_LL)
                 | (PERF_COUNT_HW_CACHE_OP_READ << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)) },
    { .name = "llc_writes",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_LL)
                 | (PERF_COUNT_HW_CACHE_OP_WRITE << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)) },
    { .name = "llc_read_misses",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_LL)
                 | (PERF_COUNT_HW_CACHE_OP_READ << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_MISS << 16)) },
    { .name = "llc_write_misses",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_LL)
                 | (PERF_COUNT_HW_CACHE_OP_WRITE << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_MISS << 16)) },

    // Data TLB stuff. Probably not needed.
    { .name = "dtlb_reads",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_DTLB)
                 | (PERF_COUNT_HW_CACHE_OP_READ << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)) },
    { .name = "dtlb_writes",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_DTLB)
                 | (PERF_COUNT_HW_CACHE_OP_WRITE << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)) },
    { .name = "dtlb_read_misses",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_DTLB)
                 | (PERF_COUNT_HW_CACHE_OP_READ << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_MISS << 16)) },
    { .name = "dtlb_write_misses",
      .type = PERF_TYPE_HW_CACHE,
      .config = ((PERF_COUNT_HW_CACHE_DTLB)
                 | (PERF_COUNT_HW_CACHE_OP_WRITE << 8)
                 | (PERF_COUNT_HW_CACHE_RESULT_MISS << 16)) },
};

#define NUM_PERF_EVENTS (sizeof(PerfEventConfigs) / sizeof(PerfEventConfigs[0]))

long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                     int cpu, int group_fd, unsigned long flags);
int perf_event_number(char *event);
void perf_reset(int fd);
void perf_enable(int fd);
void perf_disable(int fd);
void perf_event_new(int group_fd, struct perf_event_status *event, bool verbose);
int setup_perf_events(struct perf_event_status *perf_events, int num_events, bool verbose);
void print_perf_results(int group_fd, struct perf_event_status *perf_events, int num_events);
void flush_dcache(int size_bytes);
void lock_cpu(int cpu_idx);
struct rusage *rusage_start(void);
int64_t rusage_get_elapsed_usecs(struct rusage *start);
long simplerand(void);
void printf_gflops(int64_t usecs, int algorithm_number, int M, int N, int P);

#endif // __PERF_H__
