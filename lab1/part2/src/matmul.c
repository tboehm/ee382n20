#include "matmul.h"
#include "matrix.h"

#include <math.h> // sqrt
#include <stdio.h> // printf (debugging)
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // sysconf and macros

extern int L1BlockSize;
extern int L2BlockSize;

void
matmul_ijk(struct matrix *A, struct matrix *B, struct matrix *C)
{
    for (int i = 0; i < A->n_rows; i++) {
        for (int j = 0; j < B->n_cols; j++) {
            for (int k = 0; k < A->n_cols; k++) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
            }
        }
    }
}

void
matmul_ikj(struct matrix *A, struct matrix *B, struct matrix *C)
{
    for (int i = 0; i < A->n_rows; i++) {
        for (int k = 0; k < A->n_cols; k++) {
            for (int j = 0; j < B->n_cols; j++) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
            }
        }
    }
}

void
matmul_jik(struct matrix *A, struct matrix *B, struct matrix *C)
{
    for (int j = 0; j < B->n_cols; j++) {
        for (int i = 0; i < A->n_rows; i++) {
            for (int k = 0; k < A->n_cols; k++) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
            }
        }
    }
}

void
matmul_jki(struct matrix *A, struct matrix *B, struct matrix *C)
{
    for (int j = 0; j < B->n_cols; j++) {
        for (int k = 0; k < A->n_cols; k++) {
            for (int i = 0; i < A->n_rows; i++) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
            }
        }
    }
}

void
matmul_kij(struct matrix *A, struct matrix *B, struct matrix *C)
{
    for (int k = 0; k < A->n_cols; k++) {
        for (int i = 0; i < A->n_rows; i++) {
            for (int j = 0; j < B->n_cols; j++) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
            }
        }
    }
}

void
matmul_kji(struct matrix *A, struct matrix *B, struct matrix *C)
{
    for (int k = 0; k < A->n_cols; k++) {
        for (int j = 0; j < B->n_cols; j++) {
            for (int i = 0; i < A->n_rows; i++) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
            }
        }
    }
}

void
matmul_ijk_l1_tiled(struct matrix *A, struct matrix *B, struct matrix *C)
{
    int BLOCK_SIZE = 128 < A->n_rows ? 128 : A->n_rows;

    for (int ii = 0; ii < A->n_rows; ii += BLOCK_SIZE) {
        for (int jj = 0; jj < A->n_cols; jj += BLOCK_SIZE) {
            for (int kk = 0; kk < B->n_rows; kk += BLOCK_SIZE) {
                for (int i = ii; i < ii + BLOCK_SIZE; i++) {
                    for (int j = jj; j < jj + BLOCK_SIZE; j++) {
                        for (int k = kk; k < kk + BLOCK_SIZE; k++) {
                            C->data[i][j] += A->data[i][k] * B->data[k][j];
                        }
                    }
                }
            }
        }
    }
}


void
matmul_ijk_l2_tiled(struct matrix *A, struct matrix *B, struct matrix *C)
{
    int L1BS = L1BlockSize < A->n_rows ? L1BlockSize : A->n_rows;
    int L2BS = L2BlockSize < A->n_rows ? L2BlockSize : A->n_rows;

    for (int ii2 = 0; ii2 < A->n_rows; ii2 += L2BS) {
    for (int kk2 = 0; kk2 < A->n_cols; kk2 += L2BS) {
    for (int jj2 = 0; jj2 < B->n_rows; jj2 += L2BS) {
        for (int ii1 = ii2; ii1 < ii2 + L2BS; ii1 += L1BS) {
        for (int kk1 = kk2; kk1 < kk2 + L2BS; kk1 += L1BS) {
        for (int jj1 = jj2; jj1 < jj2 + L2BS; jj1 += L1BS) {
            for (int i = ii1; i < ii1 + L1BS; i += 1) {
            for (int k = kk1; k < kk1 + L1BS; k += 1) {
            for (int j = jj1; j < jj1 + L1BS; j += 1) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
            }}}
        }}}
    }}}
}

void
matmul_ikj_reg_tiled(struct matrix *A, struct matrix *B, struct matrix *C)
{
    for (int i = 0; i < A->n_rows; i += 2) {
        for (int k = 0; k < A->n_cols; k += 1) {
            for (int j = 0; j < B->n_rows; j += 2) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
                C->data[i + 1][j] += A->data[i + 1][k] * B->data[k][j];
                C->data[i][j + 1] += A->data[i][k] * B->data[k][j + 1];
                C->data[i + 1][j + 1] += A->data[i + 1][k] * B->data[k][j + 1];
            }
        }
    }
}

void
matmul_ikj_l1_tiled(struct matrix *A, struct matrix *B, struct matrix *C)
{
    int L1BS = L1BlockSize < A->n_rows ? L1BlockSize : A->n_rows;

    printf("matmul_ikj_l1_tiled: bs = %d\n", L1BS);

    for (int ii = 0; ii < A->n_rows; ii += L1BS) {
    for (int kk = 0; kk < A->n_cols; kk += L1BS) {
    for (int jj = 0; jj < B->n_rows; jj += L1BS) {
        for (int i = ii; i < ii + L1BS; i += 2) {
        for (int k = kk; k < kk + L1BS; k += 1) {
        for (int j = jj; j < jj + L1BS; j += 2) {
            C->data[i][j] += A->data[i][k] * B->data[k][j];
            C->data[i+1][j] += A->data[i+1][k] * B->data[k][j];
            C->data[i][j+1] += A->data[i][k] * B->data[k][j+1];
            C->data[i+1][j+1] += A->data[i+1][k] * B->data[k][j+1];
        }}}
    }}}
}

void
matmul_ikj_l2_tiled(struct matrix *A, struct matrix *B, struct matrix *C)
{
    int L1BS = L1BlockSize < A->n_rows ? L1BlockSize : A->n_rows;
    int L2BS = L2BlockSize < A->n_rows ? L2BlockSize : A->n_rows;

    for (int ii2 = 0; ii2 < A->n_rows; ii2 += L2BS) {
    for (int kk2 = 0; kk2 < A->n_cols; kk2 += L2BS) {
    for (int jj2 = 0; jj2 < B->n_rows; jj2 += L2BS) {
        for (int ii1 = ii2; ii1 < ii2 + L2BS; ii1 += L1BS) {
        for (int kk1 = kk2; kk1 < kk2 + L2BS; kk1 += L1BS) {
        for (int jj1 = jj2; jj1 < jj2 + L2BS; jj1 += L1BS) {
            for (int i = ii1; i < ii1 + L1BS; i += 2) {
            for (int k = kk1; k < kk1 + L1BS; k += 1) {
            for (int j = jj1; j < jj1 + L1BS; j += 2) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
                C->data[i+1][j] += A->data[i+1][k] * B->data[k][j];
                C->data[i][j+1] += A->data[i][k] * B->data[k][j+1];
                C->data[i+1][j+1] += A->data[i+1][k] * B->data[k][j+1];
            }}}
        }}}
    }}}
}

void
matmul_ikj_l3_tiled(struct matrix *A, struct matrix *B, struct matrix *C)
{
    int L1BS = 128 < A->n_rows ? 128 : A->n_rows;
    int L2BS = 512 < A->n_rows ? 512 : A->n_rows;
    int L3BS = 4096 < A->n_rows ? 4096 : A->n_rows;

    for (int ii3 = 0; ii3 < A->n_rows; ii3 += L3BS) {
    for (int kk3 = 0; kk3 < A->n_cols; kk3 += L3BS) {
    for (int jj3 = 0; jj3 < B->n_rows; jj3 += L3BS) {
        for (int ii2 = ii3; ii2 < ii3 + L3BS; ii2 += L2BS) {
        for (int kk2 = kk3; kk2 < kk3 + L3BS; kk2 += L2BS) {
        for (int jj2 = jj3; jj2 < jj3 + L3BS; jj2 += L2BS) {
            for (int ii1 = ii2; ii1 < ii2 + L2BS; ii1 += L1BS) {
            for (int kk1 = kk2; kk1 < kk2 + L2BS; kk1 += L1BS) {
            for (int jj1 = jj2; jj1 < jj2 + L2BS; jj1 += L1BS) {
                for (int i = ii1; i < ii1 + L1BS; i += 2) {
                for (int k = kk1; k < kk1 + L1BS; k += 1) {
                for (int j = jj1; j < jj1 + L1BS; j += 2) {
                    C->data[i][j] += A->data[i][k] * B->data[k][j];
                    C->data[i+1][j] += A->data[i+1][k] * B->data[k][j];
                    C->data[i][j+1] += A->data[i][k] * B->data[k][j+1];
                    C->data[i+1][j+1] += A->data[i+1][k] * B->data[k][j+1];
                }}}
            }}}
        }}}
    }}}
}

static inline void
matmul_hierarchical_inner(struct matrix *A, struct matrix *B, struct matrix *C,
                          int ii, int jj, int kk, int prev_bs)
{
    for (int i = ii; i < ii + prev_bs; i += 2) {
        for (int k = kk; k < kk + prev_bs; k++) {
            for (int j = jj; j < jj + prev_bs; j += 2) {
                C->data[i][j] += A->data[i][k] * B->data[k][j];
                C->data[i+1][j] += A->data[i+1][k] * B->data[k][j];
                C->data[i][j+1] += A->data[i][k] * B->data[k][j+1];
                C->data[i+1][j+1] += A->data[i+1][k] * B->data[k][j+1];
            }
        }
    }
}

static void
matmul_hierarchical_level(struct matrix *A, struct matrix *B, struct matrix *C,
                          int ii, int jj, int kk, int *block_sizes, int this_level)
{
    int prev_bs = block_sizes[this_level + 1];
    int bs;
    int next_level;

    if (this_level < 0) {
        matmul_hierarchical_inner(A, B, C, ii, jj, kk, prev_bs);
        return;
    }

    bs = block_sizes[this_level];
    next_level = this_level - 1;
    for (int i = ii; i < ii + prev_bs; i += bs) {
        for (int k = kk; k < kk + prev_bs; k += bs) {
            for (int j = jj; j < jj + prev_bs; j += bs) {
                matmul_hierarchical_level(A, B, C, i, j, k, block_sizes, next_level);
            }
        }
    }
}

static const unsigned long ScMacros[] = {
    [0] = 0,
    [1] = _SC_LEVEL1_DCACHE_SIZE,
    [2] = _SC_LEVEL2_CACHE_SIZE,
    [3] = _SC_LEVEL3_CACHE_SIZE,
};

static long
get_cache_size(int level)
{
    unsigned long sc_macro;
    long cache_size;

    if (level < 1 || level > 3) {
        fprintf(stderr, "Unknown level: %d\n", level);
        return -1;
    }

    sc_macro = ScMacros[level];
    cache_size = sysconf(sc_macro);
    if (cache_size < 0) {
        perror("sysconf");
        return -1;
    }

    return cache_size;
}

static inline int
round_down_power_2(int n)
{
    int pow2 = 1;

    while ((n >>= 1)) {
        pow2 <<= 1;
    }

    return pow2;
}

void
matmul_hierarchical(struct matrix *A, struct matrix *B, struct matrix *C)
{
    // FIXME: Don't assume how many levels there are.
    // FIXME: (Maybe) don't assume square matrices -> block sizes aren't equal.
    int num_levels = 3;

    int *block_sizes = (int *)malloc(num_levels * sizeof(int));

    // Get the size of each cache to see what block sizes we need.
    for (int i = 0; i < num_levels; i++) {
        int level = i + 1;
        long cache_size = get_cache_size(level);

        if (cache_size < 0) {
            fprintf(stderr, "Failed to get L%d cache size\n", level);
            exit(EXIT_FAILURE);
        }

        if (cache_size == 0) {
            num_levels = level - 1;
            printf("Number of levels for hierarchical tiling: %d\n", num_levels);
        }

        // int bs = (int)sqrt(cache_size / 3) / sizeof(float);
        int bs = 2 * (int)sqrt(4 + (cache_size / sizeof(float))) - 2;

        if (bs == 0 || bs > A->n_rows) {
            // No sense in blocking for this level of cache or larger.
            num_levels = level - 1;
            printf("Number of levels for hierarchical tiling: %d\n", num_levels);
            break;
        }

        bs = round_down_power_2(bs);

        block_sizes[i] = bs;

        printf("Block size for L%d cache: %d\n", level, bs);
    }

    if (num_levels == 0) {
        matmul_ikj(A, B, C);
        return;
    }

    // Do the multiplication.
    int bs = block_sizes[num_levels - 1];
    for (int ii = 0; ii < A->n_rows; ii += bs) {
        for (int kk = 0; kk < A->n_cols; kk += bs) {
            for (int jj = 0; jj < B->n_rows; jj += bs) {
                // printf("outer: ii = %d, jj = %d, kk = %d\n", ii, jj, kk);
                matmul_hierarchical_level(A, B, C, ii, jj, kk, block_sizes, num_levels - 2);
            }
        }
    }

    free(block_sizes);
}


int BASE_BLOCK_SIZE;
static void
matmul_ijk_oblivious_work(float **A, float **B, float **C, int block_size, 
    int A_start_row, int A_start_col, int B_start_row, int B_start_col) {
    if (block_size > BASE_BLOCK_SIZE) {
        int sb_size = block_size/2;
        matmul_ijk_oblivious_work(A, B, C, sb_size, A_start_row, A_start_col, B_start_row, B_start_col);
        matmul_ijk_oblivious_work(A, B, C, sb_size, A_start_row, A_start_col+sb_size, B_start_row+sb_size, B_start_col);
        matmul_ijk_oblivious_work(A, B, C, sb_size, A_start_row, A_start_col, B_start_row, B_start_col+sb_size);
        matmul_ijk_oblivious_work(A, B, C, sb_size, A_start_row, A_start_col+sb_size, B_start_row+sb_size, B_start_col+sb_size);
        matmul_ijk_oblivious_work(A, B, C, sb_size, A_start_row+sb_size, A_start_col, B_start_row, B_start_col);
        matmul_ijk_oblivious_work(A, B, C, sb_size, A_start_row+sb_size, A_start_col+sb_size, B_start_row+sb_size, B_start_col);
        matmul_ijk_oblivious_work(A, B, C, sb_size, A_start_row+sb_size, A_start_col, B_start_row, B_start_col+sb_size);
        matmul_ijk_oblivious_work(A, B, C, sb_size, A_start_row+sb_size, A_start_col+sb_size, B_start_row+sb_size, B_start_col+sb_size);
    } else {
        // base case
        for (int i = 0; i < block_size; i++) {
            for (int j = 0; j < block_size; j++) {
                for (int k = 0; k < block_size; k++) {
                    C[A_start_row+i][B_start_col+j] += A[A_start_row+i][A_start_col+k] * B[B_start_row+k][B_start_col+j];
                }
            }
        }
    }
}

static void
matmul_ikj_oblivious_work(float **A, float **B, float **C, int block_size, 
    int A_start_row, int A_start_col, int B_start_row, int B_start_col) {
    if (block_size > BASE_BLOCK_SIZE) {
        int sb_size = block_size/2;
        // i = 0, j = 0, k = 0
        matmul_ikj_oblivious_work(A, B, C, sb_size, A_start_row, A_start_col, B_start_row, B_start_col);
        // i = 0, j = 0, k = 1
        matmul_ikj_oblivious_work(A, B, C, sb_size, A_start_row, A_start_col, B_start_row, B_start_col+sb_size);
        // i = 0, j = 1, k = 0
        matmul_ikj_oblivious_work(A, B, C, sb_size, A_start_row, A_start_col+sb_size, B_start_row+sb_size, B_start_col);
        // i = 0, j = 1, k = 1
        matmul_ikj_oblivious_work(A, B, C, sb_size, A_start_row, A_start_col+sb_size, B_start_row+sb_size, B_start_col+sb_size);
        // i = 1, j = 0, k = 0
        matmul_ikj_oblivious_work(A, B, C, sb_size, A_start_row+sb_size, A_start_col, B_start_row, B_start_col);
        // i = 1, j = 0, k = 1
        matmul_ikj_oblivious_work(A, B, C, sb_size, A_start_row+sb_size, A_start_col, B_start_row, B_start_col+sb_size);
        // i = 1, j = 1, k = 0
        matmul_ikj_oblivious_work(A, B, C, sb_size, A_start_row+sb_size, A_start_col+sb_size, B_start_row+sb_size, B_start_col);
        // i = 1, j = 1, k = 1
        matmul_ikj_oblivious_work(A, B, C, sb_size, A_start_row+sb_size, A_start_col+sb_size, B_start_row+sb_size, B_start_col+sb_size);
    } else {
        // base case
        for (int i = 0; i < block_size; i++) {
            for (int k = 0; k < block_size; k++) {
                for (int j = 0; j < block_size; j++) {
                    C[A_start_row+i][B_start_col+j] += A[A_start_row+i][A_start_col+k] * B[B_start_row+k][B_start_col+j];
                }
            }
        }
    }
}

void
matmul_ikj_oblivious(struct matrix *A, struct matrix *B, struct matrix *C)
{
  // assume square matrix of power of 2
  // wrapper around actual recursive call to fit API
  matmul_ikj_oblivious_work(A->data, B->data, C->data, A->n_rows, 0, 0, 0, 0);
}

void
matmul_ijk_oblivious(struct matrix *A, struct matrix *B, struct matrix *C)
{
  // assume square matrix of power of 2
  // wrapper around actual recursive call to fit API
  matmul_ijk_oblivious_work(A->data, B->data, C->data, A->n_rows, 0, 0, 0, 0);
}

int
matmul_algorithm_number(const char *name)
{
    for (size_t i = 0; i < NUM_MATMUL_FUNCS; i++) {
        if (strcmp(name, MatMulFuncs[i].name) == 0) {
            return i;
        }
    }

    return -1;
}

/* MATRIX TRANSPOSE OPTIMIZATIONS */
void
matmul_transpose_ijk(struct matrix *A, struct matrix *B, struct matrix *C)
{
    B = matrix_transpose(B);

    for (int i = 0; i < A->n_rows; i++) {
        for (int j = 0; j < B->n_rows; j++) {
            for (int k = 0; k < A->n_cols; k++) {
                C->data[i][j] += A->data[i][k] * B->data[j][k];
            }
        }
    }

    matrix_free(B);
}

static void
matmul_transpose_ijk_oblivious_work(float **A, float **B, float **C, int block_size, 
    int A_start_row, int A_start_col, int B_start_row, int B_start_col) {
    if (block_size > BASE_BLOCK_SIZE) {
        int sb_size = block_size/2;
        matmul_transpose_ijk_oblivious_work(A, B, C, sb_size, A_start_row,
                A_start_col, B_start_row, B_start_col);
        matmul_transpose_ijk_oblivious_work(A, B, C, sb_size, A_start_row,
                A_start_col+sb_size, B_start_row+sb_size, B_start_col);
        matmul_transpose_ijk_oblivious_work(A, B, C, sb_size, A_start_row,
                A_start_col, B_start_row, B_start_col+sb_size);
        matmul_transpose_ijk_oblivious_work(A, B, C, sb_size, A_start_row,
                A_start_col+sb_size, B_start_row+sb_size, B_start_col+sb_size);
        matmul_transpose_ijk_oblivious_work(A, B, C, sb_size, A_start_row+sb_size,
                A_start_col, B_start_row, B_start_col);
        matmul_transpose_ijk_oblivious_work(A, B, C, sb_size, A_start_row+sb_size,
                A_start_col+sb_size, B_start_row+sb_size, B_start_col);
        matmul_transpose_ijk_oblivious_work(A, B, C, sb_size, A_start_row+sb_size,
                A_start_col, B_start_row, B_start_col+sb_size);
        matmul_transpose_ijk_oblivious_work(A, B, C, sb_size, A_start_row+sb_size,
                A_start_col+sb_size, B_start_row+sb_size, B_start_col+sb_size);
    } else {
        // base case
        for (int i = 0; i < block_size; i++) {
            for (int j = 0; j < block_size; j++) {
                for (int k = 0; k < block_size; k++) {
                    C[A_start_row+i][B_start_col+j] += A[A_start_row+i][A_start_col+k] * B[B_start_row+j][B_start_col+k];
                }
            }
        }
    }
}

void
matmul_transpose_ijk_oblivious(struct matrix *A, struct matrix *B, struct matrix *C)
{
    B = matrix_transpose(B);
    // assume square matrix of power of 2
    // wrapper around actual recursive call to fit API
    matmul_transpose_ijk_oblivious_work(A->data, B->data, C->data, A->n_rows, 0, 0, 0, 0);

    matrix_free(B);
}

void
matmul_transpose_ijk_l1_tiled(struct matrix *A, struct matrix *B, struct matrix *C)
{
    // FIXME: Parameterize
    int BLOCK_SIZE = 128 < A->n_rows ? 128 : A->n_rows;

    for (int i = 0; i < A->n_rows / BLOCK_SIZE; i++) {
        for (int j = 0; j < B->n_rows / BLOCK_SIZE; j++) {
            for (int k = 0; k < A->n_cols / BLOCK_SIZE; k++) {
                for (int ii = 0; ii < BLOCK_SIZE; ii++) {
                    for (int jj = 0; jj < BLOCK_SIZE; jj++) {
                        for (int kk = 0; kk < BLOCK_SIZE; kk++) {
                            C->data[i + ii][j + jj] += A->data[i + ii][k + kk] * B->data[j + jj][k + kk];
                        }
                    }
                }
            }
        }
    }
}
