#!/bin/bash

#-----------------------------------------------------------------------
# (1) Profiling basic ijk matmul
#-----------------------------------------------------------------------

#SBATCH -J matmul-ikj
#SBATCH -o matmul-ikj.out%j  # Name of stdout output file
#SBATCH -e matmul-ikj.err%j  # Name of stderr error file
#SBATCH -N 2
#SBATCH -n 2
#SBATCH -p skx-normal
#SBATCH -t 00:120:00
#SBATCH --mail-user=stevenzhu@utexas.edu
#SBATCH --mail-type=all         # Send email at begin and end of job
#SBATCH -A EE382N-20-Parallelis

#-----------------------------------------------------------------------

module load launcher

# Using SLURM
export LAUNCHER_RMI=SLURM
export LAUNCHER_PLUGIN_DIR=$LAUNCHER_DIR/plugins

export LAUNCHER_WORKDIR=$(realpath .)
export LAUNCHER_JOB_FILE=./sweep_matmul_ikj_oblivious

$LAUNCHER_DIR/paramrun
