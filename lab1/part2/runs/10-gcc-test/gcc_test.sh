#!/bin/bash

#-----------------------------------------------------------------------
# (1) Profiling basic ijk matmul
#-----------------------------------------------------------------------

#SBATCH -J matmul-ijk
#SBATCH -o matmul-ijk.out%j  # Name of stdout output file
#SBATCH -e matmul-ijk.err%j  # Name of stderr error file
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p skx-normal
#SBATCH -t 03:00:00
#SBATCH --mail-user=mebarondeau@utexas.edu
#SBATCH --mail-type=all         # Send email at begin and end of job
#SBATCH -A EE382N-20-Parallelis

#-----------------------------------------------------------------------

module load launcher

# Using SLURM
export LAUNCHER_RMI=SLURM
export LAUNCHER_PLUGIN_DIR=$LAUNCHER_DIR/plugins

export LAUNCHER_WORKDIR=$(realpath .)
export LAUNCHER_JOB_FILE=./gcc_test

$LAUNCHER_DIR/paramrun
