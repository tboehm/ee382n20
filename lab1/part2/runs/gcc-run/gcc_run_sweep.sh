#!/bin/bash

#-----------------------------------------------------------------------
# Profiling all matmul algorithms
#-----------------------------------------------------------------------

#SBATCH -J matmul
#SBATCH -o matmul.out%j
#SBATCH -e matmul.err%j
#SBATCH -N 2
#SBATCH -n 2
#SBATCH -p skx-normal
#SBATCH -t 01:00:00
#SBATCH --mail-user=tboehm@utexas.edu
#SBATCH --mail-type=all
#SBATCH -A EE382N-20-Parallelis

#-----------------------------------------------------------------------

module load launcher

# Using SLURM
export LAUNCHER_RMI=SLURM
export LAUNCHER_PLUGIN_DIR=$LAUNCHER_DIR/plugins

export LAUNCHER_WORKDIR=$(realpath .)
export LAUNCHER_JOB_FILE=./gcc_run_sweep

$LAUNCHER_DIR/paramrun
