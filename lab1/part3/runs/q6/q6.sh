#!/bin/bash

#-----------------------------------------------------------------------
# Profiling all matmul algorithms
#-----------------------------------------------------------------------

#SBATCH -J q6
#SBATCH -o q6.%j
#SBATCH -e q6.%j
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p skx-normal
#SBATCH -t 06:00:00
#SBATCH --mail-user=tboehm@utexas.edu
#SBATCH --mail-type=all
#SBATCH -A EE382N-20-Parallelis

#-----------------------------------------------------------------------

module load launcher

# Using SLURM
export LAUNCHER_RMI=SLURM
export LAUNCHER_PLUGIN_DIR=$LAUNCHER_DIR/plugins

export LAUNCHER_WORKDIR=$(realpath .)
export LAUNCHER_JOB_FILE=./q6

#$LAUNCHER_DIR/paramrun
#../../pin -t ../../dcache.so -c1 32 -c2 1024 -b1 64 -b2 64 -a1 8 -a2 16 -o pin.out.4096_32_1024_64_128_ijk_normal_0 -- ../../../part2/matmul --algorithm ijk_normal -M 4096 -N 4096 -P 4096 --block1 64 --block2 128 --base_size 16
