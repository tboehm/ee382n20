#!/bin/bash

#-----------------------------------------------------------------------
# Profiling all matmul algorithms
#-----------------------------------------------------------------------

#SBATCH -J q7
#SBATCH -o q7.out%j
#SBATCH -e q7.err%j
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p skx-normal
#SBATCH -t 04:00:00
#SBATCH --mail-user=tboehm@utexas.edu
#SBATCH --mail-type=all
#SBATCH -A EE382N-20-Parallelis

#-----------------------------------------------------------------------

module load launcher

# Using SLURM
export LAUNCHER_RMI=SLURM
export LAUNCHER_PLUGIN_DIR=$LAUNCHER_DIR/plugins

export LAUNCHER_WORKDIR=$(realpath .)
export LAUNCHER_JOB_FILE=q7

$LAUNCHER_DIR/paramrun
