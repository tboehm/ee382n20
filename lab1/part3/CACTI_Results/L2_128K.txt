Cache size                    : 131072
Block size                    : 64
Associativity                 : 16
Read only ports               : 0
Write only ports              : 0
Read write ports              : 1
Single ended read ports       : 0
Cache banks (UCA)             : 1
Technology                    : 0.045
Temperature                   : 350
Tag size                      : 42
cache type                    : Cache
Model as memory               : 0
Access mode                   : 0
Data array cell type          : 0
Data array peripheral type    : 0
Tag array cell type           : 0
Tag array peripheral type     : 0
Design objective (UCA wt)     : 0 0 0 100 0
Design objective (UCA dev)    : 20 100000 100000 100000 100000
Design objective (NUCA wt)    : 100 100 0 0 100
Design objective (NUCA dev)   : 10 10000 10000 10000 10000
Cache model                   : 0
Nuca bank                     : 0
Wire inside mat               : 1
Wire outside mat              : 1
Interconnect projection       : 1
Wire signalling               : 0
Cores                         : 8
Print level                   : 1
ECC overhead                  : 1
Page size                     : 8192
Burst length                  : 8
Internal prefetch width       : 8
Force cache config            : 0

---------- CACTI version 6.5, Uniform Cache Access SRAM Model ----------

Cache Parameters:
    Total cache size (bytes): 131072
    Number of banks: 1
    Associativity: 16
    Block size (bytes): 64
    Read/write Ports: 1
    Read ports: 0
    Write ports: 0
    Technology size (nm): 45

    Access time (ns): 3.55303
    Cycle time (ns):  23.8085
    Total dynamic read energy per access (nJ): 0.112032
    Total leakage power of a bank (mW): 144.433
    Cache height x width (mm): 0.199317 x 4.50973

    Best Ndwl : 2
    Best Ndbl : 1
    Best Nspd : 1
    Best Ndcm : 1
    Best Ndsam L1 : 16
    Best Ndsam L2 : 1

    Best Ntwl : 2
    Best Ntbl : 2
    Best Ntspd : 0.5
    Best Ntcm : 1
    Best Ntsam L1 : 1
    Best Ntsam L2 : 1
    Data array, H-tree wire type: Delay optimized global wires
    Tag array, wire type: Low swing wires

Time Components:

  Data side (with Output driver) (ns): 3.55303
	H-tree input delay (ns): 0
	Decoder + wordline delay (ns): 2.81089
	Bitline delay (ns): 0.67942
	Sense Amplifier delay (ns): 0.00336627
	H-tree output delay (ns): 0.0593605

  Tag side (with Output driver) (ns): 0.299555
	H-tree input delay (ns): 0
	Decoder + wordline delay (ns): 0.181662
	Bitline delay (ns): 0.067863
	Sense Amplifier delay (ns): 0.00336627
	Comparator delay (ns): 0.0536971
	H-tree output delay (ns): 0.0466634


Power Components:

  Data array: Total dynamic read energy/access  (nJ): 0.10419
	Total leakage read/write power of a bank (mW): 127.869
	Total energy in H-tree (that includes both address and data transfer) (nJ): 0
	Output Htree Energy (nJ): 0
	Decoder (nJ): 0.000180453
	Wordline (nJ): 0.00194757
	Bitline mux & associated drivers (nJ): 0
	Sense amp mux & associated drivers (nJ): 0
	Bitlines (nJ): 0.0409534
	Sense amplifier energy (nJ): 0.0153729
	Sub-array output driver (nJ): 0.0267865

  Tag array:  Total dynamic read energy/access (nJ): 0.00784179
	Total leakage read/write power of a bank (mW): 16.5632
	Total energy in H-tree (that includes both address and data transfer) (nJ): 0
	Output Htree Energy (nJ): 0
	Decoder (nJ): 0.000197353
	Wordline (nJ): 0.000222635
	Bitline mux & associated drivers (nJ): 0
	Sense amp mux & associated drivers (nJ): 0
	Bitlines (nJ): 0.00287954
	Sense amplifier energy (nJ): 0.00108091
	Sub-array output driver (nJ): 0.000786619


Area Components:

  Data array: Area (mm2): 0.865341
	Height (mm): 0.199317
	Width (mm): 4.34153
	Area efficiency (Memory cell area/Total area) - 35.8254 %
		MAT Height (mm): 0.199317
		MAT Length (mm): 4.34153
		Subarray Height (mm): 0.084096
		Subarray Length (mm): 2.16

  Tag array: Area (mm2): 0.0322651
	Height (mm): 0.191819
	Width (mm): 0.168206
	Area efficiency (Memory cell area/Total area) - 67.558 %
		MAT Height (mm): 0.191819
		MAT Length (mm): 0.168206
		Subarray Height (mm): 0.084096
		Subarray Length (mm): 0.0759375

Wire Properties:

  Delay Optimal
	Repeater size - 79.3886 
	Repeater spacing - 0.22182 (mm) 
	Delay - 0.075918 (ns/mm) 
	PowerD - 0.000405587 (nJ/mm) 
	PowerL - 0.0234368 (mW/mm)
	Wire width - 0.09 microns
	Wire spacing - 0.09 microns

  5% Overhead
	Repeater size - 50.3886 
	Repeater spacing - 0.32182 (mm) 
	Delay - 0.0795597 (ns/mm) 
	PowerD - 0.000284677 (nJ/mm) 
	PowerL - 0.0102532 (mW/mm)
	Wire width - 0.09 microns
	Wire spacing - 0.09 microns

  10% Overhead
	Repeater size - 40.3886 
	Repeater spacing - 0.32182 (mm) 
	Delay - 0.0831154 (ns/mm) 
	PowerD - 0.000263647 (nJ/mm) 
	PowerL - 0.00821838 (mW/mm)
	Wire width - 0.09 microns
	Wire spacing - 0.09 microns

  20% Overhead
	Repeater size - 36.3886 
	Repeater spacing - 0.42182 (mm) 
	Delay - 0.0907427 (ns/mm) 
	PowerD - 0.000242437 (nJ/mm) 
	PowerL - 0.00564909 (mW/mm)
	Wire width - 0.09 microns
	Wire spacing - 0.09 microns

  30% Overhead
	Repeater size - 28.3886 
	Repeater spacing - 0.42182 (mm) 
	Delay - 0.098604 (ns/mm) 
	PowerD - 0.000228872 (nJ/mm) 
	PowerL - 0.00440714 (mW/mm)
	Wire width - 0.09 microns
	Wire spacing - 0.09 microns

  Low-swing wire (1 mm) - Note: Unlike repeated wires, 
	delay and power values of low-swing wires do not
	have a linear relationship with length. 
	delay - 0.481104 (ns) 
	powerD - 3.99344e-05 (nJ) 
	PowerL - 5.32702e-06 (mW)
	Wire width - 1.8e-07 microns
	Wire spacing - 1.8e-07 microns


