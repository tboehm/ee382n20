#!/usr/bin/env python3

import sys

TRIALS = 20

PIN = "../../../../pin"
DCACHE_SO = "../../../../dcache.so"
MATMUL = "../../../part2/matmul"


def make_pin_run(M, Z1, Z2, B1, B2, alg, trial):
    # Please forgive these long lines.
    pin_cmd = f"{PIN} -t {DCACHE_SO} -c1 {Z1} -c2 {Z2} -b1 64 -b2 64 -a1 8 -a2 16"
    pin_cmd += f" -o pin.out.{M}_{Z1}_{Z2}_{B1}_{B2}_{alg}_{trial}"

    matmul_cmd = f"{MATMUL} --algorithm {alg} -M {M} -N {M} -P {M} --block1 {B1} --block2 {B2} --base_size 16 --no-perf"

    return pin_cmd + ' -- ' + matmul_cmd


def make_perf_run(M, B1, B2, alg, trial):
    perf_cmd = "perf stat -e L1-dcache-loads -e L1-dcache-stores -e l2_rqsts.references -e LLC-loads -e LLC-stores -e LLC-load-misses -e LLC-store-misses"

    matmul_cmd = f"{MATMUL} --algorithm {alg} -M {M} -N {M} -P {M} --block1 {B1} --block2 {B2} --base_size 16 --no-perf"

    redir = f" > perf.out.{M}_{B1}_{B2}_{alg}_{trial}"

    return perf_cmd + ' ' + matmul_cmd + redir


# Note: We're using the ijk algortihms rather than more optimal ikj in order to be consistent
# with what the lab doc requests.
q6_runs = [
    # M = 4096
    {"M": 4096, "Z1": 32, "Z2": 1024, "B1": 64, "B2": 128, "alg": "ijk_normal"},
    {"M": 4096, "Z1": 32, "Z2": 1024, "B1": 64, "B2": 128, "alg": "ijk_l2_tiled"},
    {"M": 4096, "Z1": 32, "Z2": 1024, "B1": 64, "B2": 128, "alg": "ijk_oblivious"},
    # M :  512, larger caches
    {"M": 512, "Z1": 8, "Z2": 256, "B1": 32, "B2": 64, "alg": "ijk_normal"},
    {"M": 512, "Z1": 8, "Z2": 256, "B1": 32, "B2": 64, "alg": "ijk_l2_tiled"},
    {"M": 512, "Z1": 8, "Z2": 256, "B1": 32, "B2": 64, "alg": "ijk_oblivious"},
    # M :  512, smaller caches
    {"M": 512, "Z1": 1, "Z2": 128, "B1": 16, "B2": 64, "alg": "ijk_normal"},
    {"M": 512, "Z1": 1, "Z2": 128, "B1": 16, "B2": 64, "alg": "ijk_l2_tiled"},
    {"M": 512, "Z1": 1, "Z2": 128, "B1": 16, "B2": 64, "alg": "ijk_oblivious"},
]

# Z1 = 32768 -> BS1 = 26.13, Z2 = 1048576 -> BS2 = 147.80
# Z1 =  8192 -> BS1 = 13.06, Z2 =  262144 -> BS2 =  73.90
# Z1 =  1024 -> BS1 =  4.62, Z2 =  131072 -> BS2 =  52.26

opt_b1_4096 = 16
opt_b2_4096 = 128
opt_b1_512_big = 8
opt_b2_512_big = 64
opt_b1_512_small = 4
opt_b2_512_small = 32

q7_runs = [
    # M = 4096
    {"M": 4096, "Z1": 32, "Z2": 1024, "B1": 128, "B2": 512, "alg": "ijk_l2_tiled"},
    {"M": 4096, "Z1": 32, "Z2": 1024, "B1": opt_b1_4096, "B2": opt_b2_4096, "alg": "ijk_l2_tiled"},
    # M :  512, larger caches
    #{"M": 512, "Z1": 8, "Z2": 256, "B1": 128, "B2": 512, "alg": "ijk_l2_tiled"},
    #{"M": 512, "Z1": 8, "Z2": 256, "B1": opt_b1_512_big, "B2": opt_b2_512_big, "alg": "ijk_l2_tiled"},
    # M :  512, smaller caches
    #{"M": 512, "Z1": 1, "Z2": 128, "B1": 128, "B2": 512, "alg": "ijk_l2_tiled"},
    #{"M": 512, "Z1": 1, "Z2": 128, "B1": opt_b1_512_small, "B2": opt_b2_512_small, "alg": "ijk_l2_tiled"},
]

q8_runs = [
    # M = 4096
    {"M": 4096, "B1": 128, "B2": 512, "alg": "ijk_normal"},
    {"M": 4096, "B1": 128, "B2": 512, "alg": "ijk_l2_tiled"},
    {"M": 4096, "B1": 128, "B2": 512, "alg": "ijk_oblivious"},
    {"M": 4096, "B1": opt_b1_4096, "B2": opt_b2_4096, "alg": "ijk_l2_tiled"},
    # M :  512
    {"M": 512, "B1": 128, "B2": 512, "alg": "ijk_normal"},
    {"M": 512, "B1": 128, "B2": 512, "alg": "ijk_l2_tiled"},
    {"M": 512, "B1": 128, "B2": 512, "alg": "ijk_oblivious"},
    {"M": 512, "B1": opt_b1_4096, "B2": opt_b2_4096, "alg": "ijk_l2_tiled"},
]

try:
    question = int(sys.argv[1])
    assert question >= 6 and question <= 8
except (IndexError, ValueError) as e:
    print(f"Usage: {sys.argv[0]} <question number>")
    exit(1)

ACTIVE_RUNS = {6: q6_runs, 7: q7_runs, 8: q8_runs}[question]

for run_args in ACTIVE_RUNS:
    for trial in range(TRIALS):
        if ACTIVE_RUNS is q8_runs:
            out = make_perf_run(**run_args, trial=trial) 
        else:
            out = make_pin_run(**run_args, trial=trial) 
        print(out)
print()
