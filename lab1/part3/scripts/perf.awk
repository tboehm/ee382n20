#!/usr/bin/awk -f

BEGIN {
    FS = " "
    header = "Algorithm,M,BS1,BS2,L1-loads,L2-stores,L2-accesses,L3-loads,L3-stores,Mem-loads,Mem-stores,Seconds"
    csv_line = ""
    print(header)
}

/^ Performance counter stats for/ {
    alg = $7
    M = $9
    BS1 = $15
    BS2 = $17
    csv_line = alg "," M "," BS1 "," BS2
}

/((L1-dcache|LLC|mem)-(loads|stores|load-misses|store-misses)|l2_rqsts\.references)/ {
    value = $1
    gsub(/,/, "", value)
    csv_line = csv_line "," value
}

/seconds time elapsed/ {
    csv_line = csv_line "," $1
    print(csv_line)
    csv_line = ""
}
