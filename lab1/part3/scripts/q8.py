#!/usr/bin/env python3

import numpy as np
import os
import pandas as pd

data = pd.read_csv('runs/q8/perf_run3.csv')
data['L1-accesses'] = data['L1-loads'] + data['L2-stores']
data['L3-accesses'] = data['L3-loads'] + data['L3-stores']
data['Mem-accesses'] = data['Mem-loads'] + data['Mem-stores']


def format_stat(stat):
    sci = '%.2e' % stat
    base = float(sci[:4])
    exp = int(sci[6:])

    while exp % 3 != 0:
        exp -= 1
        base *= 10

    if exp == 0:
        return '%.2f' % base
    elif exp == 3:
        return '%.2f Th' % base
    elif exp == 6:
        return '%.2f Mn' % base
    elif exp == 9:
        return '%.2f Bn' % base
    else:
        return '%.3e' % stat


statistics = pd.DataFrame(columns=['Algorithm', 'M', 'BS1', 'BS2', 'Statistic', 'Mean', 'Stddev'])
for M in data.M.unique():
    sub1 = data[data.M == M]
    algs = sub1.Algorithm.unique()
    for alg in algs:
        sub2 = sub1[sub1.Algorithm == alg]
        block_l1s = sub2.BS1.unique()
        for bs1 in block_l1s:
            sub3 = sub2[sub2.BS1 == bs1]
            bs2 = sub3.BS2.unique()[0]
            for level in ['L1', 'L2', 'L3', 'Mem']:
                stat = level + "-accesses"
                mean = format_stat(sub3[stat].mean())
                std = format_stat(sub3[stat].std())
                row = {
                    'Algorithm': alg,
                    'M': M,
                    'BS1': bs1,
                    'BS2': bs2,
                    'Statistic': stat,
                    'Mean': mean,
                    'Stddev': std,
                }
                statistics = statistics.append(row, ignore_index=True)
print(statistics)
